package com.next.aap.ws.util;

import java.io.FileReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import au.com.bytecode.opencsv.CSVReader;

import com.next.aap.dto.AssemblyConstituencyDto;
import com.next.aap.dto.AssemblyConstituencyWeb;
import com.next.aap.dto.DistrictDto;
import com.next.aap.dto.DistrictWeb;
import com.next.aap.dto.ParliamentConstituencyDto;
import com.next.aap.dto.StateDto;
import com.next.aap.server.service.AapService;

public class ParliamentConstituencyDataUploader {

	private static Map<String, String> stateNameMap = new HashMap<String, String>();
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		stateNameMap.put("Andhra Pradesh", "Andhra Pradesh");
		stateNameMap.put("Dadra and Nagar Haveli", "Dadra and Nagar Haveli(UT)");
		stateNameMap.put("Daman and Diu", "Daman and Diu(UT)");
		stateNameMap.put("Goa", "Goa");
		stateNameMap.put("Gujarat", "Gujarat");
		stateNameMap.put("Haryana", "Haryana");
		stateNameMap.put("Himachal Pradesh", "Himachal Pradesh");
		stateNameMap.put("Jharkhand", "Jharkhand");
		stateNameMap.put("Madhya Pradesh", "Madhya Pradesh");
		stateNameMap.put("Meghalaya", "Meghalaya");
		stateNameMap.put("Mizoram", "Mizoram");
		stateNameMap.put("National Capital Territory of Delhi", "Delhi");
		stateNameMap.put("Odisha", "Odisha");
		
		stateNameMap.put("Puducherry", "Puducherry");
		stateNameMap.put("Rajasthan", "Rajasthan");
		stateNameMap.put("Sikkim", "Sikkim");
		stateNameMap.put("Tripura", "Tripura");
		stateNameMap.put("West Bengal", "West Bengal");
		stateNameMap.put("ANDAMAN & NICOBAR ISLANDS", "Andaman and Nicobar");
		stateNameMap.put("DADRA & NAGAR HAVELI", "Dadra and Nagar Haveli(UT)");
		stateNameMap.put("DAMAN & DIU", "Daman and Diu(UT)");
		stateNameMap.put("JAMMU & KASHMIR", "Jammu and Kashmir");
		stateNameMap.put("NATIONAL CAPITAL TERRITORY OF DELHI", "Delhi");
		stateNameMap.put("ORISSA", "Odisha");
		stateNameMap.put("PONDICHERRY", "Puducherry");
		stateNameMap.put("UTTARANCHAL", "Uttarakhand");
		
		ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("/mobilecore-spring.xml");
		AapService votingService = applicationContext.getBean(AapService.class);
		System.out.println("voting Service "+votingService);
		downloadAllData(votingService);
		applicationContext.close();
	}
	private static StateDto createState(AapService votingService, String name){
		name = stateNameMap.get(name);
		StateDto stateDto = new StateDto();
		stateDto.setName(name);
		stateDto = votingService.saveState(stateDto);
		return stateDto;
	}
	private static ParliamentConstituencyDto createPc(AapService aapService, String name, StateDto stateDto){
		name = removeCountNumber(name);
		System.out.println("saving Pc="+name);
		ParliamentConstituencyDto parliamentConstituencyDto = new ParliamentConstituencyDto();
		parliamentConstituencyDto.setName(name);
		parliamentConstituencyDto.setStateId(stateDto.getId());
		aapService.saveParliamentConstituency(parliamentConstituencyDto);
		//districtDto = votingService.saveDistrict(districtDto);
		
		
		return parliamentConstituencyDto;
	}
	
	/*
	private static PollingStationDto createPollingBooth(AapService votingService, String name, AssemblyConstituencyDto assemblyConstituencyDto){
		name = removeCountNumber(name);

		PollingStationDto pollingStationDto = new PollingStationDto();
		pollingStationDto.setName(name);
		pollingStationDto.setAssemblyConstituencyId(assemblyConstituencyDto.getId());
		pollingStationDto = votingService.savePollingStation(pollingStationDto);
		
		return pollingStationDto;
	}
	*/
	private static String removeCountNumber(String name){
		StringBuffer sb = new StringBuffer();
		char oneChar;
		boolean nameStarted = false;
		for(int i=0; i < name.length();i++ ){
			oneChar = name.charAt(i);
			if(!nameStarted){
				if(oneChar == '-' || oneChar == ' ' || (oneChar >= '0' && oneChar <= '9')){
					continue;
				}
			}
			nameStarted = true;
			sb.append(oneChar);
		}
		return sb.toString();
	}
	private static void downloadAllData(AapService aapService){
		try{
			CSVReader csvReader = new CSVReader(new FileReader("/Users/ravi/Downloads/MP_Info.csv"));
			List<String[]> allPcs = csvReader.readAll();
			int totalPc = 0;
			Set<String> missingStates = new TreeSet<String>();
			for(String[] onePcRow:allPcs){
				if(onePcRow[0].trim().equals("")){
					continue;
				}
				//System.out.println(onePcRow[0] + " = "+onePcRow[1]);
				String stateName = stateNameMap.get(onePcRow[0]);
				if(stateName == null){
					stateName = onePcRow[0];
				}
				StateDto state = aapService.getStateByName(stateName);
				if(state == null){
					missingStates.add(stateName);
				}
				createPc(aapService, onePcRow[1], state);
				totalPc++;
			}
			System.out.println("Total Rows = " + allPcs.size());
			System.out.println("Total Pc = " + totalPc);
			System.out.println("Total Missing States = " + missingStates.size());
			for(String oneStateName:missingStates){
				System.out.println("     " + oneStateName);
			}
			csvReader.close();
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
	}
	private static void sleep(long millis){
		try {
			Thread.sleep(millis * 2);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
