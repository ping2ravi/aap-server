package com.next.aap.ws.controller;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.next.aap.dto.AssemblyConstituencyWeb;
import com.next.aap.dto.DistrictWeb;
import com.next.aap.dto.NewsItem;
import com.next.aap.dto.StateDto;
import com.next.aap.server.cache.CandidateCache;
import com.next.aap.server.cache.LocationCache;
import com.next.aap.server.cache.ManifestoCache;
import com.next.aap.server.service.AapService;

@Controller
public class LocationController{

	@Autowired
	private LocationCache locationCache;
	@Autowired
	private ManifestoCache manifestoCache;
	@Autowired
	private CandidateCache candidateCache;
	@Autowired
	private AapService aapService;
	
	
	@RequestMapping(value="/state/getall", method = RequestMethod.GET)
    @ResponseBody
    public String getAllStates(HttpServletResponse httpResponse){
		return locationCache.getAllStates();
	}
	
	@RequestMapping(value="/state/save", method = RequestMethod.POST)
    @ResponseBody
    public StateDto saveState(@RequestBody StateDto stateDto){
		return aapService.saveState(stateDto);
	}
	
	@RequestMapping(value="/districtofstate/get/{stateId}", method = RequestMethod.GET)
    @ResponseBody
    public String getDistrictOfState(HttpServletResponse httpResponse,@PathVariable long stateId){
		return locationCache.getAllDistrictOfState(stateId);
	}
	
	@RequestMapping(value="/district/save", method = RequestMethod.POST)
    @ResponseBody
    public DistrictWeb saveDistrict(@RequestBody DistrictWeb districtWeb) {
		return aapService.saveDistrict(districtWeb);
	}
	@RequestMapping(value="/districts/save", method = RequestMethod.POST)
    @ResponseBody
    public List<DistrictWeb> saveDistricts(@RequestBody DistrictWeb[] districtWeb){
		return aapService.saveDistricts(Arrays.asList(districtWeb));
	}

	@RequestMapping(value="/acofdistrict/get/{districtId}", method = RequestMethod.GET)
    @ResponseBody
    public String getAllAssemblyConstituenciesOfDistrict(HttpServletResponse httpResponse,@PathVariable long districtId){
		return locationCache.getAllAssemblyConstituenciesOfDistrict(districtId);
	}
	
	@RequestMapping(value="/acofstate/get/{stateId}", method = RequestMethod.GET)
    @ResponseBody
    public String getAllAssemblyConstituenciesOfState(HttpServletResponse httpResponse,@PathVariable long stateId){
		return locationCache.getAllAssemblyConstituenciesOfState(stateId);
	}
	
	@RequestMapping(value="/pcofstate/get/{stateId}", method = RequestMethod.GET)
    @ResponseBody
    public String getAllParliamentConstituenciesOfState(HttpServletResponse httpResponse,@PathVariable long stateId){
		return locationCache.getAllParliamentConstituenciesOfState(stateId);
	}
	
	@RequestMapping(value="/acofstate/manifestolist/{stateId}", method = RequestMethod.GET)
    @ResponseBody
    public String getAllAssemblyConstituenciesOfStateForManifesto(HttpServletResponse httpResponse,@PathVariable long stateId){
		return manifestoCache.getAcWithManifesto();
	}

	@RequestMapping(value="/acofstate/candidatelist/{stateId}", method = RequestMethod.GET)
    @ResponseBody
    public String getAllAssemblyConstituenciesOfStateForCandidate(HttpServletResponse httpResponse,@PathVariable long stateId){
		return candidateCache.getAcWithCandidates();
	}

	@RequestMapping(value="/ac/save", method = RequestMethod.POST)
    @ResponseBody
    public AssemblyConstituencyWeb saveAssemblyConstituency(@RequestBody AssemblyConstituencyWeb assemblyConstituencyWeb){
		return aapService.saveAssemblyConstituency(assemblyConstituencyWeb);
	}
	
	@RequestMapping(value="/acs/save", method = RequestMethod.POST)
    @ResponseBody
    public List<AssemblyConstituencyWeb> saveAssemblyConstituency(@RequestBody AssemblyConstituencyWeb[] assemblyConstituencyWeb){
		return aapService.saveAssemblyConstituencies(Arrays.asList(assemblyConstituencyWeb));
	}

	@RequestMapping(value="/refresh/delhi/all", method = RequestMethod.GET)
    @ResponseBody
    public String refreshDelhiAllData(){
		locationCache.refreshCache();
		candidateCache.refreshCache();
		manifestoCache.refreshCache();
		return "All cache refreshed";
	}
	
	@RequestMapping(value="/refresh/location", method = RequestMethod.GET)
    @ResponseBody
    public String refreshAllLocations(){
		locationCache.refreshCache();
		return "All Location cache refreshed";

	}
	@RequestMapping(value="/refresh/delhi/candidate", method = RequestMethod.GET)
    @ResponseBody
    public String refreshDelhiCandidateData(){
		candidateCache.refreshCache();
		return "Candidate cache refreshed";
	}
	
	@RequestMapping(value="/refresh/delhi/manifesto", method = RequestMethod.GET)
    @ResponseBody
    public String refreshDelhiManifestoData(){
		manifestoCache.refreshCache();
		return "Manifesto cache refreshed";
	}
	
	@RequestMapping(value="/refresh/delhi/location", method = RequestMethod.GET)
    @ResponseBody
    public String refreshDelhiLocationData(){
		locationCache.refreshCache();
		return "Location cache refreshed";
	}
	
	@RequestMapping(value="/ac/manifesto/{acId}", method = RequestMethod.GET)
    @ResponseBody
    public String getManifestoOfAssemblyConstituency(@PathVariable Long acId){
		return manifestoCache.getManifestoOfAssemblyConstituency(acId);
	}

	@RequestMapping(value = "/getnews", method = RequestMethod.GET, 
	produces = "application/json; charset=utf-8")
	@ResponseBody
	public NewsItem sendMobileData() {
		NewsItem newsItem = aapService.getNewsByOriginalUrl("http://www.aamaadmiparty.org/news/%E0%A4%AC%E0%A4%A6%E0%A4%B2%E0%A4%BE%E0%A4%B5-%E0%A4%AE%E0%A5%87%E0%A4%82-%E0%A4%AD%E0%A4%BE%E0%A4%97%E0%A5%80%E0%A4%A6%E0%A4%BE%E0%A4%B0-%E0%A4%AC%E0%A4%A8%E0%A5%87%E0%A4%82-%E0%A4%AF%E0%A5%81%E0%A4%B5%E0%A4%BE-%E0%A4%86%E0%A4%AE-%E0%A4%86%E0%A4%A6%E0%A4%AE%E0%A5%80-%E0%A4%AA%E0%A4%BE%E0%A4%B0%E0%A5%8D%E0%A4%9F%E0%A5%80-%E0%A4%95%E0%A4%BE-%E0%A4%86%E0%A4%B9%E0%A5%8D%E0%A4%B5%E0%A4%BE%E0%A4%A8");
		return newsItem;
	}
}
