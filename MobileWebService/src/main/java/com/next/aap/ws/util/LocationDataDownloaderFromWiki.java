package com.next.aap.ws.util;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.next.aap.dto.AssemblyConstituencyDto;
import com.next.aap.dto.AssemblyConstituencyWeb;
import com.next.aap.dto.DistrictDto;
import com.next.aap.dto.DistrictWeb;
import com.next.aap.dto.StateDto;
import com.next.aap.server.service.AapService;

public class LocationDataDownloaderFromWiki {

	private static Map<String, String> stateNameMap = new HashMap<String, String>();
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		stateNameMap.put("Andhra Pradesh", "Andhra Pradesh");
		stateNameMap.put("Dadra and Nagar Haveli", "Dadra and Nagar Haveli(UT)");
		stateNameMap.put("Daman and Diu", "Daman and Diu(UT)");
		stateNameMap.put("Goa", "Goa");
		stateNameMap.put("Gujarat", "Gujarat");
		stateNameMap.put("Haryana", "Haryana");
		stateNameMap.put("Himachal Pradesh", "Himachal Pradesh");
		stateNameMap.put("Jharkhand", "Jharkhand");
		stateNameMap.put("Madhya Pradesh", "Madhya Pradesh");
		stateNameMap.put("Meghalaya", "Meghalaya");
		stateNameMap.put("Mizoram", "Mizoram");
		stateNameMap.put("National Capital Territory of Delhi", "Delhi");
		stateNameMap.put("Odisha", "Odisha");
		
		stateNameMap.put("Puducherry", "Puducherry");
		stateNameMap.put("Rajasthan", "Rajasthan");
		stateNameMap.put("Sikkim", "Sikkim");
		stateNameMap.put("Tripura", "Tripura");
		stateNameMap.put("West Bengal", "West Bengal");
		ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("/mobilecore-spring.xml");
		AapService votingService = applicationContext.getBean(AapService.class);
		System.out.println("voting Service "+votingService);
		downloadAllData(votingService);
		applicationContext.close();
	}
	private static StateDto createState(AapService votingService, String name){
		System.out.println("creating state = "+name);
		//name = stateNameMap.get(name);
		StateDto stateDto = new StateDto();
		stateDto.setName(name);
		stateDto = votingService.saveState(stateDto);
		return stateDto;
	}
	private static DistrictWeb createDistrict(AapService votingService, String name, StateDto stateDto){
		name = removeCountNumber(name);
		System.out.println("creating district="+name);
		DistrictWeb districtDto = new DistrictWeb();
		districtDto.setName(name);
		districtDto.setStateId(stateDto.getId());
		districtDto = votingService.saveDistrict(districtDto);
		
		
		return districtDto;
	}
	
	private static AssemblyConstituencyDto createAssemblyConstituency(AapService votingService, String name, DistrictDto districtDto){
		name = removeCountNumber(name);

		AssemblyConstituencyWeb assemblyConstituencyDto = new AssemblyConstituencyWeb();
		assemblyConstituencyDto.setName(name);
		assemblyConstituencyDto.setDistrictId(districtDto.getId());
		assemblyConstituencyDto = votingService.saveAssemblyConstituency(assemblyConstituencyDto);
		return assemblyConstituencyDto;
	}
	/*
	private static PollingStationDto createPollingBooth(AapService votingService, String name, AssemblyConstituencyDto assemblyConstituencyDto){
		name = removeCountNumber(name);

		PollingStationDto pollingStationDto = new PollingStationDto();
		pollingStationDto.setName(name);
		pollingStationDto.setAssemblyConstituencyId(assemblyConstituencyDto.getId());
		pollingStationDto = votingService.savePollingStation(pollingStationDto);
		
		return pollingStationDto;
	}
	*/
	private static String removeCountNumber(String name){
		StringBuffer sb = new StringBuffer();
		char oneChar;
		boolean nameStarted = false;
		for(int i=0; i < name.length();i++ ){
			oneChar = name.charAt(i);
			if(!nameStarted){
				if(oneChar == '-' || oneChar == ' ' || (oneChar >= '0' && oneChar <= '9')){
					continue;
				}
			}
			nameStarted = true;
			sb.append(oneChar);
		}
		return sb.toString();
	}
	private static void downloadAllData(AapService aapService){
		WebDriver webDriver = new FirefoxDriver();
		webDriver.get("http://en.wikipedia.org/wiki/List_of_districts_of_India");
		
		
		WebElement stateTable = webDriver.findElements(By.className("sortable")).get(0);
		List<WebElement> allRows = stateTable.findElements(By.tagName("tr"));
		Map<String, String> nameAndUrl = new LinkedHashMap<String, String>();
		int existingStates = 0;
		for(int i=1;i<allRows.size() - 1;i++){
			List<WebElement> allColumns = allRows.get(i).findElements(By.tagName("td"));
			WebElement aHref = allColumns.get(1).findElement(By.tagName("a"));
			if(stateNameMap.get(aHref.getText().trim()) == null){
				nameAndUrl.put(aHref.getText(), aHref.getAttribute("href"));
			}else{
				existingStates++;
				if(aHref.getText().trim().equals("Puducherry")){
					nameAndUrl.put(aHref.getText(), "http://en.wikipedia.org" + aHref.getAttribute("href"));
				}
				StateDto state = aapService.getStateByName(stateNameMap.get(aHref.getText().trim()));
				List<DistrictDto> districts = aapService.getAllDistrictOfState(state.getId());
				int totalDistricts = Integer.parseInt(allColumns.get(2).getText().trim());
				if(totalDistricts == districts.size()){
					//nameAndUrl.put(aHref.getText(), "http://en.wikipedia.org" + aHref.getAttribute("href"));
				}else{
					/*
					String districtNames = "";
					for(DistrictDto oneDistrict:districts){
						districtNames = districtNames + oneDistrict.getName()+",";
					}
					nameAndUrl.put("***Bad : totalDistricts="+totalDistricts+", districts.size()="+districts.size()+ " "+ aHref.getText(), "http://en.wikipedia.org" + aHref.getAttribute("href")+" : "+districtNames);
					*/
				}
			}
		}
		
		StateDto state;
		for(Entry<String, String> oneEntry:nameAndUrl.entrySet()){
			if(!oneEntry.getKey().equals("Uttar Pradesh")){
				continue;
			}
			System.out.println(oneEntry.getKey()+" = "+oneEntry.getValue());
			state = createState(aapService, oneEntry.getKey().trim());
			webDriver.get( oneEntry.getValue());
			List<WebElement> districtTables = webDriver.findElements(By.className("sortable"));
			if(districtTables == null || districtTables.size() == 0){
				continue;
			}
			WebElement districtTable = districtTables.get(1);
			allRows = districtTable.findElements(By.tagName("tr"));
			for(int i=1;i<allRows.size();i++){
				List<WebElement> allColumns = allRows.get(i).findElements(By.tagName("td"));
				try{
					WebElement aHref = allColumns.get(1).findElement(By.tagName("a"));
					createDistrict(aapService, aHref.getText().trim(), state);
					System.out.println("         "+ aHref.getText().trim());
				}catch(Exception ex){
					
				}
			}
		}
		System.out.println("existingStates="+existingStates);
		System.out.println("stateNameMap="+stateNameMap.size());
		webDriver.close();
		/*
		//read all states in combo box
		//WebElement stateComboBox = webDriver.findElement(By.name("ddlState"));
		Select stateComboBox = new Select(webDriver.findElement(By.id("ddlState")));
		Select districtComboBox;
		Select acComboBox;
		Select pbComboBox;
		int stateCount = 1;
		int districtCount = 1;
		int totalDistricts;
		int acCount = 1;
		int pbCount = 1;
		int totalAcs;
		int totalPbs;
		int totalStates = stateComboBox.getOptions().size();
		WebElement oneStateOption;
		WebElement oneDistrictOption;
		WebElement oneAcOption;
		WebElement onePbOption;
		
		String stateName;
		StateDto state;
		String districtName;
		DistrictDto districtDto;
		String acName;
		AssemblyConstituencyDto assemblyConstituencyDto;
		String pbName;
		while(stateCount < totalStates ){
			stateComboBox = new Select(webDriver.findElement(By.id("ddlState")));
			oneStateOption = stateComboBox.getOptions().get(stateCount);
			System.out.println("Working on State " + oneStateOption.getText());
			stateName = oneStateOption.getText();
			state = createState(votingService, stateName);
			stateComboBox.selectByIndex(stateCount);
			
			sleep(1000);
			
			districtComboBox = new Select(webDriver.findElement(By.id("ddlDistrict")));
			totalDistricts = districtComboBox.getOptions().size();
			districtCount = 1;
			while(districtCount < totalDistricts){
				districtComboBox = new Select(webDriver.findElement(By.id("ddlDistrict")));
				oneDistrictOption = districtComboBox.getOptions().get(districtCount);
				System.out.println("     Working on District " + oneDistrictOption.getText());
				districtName = oneDistrictOption.getText();
				districtComboBox.selectByIndex(districtCount);
				districtDto = createDistrict(votingService, districtName, state);
				
				sleep(1000);
				acComboBox = new Select(webDriver.findElement(By.id("ddlAC")));
				totalAcs = acComboBox.getOptions().size();
				acCount = 1;
				while(acCount < totalAcs){
					//acComboBox = new Select(webDriver.findElement(By.id("ddlAC")));
					oneAcOption = acComboBox.getOptions().get(acCount);
					System.out.println("          Working on AC " + oneAcOption.getText());
					acName = oneAcOption.getText();
					//acComboBox.selectByIndex(acCount);

					assemblyConstituencyDto = createAssemblyConstituency(votingService, acName, districtDto);
					//sleep(1000);
					
					
					acCount++;
				}
				districtCount++;
			}
			stateCount++;
		}
		*/
		
	}
	private static void sleep(long millis){
		try {
			Thread.sleep(millis * 2);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
