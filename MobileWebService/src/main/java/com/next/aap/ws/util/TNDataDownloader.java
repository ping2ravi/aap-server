package com.next.aap.ws.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.next.aap.dto.AssemblyConstituencyDto;
import com.next.aap.dto.AssemblyConstituencyWeb;
import com.next.aap.dto.DistrictDto;
import com.next.aap.dto.DistrictWeb;
import com.next.aap.dto.StateDto;
import com.next.aap.server.service.AapService;

public class TNDataDownloader {

	private static Map<String, String> stateNameMap = new HashMap<String, String>();
	
	private static Map<String, String> districtNameMap = new HashMap<String, String>();
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		stateNameMap.put("Andhra Pradesh", "Andhra Pradesh");
		stateNameMap.put("Dadra & Nagar Haveli", "Dadra and Nagar Haveli(UT)");
		stateNameMap.put("Daman & Diu", "Daman and Diu(UT)");
		stateNameMap.put("Goa", "Goa");
		stateNameMap.put("Gujarat", "Gujarat");
		stateNameMap.put("Haryana", "Haryana");
		stateNameMap.put("Himachal Pradesh", "Himachal Pradesh");
		stateNameMap.put("Jharkhand", "Jharkhand");
		stateNameMap.put("Madhya Pradesh", "Madhya Pradesh");
		stateNameMap.put("Meghalaya", "Meghalaya");
		stateNameMap.put("Mizoram", "Mizoram");
		stateNameMap.put("NCT OF Delhi", "Delhi");
		stateNameMap.put("Orissa", "Odisha");
		
		stateNameMap.put("Puducherry", "Puducherry");
		stateNameMap.put("Rajasthan", "Rajasthan");
		stateNameMap.put("Sikkim", "Sikkim");
		stateNameMap.put("Tripura", "Tripura");
		stateNameMap.put("West Bengal", "West Bengal");
		
		
		
		districtNameMap.put("Tiruvallur", "Thiruvallur");
		districtNameMap.put("Tirupur", "Tiruppur");
		districtNameMap.put("Tiruvarur", "Thiruvarur");
		districtNameMap.put("DAVANGERE", "Davanagere");
		districtNameMap.put("RAMANAGARAM", "Ramanagara");
		districtNameMap.put("TUMKUR", "Tumakuru");
		districtNameMap.put("UTTARA KANNADA", "Uttara Kannada");
		
		ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("/mobilecore-spring.xml");
		AapService votingService = applicationContext.getBean(AapService.class);
		System.out.println("voting Service "+votingService);
		downloadAllData(votingService);
		applicationContext.close();
	}
	private static DistrictWeb createDistrict(AapService votingService, String name, StateDto stateDto){
		name = removeCountNumber(name);
		if(districtNameMap.get(name) != null){
			name = districtNameMap.get(name);
		}
		System.out.println("name="+name);
		DistrictWeb districtDto = new DistrictWeb();
		districtDto.setName(name);
		districtDto.setStateId(stateDto.getId());
		districtDto = votingService.saveDistrict(districtDto);
		return districtDto;
	}
	
	private static AssemblyConstituencyDto createAssemblyConstituency(AapService votingService, String name, DistrictDto districtDto){
		name = removeCountNumber(name);
		System.out.println("          creating AC " +name);
		AssemblyConstituencyWeb assemblyConstituencyDto = new AssemblyConstituencyWeb();
		assemblyConstituencyDto.setName(name);
		assemblyConstituencyDto.setDistrictId(districtDto.getId());
		assemblyConstituencyDto = votingService.saveAssemblyConstituency(assemblyConstituencyDto);
		return assemblyConstituencyDto;
	}
	/*
	private static PollingStationDto createPollingBooth(AapService votingService, String name, AssemblyConstituencyDto assemblyConstituencyDto){
		name = removeCountNumber(name);

		PollingStationDto pollingStationDto = new PollingStationDto();
		pollingStationDto.setName(name);
		pollingStationDto.setAssemblyConstituencyId(assemblyConstituencyDto.getId());
		pollingStationDto = votingService.savePollingStation(pollingStationDto);
		
		return pollingStationDto;
	}
	*/
	private static String removeCountNumber(String name){
		StringBuffer sb = new StringBuffer();
		char oneChar;
		boolean nameStarted = false;
		for(int i=0; i < name.length();i++ ){
			oneChar = name.charAt(i);
			if(!nameStarted){
				if(oneChar == '-' || oneChar == ' ' || (oneChar >= '0' && oneChar <= '9')){
					continue;
				}
			}
			nameStarted = true;
			sb.append(oneChar);
		}
		return sb.toString();
	}
	private static void downloadAllData(AapService votingService){
		WebDriver webDriver = new FirefoxDriver();
		webDriver.get("http://en.wikipedia.org/wiki/List_of_Assembly_constituencies_of_Tamil_Nadu");
		
		//read all states in combo box
		//WebElement stateComboBox = webDriver.findElement(By.name("ddlState"));
		WebElement table = webDriver.findElement(By.className("wikitable"));
		List<WebElement> rows = table.findElements(By.tagName("tr"));
		WebElement oneRow;
		List<WebElement> columns;
		String acName;
		String districtName;
		DistrictDto oneDistrictDto;
		for(int i=1;i<rows.size();i++){
			oneRow = rows.get(i);
			columns = oneRow.findElements(By.tagName("td"));
			districtName = columns.get(2).getText().trim();
			acName = columns.get(1).getText().trim();
			if(districtNameMap.get(districtName) != null){
				districtName = districtNameMap.get(districtName);	
			}
			
			oneDistrictDto = votingService.getDistrictByNameAndStateId(districtName, 30L);
			if(oneDistrictDto == null){
				System.out.println("-* DO NOT EXISTS DistrictName : "+districtName+", AC="+columns.get(1).getText());	
			}else{
				System.out.println("EXISTS DistrictName : "+districtName+", AC="+columns.get(1).getText());
			}
			createAssemblyConstituency(votingService, acName, oneDistrictDto);
		}
		/*
		Select acComboBox;
		int districtCount = 1;
		int acCount = 1;
		int totalAcs;
		int totalDistricts = districtComboBox.getOptions().size();
		WebElement oneDistrictOption;
		WebElement oneAcOption;
		int exists = 0;
		int doNotExists = 0;
		StateDto state = votingService.getStateByName("Karnataka");
		AssemblyConstituencyDto assemblyConstituencyDto;
		if(state == null){
			return;
			//throw new Exception("State not found");
		}
		while(districtCount < totalDistricts ){
			
			
			districtComboBox = new Select(webDriver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlDistrict")));
			oneDistrictOption = districtComboBox.getOptions().get(districtCount);
			districtName = oneDistrictOption.getText().trim();
			districtName = districtName.split("/")[1].trim();
			if(districtNameMap.get(districtName) != null){
				districtName = districtNameMap.get(districtName);	
			}

			districtComboBox.selectByIndex(districtCount);
			sleep(1000);
			districtComboBox = new Select(webDriver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlDistrict")));
			

			//oneDistrictDto = votingService.getDistrictByNameAndStateId(districtName, 31L);
			oneDistrictDto = createDistrict(votingService, districtName, state);
			if(oneDistrictDto == null){
				doNotExists++;
				System.out.println("District : " + districtName +", DO NOT EXISTS");
				createDistrict(votingService, districtName, state);
			}else{
				exists++;
				//System.out.println("District : " + districtName +", EXISTS");	
			}
			
			acComboBox = new Select(webDriver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlAcname")));
			totalAcs = acComboBox.getOptions().size();
			acCount = 1;
			
			
			while(acCount < totalAcs){
				//acComboBox = new Select(webDriver.findElement(By.id("ddlAC")));
				oneAcOption = acComboBox.getOptions().get(acCount);
				acName = oneAcOption.getText();
				acName = acName.split("/")[1].trim();
				System.out.println("          Working on AC " + acName);
				//acComboBox.selectByIndex(acCount);

				assemblyConstituencyDto = createAssemblyConstituency(votingService, acName, oneDistrictDto);
				acCount++;
			}
			districtCount++;
		}
		System.out.println("Total District Exists : " + exists);
		System.out.println("Total District DO NOT Exists : " + doNotExists);
		*/
		webDriver.close();
		
	}
	private static void sleep(long millis){
		try {
			Thread.sleep(millis * 2);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
