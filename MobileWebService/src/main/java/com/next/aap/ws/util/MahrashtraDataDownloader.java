package com.next.aap.ws.util;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.next.aap.dto.AssemblyConstituencyDto;
import com.next.aap.dto.AssemblyConstituencyWeb;
import com.next.aap.dto.DistrictDto;
import com.next.aap.dto.DistrictWeb;
import com.next.aap.dto.StateDto;
import com.next.aap.server.service.AapService;

public class MahrashtraDataDownloader {

	private static Map<String, String> districtNameMap = new HashMap<String, String>();
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		districtNameMap.put("Amaravati", "Amravati");
		districtNameMap.put("Usmanabad", "Osmanabad");
		districtNameMap.put("Nandurabar", "Nandurbar");
		districtNameMap.put("Mumbai Suburban Dist.", "Mumbai Suburban");
		districtNameMap.put("Gondiya", "Gondia");
		
		ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("/mobilecore-spring.xml");
		AapService votingService = applicationContext.getBean(AapService.class);
		System.out.println("voting Service "+votingService);
		downloadAllData(votingService);
		applicationContext.close();
	}
	private static DistrictWeb createDistrict(AapService votingService, String name, StateDto stateDto){
		name = removeCountNumber(name);
		if(districtNameMap.get(name) != null){
			name = districtNameMap.get(name);
		}
		System.out.println("name="+name);
		DistrictWeb districtDto = new DistrictWeb();
		districtDto.setName(name);
		districtDto.setStateId(stateDto.getId());
		districtDto = votingService.saveDistrict(districtDto);
		
		
		return districtDto;
	}
	
	private static AssemblyConstituencyDto createAssemblyConstituency(AapService votingService, String name, DistrictDto districtDto){
		name = removeCountNumber(name);
		System.out.println("          creating AC " +name);
		AssemblyConstituencyWeb assemblyConstituencyDto = new AssemblyConstituencyWeb();
		assemblyConstituencyDto.setName(name);
		assemblyConstituencyDto.setDistrictId(districtDto.getId());
		assemblyConstituencyDto = votingService.saveAssemblyConstituency(assemblyConstituencyDto);
		return assemblyConstituencyDto;
	}
	/*
	private static PollingStationDto createPollingBooth(AapService votingService, String name, AssemblyConstituencyDto assemblyConstituencyDto){
		name = removeCountNumber(name);

		PollingStationDto pollingStationDto = new PollingStationDto();
		pollingStationDto.setName(name);
		pollingStationDto.setAssemblyConstituencyId(assemblyConstituencyDto.getId());
		pollingStationDto = votingService.savePollingStation(pollingStationDto);
		
		return pollingStationDto;
	}
	*/
	private static String removeCountNumber(String name){
		StringBuffer sb = new StringBuffer();
		char oneChar;
		boolean nameStarted = false;
		for(int i=0; i < name.length();i++ ){
			oneChar = name.charAt(i);
			if(!nameStarted){
				if(oneChar == '-' || oneChar == ' ' || (oneChar >= '0' && oneChar <= '9')){
					continue;
				}
			}
			nameStarted = true;
			sb.append(oneChar);
		}
		return sb.toString();
	}
	private static void downloadAllData(AapService votingService){
		WebDriver webDriver = new FirefoxDriver();
		webDriver.get("http://103.23.150.75/marathi/FrmSearchPage.aspx?ID=1");
		
		//read all states in combo box
		//WebElement stateComboBox = webDriver.findElement(By.name("ddlState"));
		Select districtComboBox = new Select(webDriver.findElement(By.id("ddldisen")));
		Select acComboBox;
		int districtCount = 1;
		int acCount = 0;
		int totalAcs;
		int totalDistricts = districtComboBox.getOptions().size();
		WebElement oneDistrictOption;
		WebElement oneAcOption;
		String acName;
		String districtName;
		DistrictDto oneDistrictDto;
		int exists = 0;
		int doNotExists = 0;
		StateDto state = votingService.getStateByName("Maharashtra");
		AssemblyConstituencyDto assemblyConstituencyDto;
		if(state == null){
			return;
			//throw new Exception("State not found");
		}
		while(districtCount < totalDistricts ){
			
			
			districtComboBox = new Select(webDriver.findElement(By.id("ddldisen")));
			oneDistrictOption = districtComboBox.getOptions().get(districtCount);
			districtName = oneDistrictOption.getText().trim();
			if(districtNameMap.get(districtName) != null){
				districtName = districtNameMap.get(districtName);	
			}

			districtComboBox.selectByIndex(districtCount);
			sleep(1000);
			districtComboBox = new Select(webDriver.findElement(By.id("ddldisen")));
			

			oneDistrictDto = votingService.getDistrictByNameAndStateId(districtName, state.getId());
			//oneDistrictDto = createDistrict(votingService, districtName, state);
			if(oneDistrictDto == null){
				doNotExists++;
				System.out.println("District : [" + districtName +"], DO NOT EXISTS");
				//createDistrict(votingService, districtName, state);
			}else{
				//System.out.println("District : [" + districtName +"], DO EXISTS");
				exists++;
				//System.out.println("District : " + districtName +", EXISTS");	
			}
			
			acComboBox = new Select(webDriver.findElement(By.id("DropDownList1")));
			totalAcs = acComboBox.getOptions().size();
			acCount = 0;
			
			
			while(acCount < totalAcs){
				//acComboBox = new Select(webDriver.findElement(By.id("ddlAC")));
				oneAcOption = acComboBox.getOptions().get(acCount);
				acName = oneAcOption.getText().trim();
				//acName = acName.split("/")[1].trim();
				System.out.println("          Working on AC " + acName);
				//acComboBox.selectByIndex(acCount);

				assemblyConstituencyDto = createAssemblyConstituency(votingService, acName, oneDistrictDto);
				acCount++;
			}
			districtCount++;
		}
		System.out.println("Total District Exists : " + exists);
		System.out.println("Total District DO NOT Exists : " + doNotExists);
		
		webDriver.close();
		
	}
	private static void sleep(long millis){
		try {
			Thread.sleep(millis * 2);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
