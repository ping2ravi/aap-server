package com.next.aap.ws.util;

import java.io.FileWriter;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import au.com.bytecode.opencsv.CSVWriter;


public class PhoneNumberDownloader {

	private static final String urlShortnerUrl = "http://myaap.in/yourls-api.php?format=json&username=arvind&password=4delhi&action=shorturl&url=";

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		
	     // feed in your array (or convert your data to an array)
			downloadData();

	}

	private static void downloadData() throws Exception {
		WebDriver webDriver = new FirefoxDriver();
		String[] alphabets ={"k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"};
		webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		boolean isFirstHref;
		Integer numberOfRecords = 1;
		int iCounter = 0;
		
		for(String oneAlphabet:alphabets){
			iCounter = 0;
			CSVWriter writer = new CSVWriter(new FileWriter("/Users/ravi/delhi/name-"+oneAlphabet+".csv", true), ',');
			isFirstHref = true;
			processALetter(webDriver, oneAlphabet);
			//System.out.println("Cliked");
			if("d".equals(oneAlphabet)){
				iCounter = 41;
			}
			boolean firstTimeException;
			boolean skip = true;
			firstTimeException = true;
			do{
				List<WebElement> allTables;
				WebElement oneTable = null;
				try{
					allTables = webDriver.findElements(By.tagName("TABLE"));
					oneTable = allTables.get(2);
					firstTimeException = true;
				}catch(Exception ex){
					//in case any exception is thrown wait for few seconds and then try again
					System.out.println("Waiting for 30 seconds befor trying again");
					Thread.sleep(30000);
					processALetter(webDriver, oneAlphabet);
					if(firstTimeException){
						if(iCounter > 0){
							iCounter--;
						}
						firstTimeException = false;
					}
					skip = true;
					continue;
				}
				firstTimeException = true;
				List<WebElement> allA = webDriver.findElements(By.tagName("a"));
				if(!skip){
					List<WebElement> allRows = oneTable.findElements(By.tagName("TR"));
					WebElement oneRow;
					for(int i=1;i<allRows.size();i++){
						//System.out.println("Row : "+i);
						oneRow = allRows.get(i);
						List<WebElement> allColumns = oneRow.findElements(By.tagName("TD"));
						if(numberOfRecords % 100 == 0){
							System.out.println(numberOfRecords);
						}
						numberOfRecords++;
						String[] data = new String[4];
						data[0] = numberOfRecords.toString();
						int colCount = 1;
						for(WebElement oneColumn:allColumns){
							data[colCount++] = oneColumn.getText();
							//System.out.print("," + oneColumn.getText());
						}
						writer.writeNext(data);
						//System.out.println("");
					}
				}else{
					skip = false;
				}
				
				boolean breakMainLoop = false;
				while(true){
					WebElement oneA = allA.get(iCounter++); 
					String href = oneA.getAttribute("href");
					if(href.contains("showNext")){
						if(isFirstHref){
							System.out.println("Found page A Href which is first so wont click it");
							isFirstHref = false;
						}else{
							System.out.println("Found page A Href "+ (iCounter -1 )+","+oneAlphabet+","+oneA.getText());
							oneA.click();
							break;
						}
					}else{
						System.out.println("Not a page A Href");
					}
					if(iCounter >= allA.size()){
						breakMainLoop = true;
						break;
					}
				}
				if(breakMainLoop){
					break;
				}
				System.out.println("Flushing ");
				writer.flush();
			}while(true);
			writer.close();
		}
		webDriver.close();
	}
	
	private static void processALetter(WebDriver webDriver, String oneAlphabet){
		webDriver.get("http://phonebook.bol.net.in/newindvnm1.html");
		WebElement alphabetWebElement = webDriver.findElement(By.name("INDV_NAME"));
		WebElement pageSizeWebElement = webDriver.findElement(By.name("NO_PAGE"));
		
		WebElement submitButtonWebElement = webDriver.findElement(By.name("submit1"));
		alphabetWebElement.sendKeys(oneAlphabet);
		pageSizeWebElement.clear();
		pageSizeWebElement.sendKeys("5000");
		//System.out.println("Cliking");
		submitButtonWebElement.click();

	}
	

}
