package com.next.aap.ws.util;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.google.gson.Gson;
import com.next.aap.dto.BlogItem;
import com.next.aap.server.service.AapService;

public class AapBlogDownloader {

	private static final String urlShortnerUrl = "http://myaap.in/yourls-api.php?format=json&username=arvind&password=4delhi&action=shorturl&url=";

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext(
				"/mobilecore-spring.xml");
		AapService aapService = applicationContext.getBean(AapService.class);
		System.out.println("aap Service " + aapService);
		downloadData(aapService);
		applicationContext.close();

	}
	
	private static void downloadData(AapService aapService) throws Exception {
		WebDriver webDriver = new FirefoxDriver();
		List<WebElement> readMoreLinks;
		List<String> allLinks = new ArrayList<String>();
		String url;
		WebElement pageDiv;
		BlogItem oneBlogItem;
		List<BlogItem> allblogItem = new ArrayList<BlogItem>();
		String title;
		String content;
		HttpUtil httpUtil = new HttpUtil();

		WebDriver webDriverForPage = new FirefoxDriver();
		BlogItem existingItem;
		for (int i = 1; i >= 0; i--) {
			webDriver.get("http://www.aamaadmiparty.org/blog?page=" + i);
			readMoreLinks = webDriver.findElements(By.className("read-more"));
			for (int j = readMoreLinks.size() - 1; j >= 0; j--) {
				WebElement oneWebElement = readMoreLinks.get(j);
				url = oneWebElement.findElement(By.tagName("a")).getAttribute(
						"href");
				System.out.println(url);
				allLinks.add(url);
				existingItem = aapService.getBlogByOriginalUrl(url);
				if(existingItem != null){
					//continue;
				}
				webDriverForPage.get(url);
				Thread.sleep(1000);
				pageDiv = webDriverForPage.findElement(By
						.className("aap-page-content-class"));

				title = pageDiv.findElement(By.className("pane-title"))
						.getText();
				WebElement nodeContentElement = pageDiv.findElement(By.className("node__content"));
				List<WebElement> fieldItems = nodeContentElement.findElements(By.className("field__item"));
				String imgSrc = null;
				try{
					WebElement imgElement = fieldItems.get(0).findElement(By.tagName("img"));
					if(imgElement != null){
						imgSrc = imgElement.getAttribute("src");
					}
				}catch(Exception ex){
					
				}
				System.out.println("+++++"+imgSrc);
				WebElement contentElement = pageDiv.findElement(By.className("field--type-text-with-summary"));
				
				
				content = (String)((JavascriptExecutor)webDriverForPage).executeScript("return arguments[0].innerHTML;", contentElement); 
				System.out.println("*** Content = "+content);
				
				
				//content = contentElement.getText();
				

				oneBlogItem = new BlogItem();
				oneBlogItem.setSource("www.aamaadmiparty.org");
				oneBlogItem.setTitle(title);
				oneBlogItem.setImageUrl(imgSrc);
				oneBlogItem.setOriginalUrl(url);
				oneBlogItem.setWebUrl(getShortUrl(httpUtil, url));
				oneBlogItem.setContent(content);
				// oneBlogItem.setId(id++);
				oneBlogItem.setAuthor("");
				System.out.println(oneBlogItem.getContent());
				aapService.saveBlog(oneBlogItem);

			}
		}
		System.out.println("Total Urls = " + allLinks.size());
		webDriverForPage.close();
		webDriver.close();
	}

	private static String getShortUrl(HttpUtil httpUtil, String longUrl) throws Exception{
		HttpClient httpClient = new DefaultHttpClient();
		System.out.println("Long url = " + longUrl);
		String encodedUrl = URLEncoder.encode(longUrl, "UTF-8");
		System.out.println("encodedUrl url = " + encodedUrl);
		System.out.println("final url = " + urlShortnerUrl + encodedUrl);
		String dayDonationString = httpUtil.getResponse(httpClient,
				urlShortnerUrl + encodedUrl);
		System.out.println("dayDonationString = " + dayDonationString);
		JSONObject jsonObject = new JSONObject(dayDonationString);
		String status = jsonObject.getString("status");
		if ("fail".equals(status)) {
			String errorCode = jsonObject.getString("code");
			if ("error:keyword".equals(errorCode)) {
				throw new RuntimeException(jsonObject.getString("message"));
			} else {
				throw new RuntimeException(jsonObject.getString("message"));
			}
		} else {
			String shortUrl = jsonObject.getString("shorturl");
			return shortUrl;
		}

	}

}
