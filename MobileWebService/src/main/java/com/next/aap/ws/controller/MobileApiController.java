package com.next.aap.ws.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.next.aap.dto.CandidateDto;
import com.next.aap.dto.CandidateSelectionProcess;
import com.next.aap.dto.ManifestoDto;
import com.next.aap.dto.RegisterFacebookUserProfile;
import com.next.aap.dto.RegisterGoogleUserProfile;
import com.next.aap.dto.RegisterTwitterUserProfile;
import com.next.aap.dto.RegisterUserDevice;
import com.next.aap.dto.RegisterUserDeviceResponse;
import com.next.aap.dto.RegisterUserProfile;
import com.next.aap.dto.RegisterVoiceOfAapDto;
import com.next.aap.dto.RegisterVoiceOfAapResponseDto;
import com.next.aap.dto.UserDeviceDto;
import com.next.aap.messages.FacebookUserRegisteredMessage;
import com.next.aap.messages.GoogleUserRegisteredMessage;
import com.next.aap.messages.NewBlogMessage;
import com.next.aap.messages.NewNewsMessage;
import com.next.aap.messages.NewVideoMessage;
import com.next.aap.messages.NotificationMessage;
import com.next.aap.messages.TaskStatus;
import com.next.aap.messages.TwitterUserRegisteredMessage;
import com.next.aap.messages.UserRegisteredMessage;
import com.next.aap.server.cache.DelhiElectionCache;
import com.next.aap.server.cache.HomeScreenDataCache;
import com.next.aap.server.cache.db.FacebookCache;
import com.next.aap.server.messages.MessageSender;
import com.next.aap.server.service.AapService;
import com.next.aap.server.tasks.TaskManager;


@Controller
public class MobileApiController {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private AapService aapService;
	@Autowired
	private HomeScreenDataCache homeScreenDataCache;
	@Autowired
	private DelhiElectionCache delhiElectionCache;
	@Autowired
	private TaskManager taskManager;
	@Autowired
	private MessageSender messageSender;
	@Autowired
	private FacebookCache facebookCache;
	
	@RequestMapping(value="/user/register/user", method = RequestMethod.POST)
    @ResponseBody
    public UserRegisteredMessage registerUserProfile(@RequestBody RegisterUserProfile registerUserProfile){
		UserRegisteredMessage facebookUserRegisteredMessage = new UserRegisteredMessage(); 
		try {
			RegisterUserDevice registerUserDevice = new RegisterUserDevice();
			registerUserDevice.setDeviceType(registerUserProfile.getDeviceType());
			registerUserDevice.setRegId(registerUserProfile.getDeviceRegId());
			aapService.registerDevice(registerUserDevice);
			facebookUserRegisteredMessage = aapService.registerUser(registerUserProfile);
		} catch (RuntimeException e) {
			facebookUserRegisteredMessage.setStatus(TaskStatus.STATUS_FAILED);
			logger.error("Error while registering user", e);
		}
		return facebookUserRegisteredMessage;
	}
	
	@RequestMapping(value="/user/register/facebook", method = RequestMethod.POST)
    @ResponseBody
    public FacebookUserRegisteredMessage registerFacebookUserProfile(@RequestBody RegisterFacebookUserProfile registerFacebookUserProfile){
		FacebookUserRegisteredMessage facebookUserRegisteredMessage = new FacebookUserRegisteredMessage(); 
		try {
			RegisterUserDevice registerUserDevice = new RegisterUserDevice();
			registerUserDevice.setDeviceType(registerFacebookUserProfile.getDeviceType());
			registerUserDevice.setRegId(registerFacebookUserProfile.getDeviceRegId());
			aapService.registerDevice(registerUserDevice);
			facebookUserRegisteredMessage = aapService.registerFacebookUser(registerFacebookUserProfile);
		} catch (RuntimeException e) {
			facebookUserRegisteredMessage.setStatus(TaskStatus.STATUS_FAILED);
			logger.error("Error while registering user", e);
		}
		return facebookUserRegisteredMessage;
	}
	
	@RequestMapping(value="/user/register/twitter", method = RequestMethod.POST)
    @ResponseBody
    public TwitterUserRegisteredMessage registerTwitterUserProfile(@RequestBody RegisterTwitterUserProfile registertwitterUserProfile){
		TwitterUserRegisteredMessage twitterUserRegisteredMessage = new TwitterUserRegisteredMessage(); 
		try {
			RegisterUserDevice registerUserDevice = new RegisterUserDevice();
			registerUserDevice.setDeviceType(registertwitterUserProfile.getDeviceType());
			registerUserDevice.setRegId(registertwitterUserProfile.getDeviceRegId());
			aapService.registerDevice(registerUserDevice);
			twitterUserRegisteredMessage = aapService.registerTwitterUser(registertwitterUserProfile);
		} catch (RuntimeException e) {
			twitterUserRegisteredMessage.setStatus(TaskStatus.STATUS_FAILED);
			logger.error("Error while registering user", e);
		}
		return twitterUserRegisteredMessage;
	}
	
	@RequestMapping(value="/user/register/google", method = RequestMethod.POST)
    @ResponseBody
    public GoogleUserRegisteredMessage registerGoogleUserProfile(@RequestBody RegisterGoogleUserProfile registerGoogleUserProfile){
		GoogleUserRegisteredMessage googleUserRegisteredMessage = new GoogleUserRegisteredMessage(); 
		try {
			RegisterUserDevice registerUserDevice = new RegisterUserDevice();
			registerUserDevice.setDeviceType(registerGoogleUserProfile.getDeviceType());
			registerUserDevice.setRegId(registerGoogleUserProfile.getDeviceRegId());
			aapService.registerDevice(registerUserDevice);
			googleUserRegisteredMessage = aapService.registerGoogleUser(registerGoogleUserProfile);
		} catch (RuntimeException e) {
			googleUserRegisteredMessage.setStatus(TaskStatus.STATUS_FAILED);
			logger.error("Error while registering user", e);
		}
		return googleUserRegisteredMessage;
	}
	/*
	@RequestMapping(value="/get/all/{lang}", method = RequestMethod.GET, 
			produces = "application/json; charset=utf-8")
    @ResponseBody
    public String getAllItems(HttpServletResponse httpResponse, @PathVariable long livingAcId, @PathVariable long votingAcId, @PathVariable long livingPcId,
    		@PathVariable long votingPcId,@PathVariable String lang){
		//httpResponse.setDateHeader("Last-Modified", System.currentTimeMillis());
		return homeScreenDataCache.getHomeScreenItems(lang, livingAcId, votingAcId, livingPcId, votingPcId);
	}
	*/
	
	@RequestMapping(value="/get/all/{livingAcId}/{votingAcId}/{livingPcId}/{votingPcId}/{lang}", method = RequestMethod.GET, 
			produces = "application/json; charset=utf-8")
    @ResponseBody
    public String getAllItemsByAcId(HttpServletResponse httpResponse, @PathVariable long livingAcId, @PathVariable long votingAcId, @PathVariable long livingPcId,
    		@PathVariable long votingPcId, @PathVariable String lang){
		//httpResponse.setDateHeader("Last-Modified", System.currentTimeMillis());
		return homeScreenDataCache.getHomeScreenItems(lang, livingAcId, votingAcId, livingPcId, votingPcId);
	}

	@RequestMapping(value="/csp/get/{lang}", method = RequestMethod.GET, 
			produces = "application/json; charset=utf-8")
    @ResponseBody
    public String getCandidateSelectionProcessEng(HttpServletResponse httpResponse, @PathVariable String lang){
		String candidateSelectionProceEng = delhiElectionCache.getCandidateSelectionProcess(lang);
		return candidateSelectionProceEng;
	}

	@RequestMapping(value="/csp/add/{lang}", method = RequestMethod.POST)
    @ResponseBody
    public String addHindiCandidateSelectionProcess(@RequestBody CandidateSelectionProcess candidateSelectionProcess,
    		 @PathVariable String lang){
		String json = new Gson().toJson(candidateSelectionProcess);
		delhiElectionCache.setCandidateSelectionProcess(lang, json);
		return json;
	}
	@RequestMapping(value="/refresh/all", method = RequestMethod.GET)
    @ResponseBody
    public String RefrreshAll(){
		homeScreenDataCache.refreshFullCache();
		return "All News/Video/Blog/Events items Refreshed";
	}

	/*
	@RequestMapping(value="/news/clear", method = RequestMethod.GET)
    @ResponseBody
    public String clearAllNews(){
		homeScreenDataCache.clearAllNewsItems();
		return "All News items cleared";
	}
	@RequestMapping(value="/news/clear/{id}", method = RequestMethod.GET)
    @ResponseBody
    public String clearParticularNews(@PathVariable long id){
		homeScreenDataCache.clearNewsItem(id);
		return "Item "+id +" has ben cleared";
		
	}
	@RequestMapping(value="/news/add/{lang}", method = RequestMethod.POST)
    @ResponseBody
    public String addNews(@RequestBody NewsItem newsItem, @PathVariable String lang){
		homeScreenDataCache.addNewsItemToCache(newsItem, lang);
		return homeScreenDataCache.getNewsItems(lang);
	}
	*/
	@RequestMapping(value="/news/get/{livingAcId}/{votingAcId}/{livingPcId}/{votingPcId}/{lang}", method = RequestMethod.GET, 
			produces = "application/json; charset=utf-8")
    @ResponseBody
    public String getNewinLanguage(@PathVariable long livingAcId, @PathVariable long votingAcId, @PathVariable long livingPcId,
    		@PathVariable long votingPcId, @PathVariable String lang){
		return homeScreenDataCache.getNewsItems(lang, livingAcId, votingAcId, livingPcId, votingPcId);
	}
	@RequestMapping(value="/news/get/{livingAcId}/{votingAcId}/{livingPcId}/{votingPcId}/{lang}/{pageNumber}", method = RequestMethod.GET, 
			produces = "application/json; charset=utf-8")
    @ResponseBody
    public String getCandidateSelectionProcessEng(HttpServletResponse httpResponse, @PathVariable long livingAcId, @PathVariable long votingAcId, @PathVariable long livingPcId,
    		@PathVariable long votingPcId, @PathVariable String lang, @PathVariable int pageNumber){
		return homeScreenDataCache.getNewsItems(lang, livingAcId, votingAcId, livingPcId, votingPcId, pageNumber);
	}


	/*
	@RequestMapping(value="/video/clear", method = RequestMethod.GET)
    @ResponseBody
    public String clearAllVideos(){
		homeScreenDataCache.clearAllVideoItems();
		return "All Video items cleared";
	}
	@RequestMapping(value="/video/add/{lang}", method = RequestMethod.POST)
    @ResponseBody
    public String addVideo(@RequestBody VideoItem videoItem, @PathVariable String lang){
		homeScreenDataCache.addVideoItemToCache(videoItem, lang);
		return homeScreenDataCache.getVideoItems(lang);

	}
	*/
	@RequestMapping(value="/video/get/{livingAcId}/{votingAcId}/{livingPcId}/{votingPcId}/{lang}", method = RequestMethod.GET, 
			produces = "application/json; charset=utf-8")
    @ResponseBody
    public String getVideoInLanguage(@PathVariable long livingAcId, @PathVariable long votingAcId, @PathVariable long livingPcId,
    		@PathVariable long votingPcId,@PathVariable String lang){
		return homeScreenDataCache.getVideoItems(lang, livingAcId, votingAcId, livingPcId, votingPcId);
	}
	@RequestMapping(value="/video/get/{livingAcId}/{votingAcId}/{livingPcId}/{votingPcId}/{lang}/{pageNumber}", method = RequestMethod.GET, 
			produces = "application/json; charset=utf-8")
    @ResponseBody
    public String getVideoInLanguage(@PathVariable long livingAcId, @PathVariable long votingAcId, @PathVariable long livingPcId,
    		@PathVariable long votingPcId,@PathVariable String lang,@PathVariable int pageNumber){
		return homeScreenDataCache.getVideoItems(lang, livingAcId, votingAcId, livingPcId, votingPcId, pageNumber);
	}
	/*
	@RequestMapping(value="/blog/add/{lang}", method = RequestMethod.POST)
    @ResponseBody
    public String addBlog(@RequestBody BlogItem blogItem, @PathVariable String lang){
		homeScreenDataCache.addBlogItemToCache(blogItem, lang);
		return homeScreenDataCache.getBlogItems(lang);
	}
	*/
	@RequestMapping(value="/blog/get/{livingAcId}/{votingAcId}/{livingPcId}/{votingPcId}/{lang}", method = RequestMethod.GET, 
			produces = "application/json; charset=utf-8")
    @ResponseBody
    public String getBlogInLanguage(@PathVariable long livingAcId, @PathVariable long votingAcId, @PathVariable long livingPcId,
    		@PathVariable long votingPcId,@PathVariable String lang){
		return homeScreenDataCache.getBlogItems(lang, livingAcId, votingAcId, livingPcId, votingPcId);
	}
	@RequestMapping(value="/blog/get/{livingAcId}/{votingAcId}/{livingPcId}/{votingPcId}/{lang}/{pageNumber}", method = RequestMethod.GET, 
			produces = "application/json; charset=utf-8")
    @ResponseBody
    public String getBlogInLanguage(@PathVariable long livingAcId, @PathVariable long votingAcId, @PathVariable long livingPcId,
    		@PathVariable long votingPcId,@PathVariable String lang, @PathVariable long acId,  @PathVariable int pageNumber){
		return homeScreenDataCache.getBlogItems(lang, livingAcId, votingAcId, livingPcId, votingPcId, pageNumber);
	}
	/*
	@RequestMapping(value="/event/add", method = RequestMethod.POST)
    @ResponseBody
    public String addEvent(@RequestBody EventItem eventItem, @PathVariable String lang){
		homeScreenDataCache.addEventItemToCache(eventItem, lang);
		return homeScreenDataCache.getEventItems(lang);
	}
	*/
	@RequestMapping(value="/device/register/android", method = RequestMethod.POST)
    @ResponseBody
    public RegisterUserDeviceResponse registerDevice(@RequestBody RegisterUserDevice registerUserDevice){
		return aapService.registerDevice(registerUserDevice);
		/*
		registerUserDevice.setDeviceType("Android");
		RegisterUserDeviceResponse registerDeviceResponse = taskManager.registerUserDeviceTaskAsync(registerUserDevice);
		return registerDeviceResponse;
		*/
	}
	
	@RequestMapping(value="/message/sendnews", method = RequestMethod.GET)
    @ResponseBody
    public String sendNewsNotification(){
		long startId = 0;
		int pageSize = 100;
		NewNewsMessage newNewsMessage = new NewNewsMessage();
		newNewsMessage.setTotalItem(1);
		newNewsMessage.setNotificationTitle("New AAP News Available");
		newNewsMessage.setNotificationDescription("New AAP News Available");
		while(true){
			List<UserDeviceDto> userDevices = aapService.getAndroidUserDevices(startId, pageSize);
			if(userDevices == null || userDevices.size() == 0){
				break;
			}
			List<String> deviceList = new ArrayList<String>(userDevices.size());
			for(UserDeviceDto oneUserDeviceDto:userDevices){
				deviceList.add(oneUserDeviceDto.getRegId());
				startId = oneUserDeviceDto.getId();
			}
			try {
				messageSender.sendMessage(newNewsMessage, NotificationMessage.NEW_NEWS_MESSAGE, deviceList);
			} catch (IOException e) {
				e.printStackTrace();
				return "Error While Sending";
			}
		}
		return "Test Message Sent";
	}
	@RequestMapping(value="/message/sendvideo", method = RequestMethod.GET)
    @ResponseBody
    public String sendVideoNotification(){
		long startId = 0;
		int pageSize = 100;
		NewVideoMessage newVideoMessage = new NewVideoMessage();
		newVideoMessage.setTotalItem(1);
		newVideoMessage.setNotificationTitle("New AAP Video Available");
		newVideoMessage.setNotificationDescription("New AAP Video Available");
		while(true){
			List<UserDeviceDto> userDevices = aapService.getAndroidUserDevices(startId, pageSize);
			if(userDevices == null || userDevices.size() == 0){
				break;
			}
			List<String> deviceList = new ArrayList<String>(userDevices.size());
			for(UserDeviceDto oneUserDeviceDto:userDevices){
				deviceList.add(oneUserDeviceDto.getRegId());
				startId = oneUserDeviceDto.getId();
			}
			try {
				messageSender.sendMessage(newVideoMessage, NotificationMessage.NEW_VIDEO_MESSAGE, deviceList);
			} catch (IOException e) {
				e.printStackTrace();
				return "Error While Sending";
			}
		}
		return "Test Message Sent";
	}
	
	@RequestMapping(value="/message/sendblog", method = RequestMethod.GET)
    @ResponseBody
    public String sendBlogNotification(){
		long startId = 0;
		int pageSize = 100;
		NewBlogMessage newBlogMessage = new NewBlogMessage();
		newBlogMessage.setTotalItem(1);
		newBlogMessage.setNotificationTitle("New AAP Blog Available");
		newBlogMessage.setNotificationDescription("New AAP Blog Available");
		while(true){
			List<UserDeviceDto> userDevices = aapService.getAndroidUserDevices(startId, pageSize);
			if(userDevices == null || userDevices.size() == 0){
				break;
			}
			List<String> deviceList = new ArrayList<String>(userDevices.size());
			for(UserDeviceDto oneUserDeviceDto:userDevices){
				deviceList.add(oneUserDeviceDto.getRegId());
				startId = oneUserDeviceDto.getId();
			}
			try {
				messageSender.sendMessage(newBlogMessage, NotificationMessage.NEW_BLOG_MESSAGE, deviceList);
			} catch (IOException e) {
				e.printStackTrace();
				return "Error While Sending";
			}
		}
		return "Test Message Sent";
	}
	
	
	@RequestMapping(value="/candidate/save", method = RequestMethod.POST)
    @ResponseBody
    public CandidateDto saveCandidate(@RequestBody CandidateDto candidateDto){
		candidateDto = aapService.saveCandidate(candidateDto);
		return candidateDto;
	}
	
	@RequestMapping(value="/manifesto/save", method = RequestMethod.POST)
    @ResponseBody
    public ManifestoDto saveManifesto(@RequestBody ManifestoDto manifestoDto){
		manifestoDto = aapService.saveManifesto(manifestoDto);
		return manifestoDto;
	}
	
	@RequestMapping(value="/voiceofapp/register", method = RequestMethod.POST)
    @ResponseBody
    public RegisterVoiceOfAapResponseDto registerVoiceOfAap(@RequestBody RegisterVoiceOfAapDto registerVoiceOfAapDto){
		RegisterVoiceOfAapResponseDto registerVoiceOfAapResponseDto = new RegisterVoiceOfAapResponseDto();
		registerVoiceOfAapResponseDto.setUserName(registerVoiceOfAapDto.getUserName());
		registerVoiceOfAapResponseDto.setStatus(TaskStatus.STATUS_COMPLETED);
		return registerVoiceOfAapResponseDto;
	}

	@RequestMapping(value="/facebook/feed", method = RequestMethod.GET)
    @ResponseBody
    public String getFacebookFeed(){
		return facebookCache.getFacebookFeed();
	}

}
