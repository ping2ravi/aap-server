package com.next.aap.server.cache.file;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import com.google.gson.Gson;
import com.next.aap.dto.AllHomeScreenItem;
import com.next.aap.dto.BlogItem;
import com.next.aap.dto.NewsItem;
import com.next.aap.dto.VideoItem;

public class HomeScreenDataCacheFileImplTest {

	HomeScreenDataCacheFileImpl homeScreenDataCacheFileImpl;
	
	@Before
	public void initTest(){
		homeScreenDataCacheFileImpl = new HomeScreenDataCacheFileImpl();
	}
	/**
	 * Test when there is enough space to hold one new item
	 */
	@Test
	public void TestAddItemToList01(){
		List<NewsItem> newsItems = new ArrayList<NewsItem>();
		NewsItem oneNewsItem = new NewsItem();
		homeScreenDataCacheFileImpl.addItemToList(newsItems, oneNewsItem, 10);
		assertEquals(1, newsItems.size());
	}
	
	/**
	 * Test when there is only one space to hold one new item
	 */
	@Test
	public void TestAddItemToList02(){
		List<NewsItem> newsItems = new ArrayList<NewsItem>();
		NewsItem oneNewsItem = new NewsItem();
		homeScreenDataCacheFileImpl.addItemToList(newsItems, oneNewsItem, 1);
		assertEquals(1, newsItems.size());
	}
	
	/**
	 * Test when there is no space to hold one new item
	 */
	@Test
	public void TestAddItemToList03(){
		List<NewsItem> newsItems = new ArrayList<NewsItem>();
		for(int i=0;i<10;i++){
			NewsItem oneNewsItem = new NewsItem();
			homeScreenDataCacheFileImpl.addItemToList(newsItems, oneNewsItem, 10);
		}
		assertEquals(10, newsItems.size());
		
		NewsItem oneNewsItem = new NewsItem();
		homeScreenDataCacheFileImpl.addItemToList(newsItems, oneNewsItem, 10);
		assertEquals(10, newsItems.size());
		
		assertEquals(oneNewsItem, newsItems.get(0));
		
		oneNewsItem = new NewsItem();
		homeScreenDataCacheFileImpl.addItemToList(newsItems, oneNewsItem, 10);
		assertEquals(10, newsItems.size());
		assertEquals(oneNewsItem, newsItems.get(0));
	}
	
	/**
	 * Test for adding a news item to cache and then read it back with the same language
	 */
	@Test
	public void TestAddNewsItemToCache01(){
		NewsItem oneNewsItem = new NewsItem();
		homeScreenDataCacheFileImpl.addNewsItemToCache(oneNewsItem, "en");
		String jsonString = homeScreenDataCacheFileImpl.getNewsItems("en");
		Gson gson = new Gson();
		List list = gson.fromJson(jsonString, List.class);
		assertEquals(1, list.size());
	}
	
	/**
	 * Test for adding a news item to cache and then read it back with the different language
	 * i.e. added with en but reading with hin
	 */
	@Test
	public void TestAddNewsItemToCache02(){
		NewsItem oneNewsItem = new NewsItem();
		homeScreenDataCacheFileImpl.addNewsItemToCache(oneNewsItem, "en");
		String jsonString = homeScreenDataCacheFileImpl.getNewsItems("hin");
		Gson gson = new Gson();
		List list = gson.fromJson(jsonString, List.class);
		assertEquals(1, list.size());
	}
	
	/**
	 * Create Test Json
	 */
	@Test
	public void createTestJson(){
		NewsItem oneNewsItem = new NewsItem();
		oneNewsItem.setAuthor("Ravi Shamra");
		oneNewsItem.setContent("The Cabinet passed an ordinance undoing the order of Supreme Court on convicted MPs and MLAs today.<br><p> Aam Aadmi Party (AAP) condemns the ordinance that has reversed the order of apex court which had debarred tainted politicians from contesting polls. The passing of this ordinance is not only a severe blow to Democracy, but also exposes the intentions of the political class which has stooped to a new low to protect the corrupt.");
		oneNewsItem.setId(1L);
		oneNewsItem.setImageUrl("https://lh3.googleusercontent.com/-14jQXJAjAco/UgorbxaEIuI/AAAAAAAANdY/gOE96ze88aY/w793-h595-no/quest04.jpg");
		List<String> oneLiners = new ArrayList<String>();
		oneLiners.add("Why BJP/Congress supporting criminals in Parliament, #Removecriminals");
		oneLiners.add("#AAP is the only choice for this country which can make it corruption free");
		oneLiners.add("#DOnate4AAP and help us fight against corrupted");
		oneNewsItem.setOneLiners(oneLiners);
		oneNewsItem.setDate(new Date().toGMTString());
		oneNewsItem.setSource("aamaadmiparty.org");
		oneNewsItem.setTitle("AAM AADMI PARTY CONDEMNS PASSING OF ORDINANCE REVERSING SUPREME COURT ORDER ON CONVICTED MPS AND MLA");
		oneNewsItem.setWebUrl("http://www.aamaadmiparty.org/news/aam-aadmi-party-condemns-passing-of-ordinance-reversing-supreme-court-order-on-convicted-mps");
		
		AllHomeScreenItem allHomeScreenItem = homeScreenDataCacheFileImpl.createAllHomeScreenItem();
		allHomeScreenItem.getNewsItems().add(oneNewsItem);
		
		oneNewsItem = new NewsItem();
		oneNewsItem.setAuthor("Indraneel");
		oneNewsItem.setContent("आम आदमी पार्टी ने आज 6 और विधान सभा सीटो के लिए अपने प्रत्याशियो का चयन किया। इन सीटो में वजीरपुर, बादली, चांदनी चौक, मंगोलपुरी, पालम और गोंडा शामिल है. पार्टी ने मशहूर TV अभिनेता प्रवीण कुमार, जिन्होंने छोटे परदे पर महाभारत में \"भीम\" का किरदार निभाया, उनको वजीरपुर शेत्र से चुनाव में मैदान में उतारने का फैसला लिया है. विक्रम बधवार, जिन्हें पार्टी चांदनी चौक विधान सभा से लडवाएगी, ने कई व्यापारी संघटनो के  जुड़कर व्यापारियो के लिए काम किया है. मंगोलपुरी में पार्टी ने तय एक युवा पत्रकार राखी बिरला को चुनावी मैदान में उतारेगी। इनके अलावा बादली से मोहन कृष्ण शर्मा,  पालम से सुनील पोखरियाल और गोंडा से लक्ष्मी नारायण शर्मा आगामी चुनाव में आम आदमी पार्टी का चेहरा बनेंगे। इन उम्मीदवारों की विस्तृत जानकारी निम्न है.  " +
				"<p.><b>प्रवीण कुमार \"भीम\", वजीरपुर</b></p>" + 
				"<p>चर्चित धारावाहिक महाभारत में भीम के किरदार से ख्याति प्राप्त करने वाले टीवी व फिल्म कलाकार प्रवीण कुमार एथलेटिक्स की दुनिया के भी सम्मानित नाम हैं. ओलंपिक गेम्स,कॉमनवेल्थ गेम्स, एशियन गेम्स जैसे अंतरराष्ट्रीय प्रतिस्पर्धाओं में प्रवीण कुमार ने हैमर थ्रो और डिस्क्स थ्रो खेलों में भारत का प्रतिनिधित्व किया और मेडल जीते. स्पोर्टस के क्षेत्र में गौरवपूर्ण योगदान को देखते हुए भारत सरकार ने प्रवीण कुमार को अर्जुन अवार्ड से सम्मानित किया है. खेलों के अलावा सीमा सुरक्षा बल में बतौर डिप्टी कमांडर काम करके भी उन्होंने देश की सेवा की है.");
		oneNewsItem.setId(2L);
		oneNewsItem.setImageUrl("https://lh5.googleusercontent.com/-UURvbgjg2T8/UiSCQc-QYRI/AAAAAAAANiA/iKgD4AjlR-o/w720-h540-no/broom.jpg");
		oneNewsItem.setDate(new Date().toGMTString());
		oneNewsItem.setSource("aamaadmiparty.org");
		oneNewsItem.setTitle("Candidate Declared for 6 more Delhi Assembly Seats");
		oneNewsItem.setWebUrl("http://www.aamaadmiparty.org/news/candidate-declared-for-6-more-delhi-assembly-seats");
		allHomeScreenItem.getNewsItems().add(oneNewsItem);
		
		VideoItem oneVideoItem = new VideoItem();
		oneVideoItem.setDescription("Announcement by AAP leaders over ordinance by Cabinet for reversing Supreme Court's order debarring tainted MPs and MLAs from contesting polls");
		oneVideoItem.setId(1L);
		oneVideoItem.setImageUrl("http://i1.ytimg.com/vi/K858o9-cWdI/default.jpg");
		oneVideoItem.setOneLiners(oneLiners);
		oneVideoItem.setPublishDate(new Date());
		oneVideoItem.setTitle("Announcement by AAP leaders over ordinance");
		oneVideoItem.setWebUrl("http://www.youtube.com/watch?v=K858o9-cWdI");
		oneVideoItem.setYoutubeVideoId("K858o9-cWdI");
		allHomeScreenItem.getVideoItems().add(oneVideoItem);
		
		oneVideoItem = new VideoItem();
		oneVideoItem.setDescription("On 22nd Sep 13 , Manish Sisodia nominated from Patparganj Vidhan Sabha went to West Vinod Nagar to make Manifesto.");
		oneVideoItem.setId(2L);
		oneVideoItem.setImageUrl("http://i1.ytimg.com/vi/r0n_ZOJ8Pcw/default.jpg");
		oneVideoItem.setOneLiners(oneLiners);
		oneVideoItem.setPublishDate(new Date());
		oneVideoItem.setTitle("AAP Patparganj Manifesto for Delhi assembly elections.");
		oneVideoItem.setWebUrl("http://www.youtube.com/watch?v=r0n_ZOJ8Pcw");
		oneVideoItem.setYoutubeVideoId("r0n_ZOJ8Pcw");
		allHomeScreenItem.getVideoItems().add(oneVideoItem);
		
		
		BlogItem oneBlogItem = new BlogItem();
		oneBlogItem.setAuthor("Ravi Shamra");
		oneBlogItem.setContent("The Cabinet passed an ordinance undoing the order of Supreme Court on convicted MPs and MLAs today.<br><p> Aam Aadmi Party (AAP) condemns the ordinance that has reversed the order of apex court which had debarred tainted politicians from contesting polls. The passing of this ordinance is not only a severe blow to Democracy, but also exposes the intentions of the political class which has stooped to a new low to protect the corrupt.");
		oneBlogItem.setId(1L);
		oneBlogItem.setImageUrl("https://lh3.googleusercontent.com/-14jQXJAjAco/UgorbxaEIuI/AAAAAAAANdY/gOE96ze88aY/w793-h595-no/quest04.jpg");oneBlogItem.setOneLiners(oneLiners);
		oneBlogItem.setDate(new Date().toGMTString());
		oneBlogItem.setSource("aamaadmiparty.org");
		oneBlogItem.setTitle("AAM AADMI PARTY CONDEMNS PASSING OF ORDINANCE REVERSING SUPREME COURT ORDER ON CONVICTED MPS AND MLA");
		oneBlogItem.setWebUrl("http://www.aamaadmiparty.org/news/aam-aadmi-party-condemns-passing-of-ordinance-reversing-supreme-court-order-on-convicted-mps");
		allHomeScreenItem.getBlogItems().add(oneBlogItem);
		
		oneBlogItem.setAuthor("Indraneel");
		oneBlogItem.setContent("The Cabinet passed an ordinance undoing the order of Supreme Court on convicted MPs and MLAs today.<br><p> Aam Aadmi Party (AAP) condemns the ordinance that has reversed the order of apex court which had debarred tainted politicians from contesting polls. The passing of this ordinance is not only a severe blow to Democracy, but also exposes the intentions of the political class which has stooped to a new low to protect the corrupt.");
		oneBlogItem.setId(2L);
		oneBlogItem.setImageUrl("https://lh3.googleusercontent.com/-14jQXJAjAco/UgorbxaEIuI/AAAAAAAANdY/gOE96ze88aY/w793-h595-no/quest04.jpg");oneBlogItem.setOneLiners(oneLiners);
		oneBlogItem.setDate(new Date().toGMTString());
		oneBlogItem.setSource("aamaadmiparty.org");
		oneBlogItem.setTitle("AAM AADMI PARTY CONDEMNS PASSING OF ORDINANCE REVERSING SUPREME COURT ORDER ON CONVICTED MPS AND MLA");
		oneBlogItem.setWebUrl("http://www.aamaadmiparty.org/news/aam-aadmi-party-condemns-passing-of-ordinance-reversing-supreme-court-order-on-convicted-mps");
		allHomeScreenItem.getBlogItems().add(oneBlogItem);
		
		Gson gson = new Gson();
		String output= gson.toJson(allHomeScreenItem);
		System.out.println(output);
	}

}
