package com.next.aap.server.service;

import org.hibernate.PropertyValueException;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.next.aap.server.persistance.User;



public class AapServiceImplTest {
	
	ApplicationContext applicationContext;
	@Before
	public void initTest(){
		applicationContext = new ClassPathXmlApplicationContext("/mobilecore-spring.xml");
	}
	
	@After
	public void finishTest(){
		((ClassPathXmlApplicationContext)applicationContext).close();
	}
	/**
	 * Test when email is empty
	 */
	@Test(expected=PropertyValueException.class)
	@Ignore
	public void testSaveUser01(){
		User user = new User();
		user.setName("Ravi Shrma");
		AapService aapService = applicationContext.getBean(AapService.class);
		aapService.saveUser(user);
	}
	/**
	 * Test when name is empty
	 */
	@Test(expected=PropertyValueException.class)
	@Ignore
	public void testSaveUser02(){
		User user = new User();
		user.setEmail("ping2ravi@gmail.com");
		AapService aapService = applicationContext.getBean(AapService.class);
		aapService.saveUser(user);
	}
	
	/**
	 * Test when name and email are provided
	 */
	@Test
	@Ignore
	public void testSaveUser03(){
		User user = new User();
		user.setEmail("ping2ravi@gmail.com");
		user.setName("Ravi Sharma");
		AapService aapService = applicationContext.getBean(AapService.class);
		aapService.saveUser(user);
		System.out.println("User added "+user.getId());
		User savedUser = aapService.getUserById(user.getId());
		System.out.println("User Found "+savedUser.getId());
	}
	
}
