package com.next.aap.server.persistance.dao.impl;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.stereotype.Repository;

import com.next.aap.server.persistance.Trend;
import com.next.aap.server.persistance.dao.TrendDao;

@Repository
public class TrendDaoHibernateSpringImpl extends BaseDaoHibernateSpring<Trend> implements TrendDao  {

	/* (non-Javadoc)
	 * @see com.next.aap.server.persistance.dao.impl.TrendDao#saveTrend(com.next.aap.server.persistance.Trend)
	 */
	@Override
	public Trend saveTrend(Trend trend) 
	{
		checkIfStringMissing("TrendText", trend.getTrendText());
		trend = super.saveObject(trend);
		return trend;
	}
	

	/* (non-Javadoc)
	 * @see com.next.aap.server.persistance.dao.impl.TrendDao#deleteTrend(com.next.aap.server.persistance.Trend)
	 */
	@Override
	public void deleteTrend(Trend trend)  {
		super.deleteObject(trend);
	}

	/* (non-Javadoc)
	 * @see com.next.aap.server.persistance.dao.impl.TrendDao#getTrendById(java.lang.Long)
	 */
	@Override
	public Trend getTrendById(Long id) 
	{
		Trend trend = super.getObjectById(Trend.class, id);
		return trend;
	}

	/* (non-Javadoc)
	 * @see com.next.aap.server.persistance.dao.impl.TrendDao#getAllTrends()
	 */
	@Override
	public List<Trend> getAllTrends() 
	{
		List<Trend> list = executeQueryGetList("from Trend");
		return list;
	}
	/*
	private void checkIfTrendExistsWithSameName(Trend trend) {
		Map<String, Object> params = new TreeMap<String, Object>();
		params.put("nameUp", trend.getName());
		Trend existingTrend;
		if(trend.getId() != null && trend.getId() > 0){
			params.put("id", trend.getId());
			existingTrend = executeQueryGetObject("from Trend where nameUp=:nameUp and id != :id", params);
		}else{
			existingTrend = executeQueryGetObject("from Trend where nameUp=:nameUp ", params);
		}
		
		if(existingTrend != null){
			throw new RuntimeException("Trend already exists with name = "+trend.getName());
		}
	}
	*/
	
	@Override
	public Trend getTrendByName(String name) {
		Map<String, Object> params = new TreeMap<String, Object>();
		params.put("nameUp", name.toUpperCase());
		Trend existingTrend = executeQueryGetObject("from Trend where nameUp = :nameUp ", params);
		return existingTrend;
	}

}