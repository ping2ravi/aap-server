package com.next.aap.server.tasks;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.next.aap.dto.RegisterUserDevice;
import com.next.aap.dto.RegisterUserDeviceResponse;
import com.next.aap.messages.DeviceRegisteredMessage;
import com.next.aap.messages.NotificationMessage;
import com.next.aap.messages.TaskStatus;
import com.next.aap.server.messages.MessageSender;
import com.next.aap.server.service.AapService;

@Component
@Scope(value = "prototype")
public class RegisterDeviceTask implements Runnable {

	private RegisterUserDevice registerUserDevice;
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private AapService appService;
	@Autowired
	private MessageSender messageSender;

	public RegisterDeviceTask(){
		
	}
	public RegisterDeviceTask(RegisterUserDevice registerUserDevice){
		this.registerUserDevice = registerUserDevice;
	}
	
	public void run() {
		try {
			RegisterUserDeviceResponse registerDeviceResponse = appService.registerDevice(registerUserDevice);

			if (TaskStatus.STATUS_COMPLETED
					.equals(registerDeviceResponse.getStatus()) || TaskStatus.STATUS_ALREADY_REGISTERED
					.equals(registerDeviceResponse.getStatus())) {
				// Now send the message to Client that registration is succesfull
				DeviceRegisteredMessage deviceRegisteredMessage = new DeviceRegisteredMessage();
				deviceRegisteredMessage.setDeviceRegId(registerUserDevice.getRegId());
				
				List<String> deviceList = new ArrayList<String>();
				deviceList.add(registerUserDevice.getRegId());
				messageSender.sendMessage(deviceRegisteredMessage,NotificationMessage.DEVICE_REGISTERED_MESSAGE, deviceList);
				logger.info("Message Sent Succesfully");
			}

		} catch (Exception e) {
			logger.error("Unable to register device {}", registerUserDevice.getRegId(), e);
		}

	}

	public RegisterUserDevice getRegisterUserDevice() {
		return registerUserDevice;
	}

	public void setRegisterUserDevice(RegisterUserDevice registerUserDevice) {
		this.registerUserDevice = registerUserDevice;
	}

}
