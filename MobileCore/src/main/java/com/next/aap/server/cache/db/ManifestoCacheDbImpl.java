package com.next.aap.server.cache.db;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.next.aap.dto.AssemblyConstituencyWithManifesto;
import com.next.aap.dto.ManifestoDto;
import com.next.aap.server.cache.ManifestoCache;
import com.next.aap.util.DataUtil;

@Component
public class ManifestoCacheDbImpl extends BaseDbCache implements ManifestoCache{

	
	private Map<Long, String> manifestoByAcMap;
	private String assemblyConstituencyWithManifestos;
	
	private boolean loadCacheOnStartup;
	
	private boolean cacheLoaded = false;;
	
	@Value("${load.cache.on.startup}")
	public void setDatabaseName(boolean loadCacheOnStartup) {
		this.loadCacheOnStartup = loadCacheOnStartup;
	}

	@PostConstruct
	public void init(){
		if(loadCacheOnStartup){
			refreshCache();
		}
	}
	private boolean alreadyRunning = false;
	/* (non-Javadoc)
	 * @see com.next.aap.server.cache.db.ManifestoCache#refreshCacheAsync()
	 */
	@Override
	public String refreshCacheAsync(){
		
		synchronized (ManifestoCacheDbImpl.class) {
			if(alreadyRunning){
				logger.info("ManifestoCache Refresh Task already running");
				return "Task Already Running";
			}
			Runnable task = new Runnable() {
				
				public void run() {
					refreshCache();
				}
			};
			new Thread(task).start();
		}
		logger.info("ManifestoCache Refresh Task started");
		return "Task Started";
	}
	/* (non-Javadoc)
	 * @see com.next.aap.server.cache.db.ManifestoCache#refreshCache()
	 */
	@Override
	public void refreshCache(){
		logger.info("Refreshing ManifestoCache");
		Map<Long, String> localManifestoByAcMap = new HashMap<Long, String>();
		List<ManifestoDto> allManifestos;
		Map<Long, AssemblyConstituencyWithManifesto> map = new TreeMap<Long, AssemblyConstituencyWithManifesto>();
		try {
			loadAllManifestoAssembliesOfState(DataUtil.delhiStateId, map);
			allManifestos = aapService.getAllManifestos();
			logger.info("Total Number of Manifestos {}", allManifestos.size());
			Gson gson = new Gson();
			AssemblyConstituencyWithManifesto oneAssemblyConstituencyWithManifesto;
			for(ManifestoDto oneManifesto:allManifestos){
				logger.info("{} AC =  {}",oneManifesto.getAssemblyConstituencyId(),oneManifesto);
				localManifestoByAcMap.put(oneManifesto.getAssemblyConstituencyId(), gson.toJson(oneManifesto));
				oneAssemblyConstituencyWithManifesto = map.get(oneManifesto.getAssemblyConstituencyId());
				if(oneAssemblyConstituencyWithManifesto != null){
					oneAssemblyConstituencyWithManifesto.setAvailable(true);	
				}
			}
			manifestoByAcMap = localManifestoByAcMap;
			assemblyConstituencyWithManifestos = gson.toJson(map.values());
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	/* (non-Javadoc)
	 * @see com.next.aap.server.cache.db.ManifestoCache#getManifestoOfAssemblyConstituency(java.lang.Long)
	 */
	@Override
	public String getManifestoOfAssemblyConstituency(Long assemblyContituencyId) {
		return manifestoByAcMap.get(assemblyContituencyId);
	}
	/* (non-Javadoc)
	 * @see com.next.aap.server.cache.db.ManifestoCache#getAcWithManifesto()
	 */
	@Override
	public String getAcWithManifesto() {
		return assemblyConstituencyWithManifestos;
	}

}
