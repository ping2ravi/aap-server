package com.next.aap.server.tasks;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import com.next.aap.dto.RegisterFacebookUserProfile;
import com.next.aap.dto.RegisterUserDevice;
import com.next.aap.dto.RegisterUserDeviceResponse;
import com.next.aap.messages.FacebookUserRegisteredMessage;
import com.next.aap.messages.TaskStatus;

@Component
public class TaskManager implements ApplicationContextAware{

	private ApplicationContext applicationContext;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	@Qualifier(value="httpRequestExecutor")
	private ThreadPoolTaskExecutor threadPoolTaskExecutor;

	/*
	public void runSendMessageTask(String messageType,Object message){
		SendMessageTask sendMessageTask = applicationContext.getBean(SendMessageTask.class);
		sendMessageTask.setMessageType(messageType);
		sendMessageTask.setMessage(message);
		
		threadPoolTaskExecutor.execute(sendMessageTask);
	}*/
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.applicationContext = applicationContext;
		
	}
	
	public RegisterUserDeviceResponse registerUserDeviceTaskAsync(RegisterUserDevice registerUserDevice){
		RegisterUserDeviceResponse registerUserDeviceResponse = new RegisterUserDeviceResponse();
		try{
			RegisterDeviceTask registerDeviceTask = (RegisterDeviceTask)applicationContext.getBean("registerDeviceTask", registerUserDevice);
			threadPoolTaskExecutor.execute(registerDeviceTask);
			registerUserDeviceResponse.setStatus(TaskStatus.STATUS_RUNNING_IN_BACKGROUND);
		}catch(Exception ex){
			registerUserDeviceResponse.setStatus(TaskStatus.STATUS_FAILED);
			logger.error("Unable to start Register User Device Task", ex);
		}
		return registerUserDeviceResponse;
	}
	public FacebookUserRegisteredMessage registerFacebookUserAsync(RegisterFacebookUserProfile registerFacebookUserProfile){
		FacebookUserRegisteredMessage registerFacebookUserResponse = new FacebookUserRegisteredMessage();
		try{
			RegisterFacebookUserTask registerFacebookUserTask = applicationContext.getBean(RegisterFacebookUserTask.class);
			registerFacebookUserTask.setRegisterFacebookUserProfile(registerFacebookUserProfile);
			threadPoolTaskExecutor.execute(registerFacebookUserTask);
			registerFacebookUserResponse.setStatus(TaskStatus.STATUS_RUNNING_IN_BACKGROUND);
		}catch(Exception ex){
			registerFacebookUserResponse.setStatus(TaskStatus.STATUS_FAILED);
			logger.error("Unable to start Register Facebook User Task", ex);
		}
		return registerFacebookUserResponse;
	}
}
