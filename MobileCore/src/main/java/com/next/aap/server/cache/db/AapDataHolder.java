package com.next.aap.server.cache.db;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import com.next.aap.dto.BlogItem;
import com.next.aap.dto.NewsItem;
import com.next.aap.dto.VideoItem;

public class AapDataHolder {

	Map<String,Map<Long, NewsItem>> newsCacheByLanguage = new HashMap<>();
	List<Long> allGlobalNewsIds = new ArrayList<>();
	Map<String,Map<Long, VideoItem>> videoCacheByLanguage = new HashMap<>();
	List<Long> allGlobalVideoIds = new ArrayList<>();
	Map<String,Map<Long, BlogItem>> blogCacheByLanguage = new HashMap<>();
	List<Long> allGlobalBlogIds = new ArrayList<>();
	
	Map<Long, List<Long>> acNewsItems = new HashMap<>();
	Map<Long, List<Long>> acBlogItems = new HashMap<>();
	Map<Long, List<Long>> acVideoItems = new HashMap<>();
	
	Map<Long, List<Long>> pcNewsItems = new HashMap<>();
	Map<Long, List<Long>> pcBlogItems = new HashMap<>();
	Map<Long, List<Long>> pcVideoItems = new HashMap<>();

	Map<Long, Long> longCache = new HashMap<>();

	public void addNews(String language,NewsItem newsItem){
		Map<Long, NewsItem> languageNews = newsCacheByLanguage.get(language);
		if(languageNews == null){
			languageNews = new HashMap<>();
			newsCacheByLanguage.put(language, languageNews);
		}
		languageNews.put(newsItem.getId(), newsItem);
		if(newsItem.isGlobal()){
			allGlobalNewsIds.add(newsItem.getId());	
		}
		
		
	}
	
	public void addVideo(String language,VideoItem videoItem){
		Map<Long, VideoItem> languageVideos = videoCacheByLanguage.get(language);
		if(languageVideos == null){
			languageVideos = new HashMap<>();
			videoCacheByLanguage.put(language, languageVideos);
		}
		languageVideos.put(videoItem.getId(), videoItem);
		if(videoItem.isGlobal()){
			allGlobalVideoIds.add(videoItem.getId());	
		}
		
		
	}
	
	
	public void addBlog(String language,BlogItem blogItem){
		Map<Long, BlogItem> languageBlogs = blogCacheByLanguage.get(language);
		if(languageBlogs == null){
			languageBlogs = new HashMap<>();
			blogCacheByLanguage.put(language, languageBlogs);
		}
		languageBlogs.put(blogItem.getId(), blogItem);
		if(blogItem.isGlobal()){
			allGlobalBlogIds.add(blogItem.getId());	
		}
		
		
	}
	
	private List<Long> convertToCache(List<Long> ids){
		List<Long> returnList = new ArrayList<>(ids.size());
		Long cachedId;
		for(Long oneId:ids){
			cachedId = getCachedLong(oneId);
			returnList.add(cachedId);
		}
		return returnList;
	}
	private Long getCachedLong(Long oneId){
		Long cachedId = longCache.get(oneId);
		if(cachedId == null){
			cachedId = oneId;
			longCache.put(oneId, cachedId);
		}
		return cachedId;

	}
	public void addAcNews(Long acId,List<Long> newsIds){
		acNewsItems.put(acId, convertToCache(newsIds));
	}
	
	public void addAcBlogs(Long blogId,List<Long> blogIds){
		acBlogItems.put(blogId, convertToCache(blogIds));
	}
	public void addAcVideos(Long videoId,List<Long> videoIds){
		acVideoItems.put(videoId, convertToCache(videoIds));
	}
	
	public void addPcNews(Long acId,List<Long> newsIds){
		pcNewsItems.put(acId, convertToCache(newsIds));
	}
	
	public void addPcBlogs(Long blogId,List<Long> blogIds){
		pcBlogItems.put(blogId, convertToCache(blogIds));
	}
	public void addPcVideos(Long videoId,List<Long> videoIds){
		pcVideoItems.put(videoId, convertToCache(videoIds));
	}

	
	public List<NewsItem> getNewsItems(String language, long livingAcId, long votingAcId, long livingPcId, long votingPcId, int pageNumber, int pageSize){
		int startItemCount = (pageNumber - 1) * pageSize;
		int endItemCount = startItemCount + pageSize;
		System.out.println("Getting news item from "+ startItemCount + " , " + endItemCount);
		List<NewsItem> newsItems = new ArrayList<>(pageSize);
		
		List<Long> locationNewsIds =  getItemsForAllLocations(language, livingAcId, votingAcId, livingPcId, votingPcId, acNewsItems, pcNewsItems, allGlobalNewsIds);
		System.out.println("locationNewsIds = "+ locationNewsIds);
		Map<Long, NewsItem> newsItemsByLang = newsCacheByLanguage.get(language);
		
		
		for(int i=startItemCount;i<endItemCount;i++){
			if(locationNewsIds.size() <= i){
				break;
			}
			System.out.println("News item "+ i + " = " + newsItemsByLang.get(locationNewsIds.get(i)));
			newsItems.add(newsItemsByLang.get(locationNewsIds.get(i)));
		}
		return newsItems;
	}
	private List<Long> getNewsItemsForAllLocations(String language, long livingAcId, long votingAcId, long livingPcId, long votingPcId){
		Set<Long> locationNewsIds = new TreeSet<>();
		addNewsItemOfOneLocation(locationNewsIds, acNewsItems.get(livingAcId));
		if(livingAcId != votingAcId){
			addNewsItemOfOneLocation(locationNewsIds, acNewsItems.get(votingAcId));
		}
		addNewsItemOfOneLocation(locationNewsIds, pcNewsItems.get(livingPcId));
		if(livingPcId != votingPcId){
			addNewsItemOfOneLocation(locationNewsIds, pcNewsItems.get(votingPcId));
		}
		
		List<Long> returnList = new ArrayList<>();
		if(locationNewsIds.isEmpty()){
			returnList.addAll(allGlobalNewsIds);
		}
		else{
			returnList.addAll(locationNewsIds);
			Collections.reverse(returnList);
		}
		
		return returnList;
	}
	private void addNewsItemOfOneLocation(Set<Long> locationNewsIds, List<Long> newsIdsToAdd){
		if(newsIdsToAdd != null){
			locationNewsIds.addAll(newsIdsToAdd);
		}
		
	}
	
	public long getTotalNewsItemPages(String language, long livingAcId, long votingAcId, long livingPcId, long votingPcId,int pageSize){
		List<Long> locationNewsIds = getNewsItemsForAllLocations(language, livingAcId, votingAcId, livingPcId, votingPcId);
		return locationNewsIds.size() / pageSize;
	}
	

	public List<VideoItem> getVideoItems(String language, long livingAcId, long votingAcId, long livingPcId, long votingPcId,int pageNumber, int pageSize){
		int startItemCount = (pageNumber - 1) * pageSize;
		int endItemCount = startItemCount + pageSize;
		List<VideoItem> videoItems = new ArrayList<>(pageSize);
		List<Long> acVideoIds = getItemsForAllLocations(language, livingAcId, votingAcId, livingPcId, votingPcId, acVideoItems, pcVideoItems, allGlobalVideoIds);
		if(acVideoIds == null){
			acVideoIds = allGlobalVideoIds;
		}
		Map<Long, VideoItem> videoItemsByLang = videoCacheByLanguage.get(language);
		
		for(int i=startItemCount;i<endItemCount;i++){
			if(acVideoIds.size() <= i){
				break;
			}
			videoItems.add(videoItemsByLang.get(acVideoIds.get(i)));
		}
		return videoItems;
	}
	
	
	private List<Long> getItemsForAllLocations(String language, long livingAcId, long votingAcId, long livingPcId, long votingPcId
			, Map<Long, List<Long>> acItemMap, Map<Long, List<Long>> pcItemMap, List<Long> defaultItemList){
		Set<Long> locationNewsIds = new TreeSet<>();
		addNewsItemOfOneLocation(locationNewsIds, acItemMap.get(livingAcId));
		if(livingAcId != votingAcId){
			addNewsItemOfOneLocation(locationNewsIds, acItemMap.get(votingAcId));
		}
		addNewsItemOfOneLocation(locationNewsIds, pcItemMap.get(livingPcId));
		if(livingPcId != votingPcId){
			addNewsItemOfOneLocation(locationNewsIds, pcItemMap.get(votingPcId));
		}
		
		List<Long> returnList = new ArrayList<>();
		if(locationNewsIds.isEmpty()){
			returnList.addAll(defaultItemList);
		}
		else{
			returnList.addAll(locationNewsIds);
			Collections.reverse(returnList);
		}
		
		return returnList;
	}
	
	
	public List<BlogItem> getBlogItems(String language, long livingAcId, long votingAcId, long livingPcId, long votingPcId, int pageNumber, int pageSize){
		int startItemCount = (pageNumber - 1) * pageSize;
		int endItemCount = startItemCount + pageSize;
		List<BlogItem> blogItems = new ArrayList<>(pageSize);
		List<Long> acBlogIds = getItemsForAllLocations(language, livingAcId, votingAcId, livingPcId, votingPcId, acBlogItems, pcBlogItems, allGlobalBlogIds);
		if(acBlogIds == null){
			acBlogIds = allGlobalBlogIds;
		}
		Map<Long, BlogItem> blogItemsByLang = blogCacheByLanguage.get(language);
		
		for(int i=startItemCount;i<endItemCount;i++){
			if(acBlogIds.size() <= i){
				break;
			}
			blogItems.add(blogItemsByLang.get(acBlogIds.get(i)));
		}
		return blogItems;
	}
	
	public void finishLoading(){
		allGlobalNewsIds = convertToCache(allGlobalNewsIds);
		allGlobalVideoIds = convertToCache(allGlobalVideoIds);
		allGlobalBlogIds = convertToCache(allGlobalBlogIds);
		
	}

}
