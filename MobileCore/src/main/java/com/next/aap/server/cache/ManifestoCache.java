package com.next.aap.server.cache;

public interface ManifestoCache {

	public abstract String refreshCacheAsync();

	public abstract void refreshCache();

	public abstract String getManifestoOfAssemblyConstituency(
			Long assemblyContituencyId);

	public abstract String getAcWithManifesto();

}