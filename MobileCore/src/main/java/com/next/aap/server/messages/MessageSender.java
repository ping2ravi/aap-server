package com.next.aap.server.messages;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Message.Builder;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Sender;
import com.google.gson.Gson;
import com.next.aap.messages.NotificationMessage;

@Component
public class MessageSender {

	private static String key = "AIzaSyD5op2kuyCJmXNGWB7rjrh94zShmC6gVw0";
	//private static String key = "AIzaSyC5zoNmY0IP-pe-hTBpVyeafXObMqdNeH8";
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	/**
	 * @param args
	 * @throws XMPPException 
	 * @throws IOException 
	 */
	public static void main(String[] args) throws  IOException {
		Sender sender = new Sender(key);
		Builder builder = new Message.Builder();
		builder.addData("msg", "Ravi its working");
		Message message = builder.build();
		
		List<String> deviceList = new ArrayList<String>();
		deviceList.add("APA91bF7jWFHMIxcgjZ662qxuBKUO0DNCYjJXdIv6_d0CULxQNtI-zyRNB0dCMn98G_ic3NgAbF1SQBUeI61YhZcxoV8W6Ch_9wPJwKfpEQ2risZkKtOTn9Zs3dKvV5QuTddaIXuLgCGTlB68kMgHT_TWivwJH5DuQ");
		System.out.println("message="+message);
		System.out.println("deviceList="+deviceList);
		MulticastResult result = sender.send(message, deviceList, 5);
		System.out.println(result);
		
	}
	@Deprecated
	public void sendMessage(String msg,List<String> deviceList) throws IOException{
		if(deviceList == null || deviceList.isEmpty()){
			return;
		}
		Sender sender = new Sender(key);

		Builder builder = new Message.Builder();
		builder.addData("msg", msg);
		Message message = builder.build();
		
		//List<String> deviceList = new ArrayList<String>();
		//deviceList.add("APA91bE5AdKI_oOJiSfn031gBzYVHo7V823zDEi0OulfLrBka80WQs3dFTnr1mPhAL_qQ9CmZznLQJRDm27XVNOcC0ppSDmmLvTsMvVimgVUlgEViBSz2ajpJalemp4wf9jcV1Z4WOAoI2xlSaeBq0r6zkFKo-ZRyg");
		MulticastResult result = sender.send(message, deviceList, 5);
		
		logger.info("Message Send Result = {} ", result);
		
	}
	
	public <T> void sendMessage(Object notificationMessage,String messageType,List<String> deviceList) throws IOException{
		if(deviceList == null || deviceList.isEmpty()){
			return;
		}
		Sender sender = new Sender(key);
		Gson gson = new Gson();
		String msg = gson.toJson(notificationMessage);
		logger.info("Sending message : {},{}",messageType, msg);

		Builder builder = new Message.Builder();
		builder.addData(NotificationMessage.MESSAGE, msg);
		builder.addData(NotificationMessage.MESSAGE_TYPE, messageType);
		Message message = builder.build();
		
		//List<String> deviceList = new ArrayList<String>();
		//deviceList.add("APA91bE5AdKI_oOJiSfn031gBzYVHo7V823zDEi0OulfLrBka80WQs3dFTnr1mPhAL_qQ9CmZznLQJRDm27XVNOcC0ppSDmmLvTsMvVimgVUlgEViBSz2ajpJalemp4wf9jcV1Z4WOAoI2xlSaeBq0r6zkFKo-ZRyg");
		MulticastResult result = sender.send(message, deviceList, 5);
		
		logger.info("Message Send Result = {} ", result);
		
	}
	

}
