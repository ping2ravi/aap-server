package com.next.aap.server.persistance.dao.impl;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.stereotype.Component;

import com.next.aap.server.persistance.Manifesto;
import com.next.aap.server.persistance.dao.ManifestoDao;

@Component
public class ManifestoDaoHibernateSpringImpl extends BaseDaoHibernateSpring<Manifesto> implements ManifestoDao {

	/* (non-Javadoc)
	 * @see com.next.aap.server.persistance.dao.impl.ManifestoDao#saveManifesto(com.next.aap.server.persistance.Manifesto)
	 */
	@Override
	public Manifesto saveManifesto(Manifesto manifesto)
	{
		checkIfObjectMissing("AssemblyConstituency", manifesto.getAssemblyConstituency());
		manifesto = super.saveObject(manifesto);
		return manifesto;
	}

	/* (non-Javadoc)
	 * @see com.next.aap.server.persistance.dao.impl.ManifestoDao#deleteManifesto(com.next.aap.server.persistance.Manifesto)
	 */
	@Override
	public void deleteManifesto(Manifesto manifesto)
	{
		super.deleteObject(manifesto);
	}

	/* (non-Javadoc)
	 * @see com.next.aap.server.persistance.dao.impl.ManifestoDao#getManifestoById(java.lang.Long)
	 */
	@Override
	public Manifesto getManifestoById(Long id)
	{
		Manifesto manifesto = super.getObjectById(Manifesto.class, id);
		return manifesto;
	}

	/* (non-Javadoc)
	 * @see com.next.aap.server.persistance.dao.impl.ManifestoDao#getAllManifestos()
	 */
	@Override
	public List<Manifesto> getAllManifestos()
	{
		List<Manifesto> list = executeQueryGetList("from Manifesto");
		return list;
	}
	/* (non-Javadoc)
	 * @see com.next.aap.server.persistance.dao.impl.ManifestoDao#getManifestoOfAssemblyConstituency(long)
	 */
	@Override
	public Manifesto getManifestoOfAssemblyConstituency(long assemblyConstituencyId){
		Map<String, Object> params = new TreeMap<String, Object>();
		params.put("assemblyConstituencyId", assemblyConstituencyId);
		Manifesto manifesto = executeQueryGetObject("from Manifesto where assemblyConstituencyId = :assemblyConstituencyId", params);
		return manifesto;
	}

}