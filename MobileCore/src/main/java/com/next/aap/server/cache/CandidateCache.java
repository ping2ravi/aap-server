package com.next.aap.server.cache;

public interface CandidateCache {

	public abstract String refreshCacheAsync();

	public abstract void refreshCache();

	public abstract String getAcWithCandidates();

}