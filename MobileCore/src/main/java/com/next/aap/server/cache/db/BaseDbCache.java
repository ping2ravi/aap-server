package com.next.aap.server.cache.db;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.next.aap.dto.AssemblyConstituencyDto;
import com.next.aap.dto.AssemblyConstituencyWithCandidate;
import com.next.aap.dto.AssemblyConstituencyWithManifesto;
import com.next.aap.dto.DistrictDto;
import com.next.aap.server.service.AapService;

public class BaseDbCache {

	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	protected AapService aapService;

	protected void loadAllManifestoAssembliesOfState(long stateId,Map<Long, AssemblyConstituencyWithManifesto> map){
		List<DistrictDto> allDistricts = aapService.getAllDistrictOfState(stateId);
		//Create Assembly COnstituency Cache
		List<AssemblyConstituencyDto> oneDistrictAcs;
		for(DistrictDto oneDistrictDto:allDistricts){
			oneDistrictAcs = aapService.getAllAssemblyConstituenciesOfDistrict(oneDistrictDto.getId());
			for(AssemblyConstituencyDto oneAc:oneDistrictAcs){
				map.put(oneAc.getId(), new AssemblyConstituencyWithManifesto(oneAc));
			}
		}
		

	}
	
	protected void loadAllCandidateAssembliesOfState(long stateId,Map<Long, AssemblyConstituencyWithCandidate> map){
		List<DistrictDto> allDistricts = aapService.getAllDistrictOfState(stateId);
		//Create Assembly COnstituency Cache
		List<AssemblyConstituencyDto> oneDistrictAcs;
		for(DistrictDto oneDistrictDto:allDistricts){
			oneDistrictAcs = aapService.getAllAssemblyConstituenciesOfDistrict(oneDistrictDto.getId());
			for(AssemblyConstituencyDto oneAc:oneDistrictAcs){
				map.put(oneAc.getId(), new AssemblyConstituencyWithCandidate(oneAc));
			}
		}
		

	}
}
