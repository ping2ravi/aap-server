package com.next.aap.server.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gdata.util.common.base.StringUtil;
import com.next.aap.dto.AssemblyConstituencyDto;
import com.next.aap.dto.AssemblyConstituencyWeb;
import com.next.aap.dto.AssemblyConstituencyWithCandidate;
import com.next.aap.dto.BlogItem;
import com.next.aap.dto.CandidateDto;
import com.next.aap.dto.DistrictDto;
import com.next.aap.dto.DistrictWeb;
import com.next.aap.dto.ManifestoDto;
import com.next.aap.dto.NewsItem;
import com.next.aap.dto.ParliamentConstituencyDto;
import com.next.aap.dto.RegisterFacebookUserProfile;
import com.next.aap.dto.RegisterGoogleUserProfile;
import com.next.aap.dto.RegisterTwitterUserProfile;
import com.next.aap.dto.RegisterUserDevice;
import com.next.aap.dto.RegisterUserDeviceResponse;
import com.next.aap.dto.RegisterUserProfile;
import com.next.aap.dto.StateDto;
import com.next.aap.dto.UserDeviceDto;
import com.next.aap.dto.VideoItem;
import com.next.aap.messages.FacebookUserRegisteredMessage;
import com.next.aap.messages.GoogleUserRegisteredMessage;
import com.next.aap.messages.TaskStatus;
import com.next.aap.messages.TwitterUserRegisteredMessage;
import com.next.aap.messages.UserRegisteredMessage;
import com.next.aap.server.persistance.AssemblyConstituency;
import com.next.aap.server.persistance.Blog;
import com.next.aap.server.persistance.Candidate;
import com.next.aap.server.persistance.District;
import com.next.aap.server.persistance.FacebookAccount;
import com.next.aap.server.persistance.GoogleAccount;
import com.next.aap.server.persistance.Manifesto;
import com.next.aap.server.persistance.News;
import com.next.aap.server.persistance.ParliamentConstituency;
import com.next.aap.server.persistance.State;
import com.next.aap.server.persistance.Tweet;
import com.next.aap.server.persistance.TwitterAccount;
import com.next.aap.server.persistance.User;
import com.next.aap.server.persistance.UserDevice;
import com.next.aap.server.persistance.Video;
import com.next.aap.server.persistance.dao.AssemblyConstituencyDao;
import com.next.aap.server.persistance.dao.BlogDao;
import com.next.aap.server.persistance.dao.CandidateDao;
import com.next.aap.server.persistance.dao.DistrictDao;
import com.next.aap.server.persistance.dao.FacebookAccountDao;
import com.next.aap.server.persistance.dao.GoogleAccountDao;
import com.next.aap.server.persistance.dao.ManifestoDao;
import com.next.aap.server.persistance.dao.NewsDao;
import com.next.aap.server.persistance.dao.ParliamentConstituencyDao;
import com.next.aap.server.persistance.dao.StateDao;
import com.next.aap.server.persistance.dao.TwitterAccountDao;
import com.next.aap.server.persistance.dao.UserDao;
import com.next.aap.server.persistance.dao.UserDeviceDao;
import com.next.aap.server.persistance.dao.VideoDao;

@Service
public class AapServiceImpl implements AapService, ApplicationContextAware {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	private ApplicationContext applicationContext;

	@Autowired
	private UserDao userDao;
	@Autowired
	private UserDeviceDao userDeviceDao;
	@Autowired
	private FacebookAccountDao facebookAccountDao;
	@Autowired
	private TwitterAccountDao twitterAccountDao;
	@Autowired
	private GoogleAccountDao googleAccountDao;
	@Autowired
	private StateDao stateDao;
	@Autowired
	private DistrictDao districtDao;
	@Autowired
	private AssemblyConstituencyDao assemblyConstituencyDao;
	@Autowired
	private ParliamentConstituencyDao parliamentConstituencyDao;
	@Autowired
	private CandidateDao candidateDao;
	@Autowired
	private ManifestoDao manifestoDao;
	@Autowired
	private NewsDao newsDao;
	@Autowired
	private VideoDao videoDao;
	@Autowired
	private BlogDao blogDao;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

	@Override
	@Transactional
	public User saveUser(User user) {
		return userDao.saveUser(user);
	}

	@Override
	@Transactional
	public User getUserById(Long id) {
		User user = userDao.getUserById(id);
		System.out.println(user.getId());
		return user;
	}

	@Override
	@Transactional
	public RegisterUserDeviceResponse registerDevice(
			RegisterUserDevice registerUserDevice) {
		RegisterUserDeviceResponse registerDeviceResponse = new RegisterUserDeviceResponse();
		if(StringUtil.isEmpty(registerUserDevice.getRegId())){
			registerDeviceResponse.setStatus(TaskStatus.STATUS_FAILED);
			return registerDeviceResponse;
		}
		UserDevice userDevice = null;
		if (StringUtil.isEmpty(registerUserDevice.getOldRegId())) {
			userDevice = userDeviceDao.getUserDeviceByRegId(
					registerUserDevice.getOldRegId(),
					registerUserDevice.getDeviceType());
		}

		if (userDevice == null) {
			userDevice = userDeviceDao.getUserDeviceByRegId(
					registerUserDevice.getRegId(),
					registerUserDevice.getDeviceType());
		}
		if (userDevice == null) {
			userDevice = new UserDevice();
			userDevice.setDateCreated(new Date());
			userDevice.setDeviceType(registerUserDevice.getDeviceType());
			registerDeviceResponse.setStatus(TaskStatus.STATUS_COMPLETED);
			logger.info("User Device Saved Succesfully");

		} else {
			registerDeviceResponse.setStatus(TaskStatus.STATUS_ALREADY_REGISTERED);
			logger.info("User Device already registered");
		}
		userDevice.setRegId(registerUserDevice.getRegId());
		userDevice.setDateModified(new Date());
		userDevice = userDeviceDao.saveUserDevice(userDevice);

		return registerDeviceResponse;
	}

	@Override
	@Transactional
	public FacebookUserRegisteredMessage registerFacebookUser(
			RegisterFacebookUserProfile registerFacebookUserProfile) {
		if(StringUtil.isEmpty(registerFacebookUserProfile.getFacebookUserId())){
			throw new RuntimeException("FacebookUserId can not be null");
		}
		if(StringUtil.isEmpty(registerFacebookUserProfile.getUserName())){
			throw new RuntimeException("Facebook UserName can not be null");
		}
		if(StringUtil.isEmpty(registerFacebookUserProfile.getEmail())){
			throw new RuntimeException("Email can not be null");
		}
		// First register User Device
		UserDevice userDevice = null;
		if(!StringUtil.isEmpty(registerFacebookUserProfile.getDeviceRegId())){
			userDevice = userDeviceDao.getUserDeviceByRegId(
					registerFacebookUserProfile.getDeviceRegId(),
					registerFacebookUserProfile.getDeviceType());
		}
		// find out if we have facebook account with given user name
		FacebookAccount facebookAccount = facebookAccountDao.getFacebookAccountByFacebookUserId(registerFacebookUserProfile.getFacebookUserId());
		User user = null;
		if(registerFacebookUserProfile.getUserId() != null && registerFacebookUserProfile.getUserId() > 0){
			user = userDao.getUserById(registerFacebookUserProfile.getUserId());
		}
		if(!StringUtil.isEmpty(registerFacebookUserProfile.getEmail())){
			user = userDao.getUserByEmail(registerFacebookUserProfile.getEmail());
		}
		if (facebookAccount == null) {
			facebookAccount = new FacebookAccount();
			facebookAccount.setDateCreated(new Date());
			facebookAccount.setDateModified(new Date());
			facebookAccount.setToken(registerFacebookUserProfile
					.getAccessToken());
			facebookAccount.setUserName(registerFacebookUserProfile.getUserName());
			facebookAccount.setFacebookUserId(registerFacebookUserProfile.getFacebookUserId());

		} else {
			facebookAccount.setDateModified(new Date());
			facebookAccount.setToken(registerFacebookUserProfile
					.getAccessToken());
			facebookAccount.setUserName(registerFacebookUserProfile
					.getUserName());

			user = facebookAccount.getUser();
		}
		if(user == null){
			user = new User();
			user.setDateCreated(new Date());
			user.setName(facebookAccount.getUserName());
		}
		user.setDateModified(new Date());
		user.setEmail(registerFacebookUserProfile.getEmail());
		
		if(user.getUserDevices() == null){
			user.setUserDevices(new HashSet<UserDevice>());	
		}
		
		if(userDevice != null){
			user.getUserDevices().add(userDevice);	
		}
		

		user = userDao.saveUser(user);
		facebookAccount.setUser(user);

		facebookAccount = facebookAccountDao.saveFacebookAccount(facebookAccount);
		if(userDevice != null){
			if (userDevice.getUsers() == null) {
				userDevice.setUsers(new HashSet<User>());
			}
			userDevice = userDeviceDao.saveUserDevice(userDevice);
		}

		FacebookUserRegisteredMessage facebookUserRegisteredMessage = new FacebookUserRegisteredMessage();
		facebookUserRegisteredMessage
				.setDeviceRegId(registerFacebookUserProfile.getDeviceRegId());
		facebookUserRegisteredMessage.setUserId(user.getId());
		facebookUserRegisteredMessage.setStatus(TaskStatus.STATUS_COMPLETED);
		return facebookUserRegisteredMessage;
	}
	
	@Override
	@Transactional
	public TwitterUserRegisteredMessage registerTwitterUser(
			RegisterTwitterUserProfile registerTwitterUserProfile) {
		if(StringUtil.isEmpty(registerTwitterUserProfile.getTwitterUserId())){
			throw new RuntimeException("Twitter User Id can not be null");
		}
		if(StringUtil.isEmpty(registerTwitterUserProfile.getHandle())){
			throw new RuntimeException("Twitter Handle can not be null");
		}
		// First register User Device
		UserDevice userDevice = null;
		if(!StringUtil.isEmpty(registerTwitterUserProfile.getDeviceRegId())){
			userDevice = userDeviceDao.getUserDeviceByRegId(
					registerTwitterUserProfile.getDeviceRegId(),
					registerTwitterUserProfile.getDeviceType());
		}
		// find out if we have twitter account with given twitter handle
		TwitterAccount twitterAccount = twitterAccountDao.getTwitterAccountByHandle(registerTwitterUserProfile.getHandle());
		if(twitterAccount == null){
			twitterAccount = twitterAccountDao.getTwitterAccountByTwitterUserId(registerTwitterUserProfile.getTwitterUserId());
		}
		User user = null;
		if(registerTwitterUserProfile.getUserId() != null && registerTwitterUserProfile.getUserId() > 0){
			user = userDao.getUserById(registerTwitterUserProfile.getUserId());
		}

		if (twitterAccount == null) {
			twitterAccount = new TwitterAccount();
			twitterAccount.setDateCreated(new Date());
		} else {
			user = twitterAccount.getUser();
		}
		twitterAccount.setDateModified(new Date());
		twitterAccount.setTwitterId(registerTwitterUserProfile.getTwitterUserId());
		twitterAccount.setToken(registerTwitterUserProfile.getAccessToken());
		twitterAccount.setScreenName(registerTwitterUserProfile.getHandle());
		twitterAccount.setTokenSecret(registerTwitterUserProfile.getAccessTokenSecret());

		if(user == null){
			user = new User();
			user.setDateCreated(new Date());
			user.setName(registerTwitterUserProfile.getHandle());
		}
		user.setDateModified(new Date());
		if(user.getUserDevices() == null){
			user.setUserDevices(new HashSet<UserDevice>());
		}
		if(userDevice != null){
			user.getUserDevices().add(userDevice);
		}

		user = userDao.saveUser(user);
		twitterAccount.setUser(user);

		twitterAccount = twitterAccountDao.saveTwitterAccount(twitterAccount);
		
		if(userDevice != null){
			if (userDevice.getUsers() == null) {
				userDevice.setUsers(new HashSet<User>());
			}

			userDevice.getUsers().add(user);
			userDevice = userDeviceDao.saveUserDevice(userDevice);
		}

		TwitterUserRegisteredMessage twitterUserRegisteredMessage = new TwitterUserRegisteredMessage();
		twitterUserRegisteredMessage.setDeviceRegId(registerTwitterUserProfile
				.getDeviceRegId());
		twitterUserRegisteredMessage.setUserId(user.getId());
		twitterUserRegisteredMessage
				.setTwitterUserId(registerTwitterUserProfile.getHandle());
		twitterUserRegisteredMessage.setStatus(TaskStatus.STATUS_COMPLETED);
		return twitterUserRegisteredMessage;
	}
	
	@Override
	@Transactional
	public GoogleUserRegisteredMessage registerGoogleUser(
			RegisterGoogleUserProfile registerGoogleUserProfile) {
		if(StringUtil.isEmpty(registerGoogleUserProfile.getGoogleUserId())){
			throw new RuntimeException("Google User Id can not be null");
		}
		if(StringUtil.isEmpty(registerGoogleUserProfile.getAccessToken())){
			throw new RuntimeException("Google Access Token can not be null");
		}
		if(StringUtil.isEmpty(registerGoogleUserProfile.getEmail())){
			throw new RuntimeException("Google Email can not be null");
		}
		if(StringUtil.isEmpty(registerGoogleUserProfile.getName())){
			throw new RuntimeException("Google Name can not be null");
		}
		// First register User Device
		UserDevice userDevice = null;
		if(!StringUtil.isEmpty(registerGoogleUserProfile.getDeviceRegId())){
			userDevice = userDeviceDao.getUserDeviceByRegId(
					registerGoogleUserProfile.getDeviceRegId(),
					registerGoogleUserProfile.getDeviceType());
		}
		// find out if we have facebook account with given user name
		GoogleAccount googleAccount = googleAccountDao.getGoogleAccountByGoogleId(registerGoogleUserProfile.getGoogleUserId());
		User user = null;
		if (googleAccount == null) {
			googleAccount = new GoogleAccount();
			googleAccount.setDateCreated(new Date());

		} else {
			user = googleAccount.getUser();
		}
		googleAccount.setDateModified(new Date());
		googleAccount.setToken(registerGoogleUserProfile.getAccessToken());
		googleAccount.setEmail(registerGoogleUserProfile.getEmail());
		googleAccount.setGoogleUserId(registerGoogleUserProfile.getGoogleUserId());
		
		if(user == null){
			if(registerGoogleUserProfile.getUserId() != null && registerGoogleUserProfile.getUserId() > 0){
				user = userDao.getUserById(registerGoogleUserProfile.getUserId());
			}
		}
		if(user == null){
			user = userDao.getUserByEmail(registerGoogleUserProfile.getEmail());
		}
		if(user == null){
			user = new User();
			user.setDateCreated(new Date());
			user.setName(registerGoogleUserProfile.getName());
			user.setEmail(registerGoogleUserProfile.getEmail());
		}
		user.setDateModified(new Date());
		if(user.getUserDevices() == null){
			user.setUserDevices(new HashSet<UserDevice>());
		}
		if(userDevice != null){
			user.getUserDevices().add(userDevice);
		}

		
		user = userDao.saveUser(user);
		googleAccount.setUser(user);

		googleAccount = googleAccountDao.saveGoogleAccount(googleAccount);
		if (userDevice != null){

			if (userDevice.getUsers() == null) {
				userDevice.setUsers(new HashSet<User>());
			}
		
			userDevice.getUsers().add(user);
			userDevice = userDeviceDao.saveUserDevice(userDevice);
		}
		GoogleUserRegisteredMessage googleUserRegisteredMessage = new GoogleUserRegisteredMessage();
		googleUserRegisteredMessage
				.setDeviceRegId(registerGoogleUserProfile.getDeviceRegId());
		googleUserRegisteredMessage.setUserId(user.getId());
		googleUserRegisteredMessage.setStatus(TaskStatus.STATUS_COMPLETED);
		return googleUserRegisteredMessage;
	}
	
	@Override
	@Transactional
	public UserRegisteredMessage registerUser(
			RegisterUserProfile registerUserProfile) {
		// First register User Device
		UserDevice userDevice = null;
		if(!StringUtil.isEmpty(registerUserProfile.getDeviceRegId())){
			userDevice = userDeviceDao.getUserDeviceByRegId(
					registerUserProfile.getDeviceRegId(),
					registerUserProfile.getDeviceType());
			
		}
		// find out if we have facebook account with given user name
		User user = null;
		if(registerUserProfile.getUserId() != null && registerUserProfile.getUserId() > 0){
			user = userDao.getUserById(registerUserProfile.getUserId());
		}
		if(user == null){
			user = userDao.getUserByEmail(registerUserProfile.getEmail());
		}
		if(user == null){
			user = new User();
			user.setDateCreated(new Date());
		}
		user.setDateModified(new Date());
		user.setEmail(registerUserProfile.getEmail());
		user.setMobile(registerUserProfile.getMobile());
		user.setName(registerUserProfile.getName());
		user.setVotingState(registerUserProfile.getVotingState());
		user.setLivingState(registerUserProfile.getLivingState());
		if(user.getUserDevices() == null){
			user.setUserDevices(new HashSet<UserDevice>());	
		}
		if(userDevice != null){
			user.getUserDevices().add(userDevice);
		}

		user = userDao.saveUser(user);
		if (registerUserProfile.getLivingStateId() != null
				&& registerUserProfile.getLivingStateId() > 0) {
			State stateLiving = stateDao
					.getStateById(registerUserProfile
							.getLivingStateId());
			if(userDevice != null){
				userDevice.setStateLiving(stateLiving);
			}
			user.setStateLiving(stateLiving);
		}
		if (registerUserProfile.getLivingDistrictId() != null
				&& registerUserProfile.getLivingDistrictId() > 0) {
			District districtLiving = districtDao
					.getDistrictById(registerUserProfile
							.getLivingDistrictId());
			if(userDevice != null){
				userDevice.setDistrictLiving(districtLiving);
			}
			user.setDistrictLiving(districtLiving);
		}
		if (registerUserProfile.getLivingAcId() != null
				&& registerUserProfile.getLivingAcId() > 0) {
			AssemblyConstituency assemblyConstituencyLiving = assemblyConstituencyDao
					.getAssemblyConstituencyById(registerUserProfile
							.getLivingAcId());
			if(userDevice != null){
				userDevice
					.setAssemblyConstituencyLiving(assemblyConstituencyLiving);
			}
			user.setAssemblyConstituencyLiving(assemblyConstituencyLiving);
		}
		
		if (registerUserProfile.getLivingPcId() != null
				&& registerUserProfile.getLivingPcId() > 0) {
			ParliamentConstituency parliamentConstituencyLiving = parliamentConstituencyDao
					.getParliamentConstituencyById(registerUserProfile
							.getLivingPcId());
			if(userDevice != null){
				userDevice.setParliamentConstituencyLiving(parliamentConstituencyLiving);
			}
			user.setParliamentConstituencyLiving(parliamentConstituencyLiving);
		}
		
		if (registerUserProfile.getVotingStateId() != null
				&& registerUserProfile.getVotingStateId() > 0) {
			State stateVoting = stateDao
					.getStateById(registerUserProfile
							.getVotingStateId());
			if(userDevice != null){
				userDevice.setStateVoting(stateVoting);
			}
			user.setStateVoting(stateVoting);
		}
		if (registerUserProfile.getVotingDistrictId() != null
				&& registerUserProfile.getVotingDistrictId() > 0) {
			District districtVoting = districtDao
					.getDistrictById(registerUserProfile
							.getVotingDistrictId());
			if(userDevice != null){
				userDevice.setDistrictVoting(districtVoting);
			}
			user.setDistrictVoting(districtVoting);
		}
		if (registerUserProfile.getVotingAcId() != null
				&& registerUserProfile.getVotingAcId() > 0) {
			AssemblyConstituency assemblyConstituencyVoting = assemblyConstituencyDao
					.getAssemblyConstituencyById(registerUserProfile
							.getVotingAcId());
			if(userDevice != null){
				userDevice
					.setAssemblyConstituencyVoting(assemblyConstituencyVoting);
			}
			user.setAssemblyConstituencyVoting(assemblyConstituencyVoting);
		}
		if (registerUserProfile.getVotingPcId() != null
				&& registerUserProfile.getVotingPcId() > 0) {
			ParliamentConstituency parliamentConstituencyVoting = parliamentConstituencyDao
					.getParliamentConstituencyById(registerUserProfile
							.getVotingPcId());
			if(userDevice != null){
				userDevice.setParliamentConstituencyVoting(parliamentConstituencyVoting);
			}
			user.setParliamentConstituencyVoting(parliamentConstituencyVoting);
		}

		if(userDevice != null){
			userDevice = userDeviceDao.saveUserDevice(userDevice);
		}
		

		UserRegisteredMessage userRegisteredMessage = new UserRegisteredMessage();
		userRegisteredMessage
				.setDeviceRegId(registerUserProfile.getDeviceRegId());
		userRegisteredMessage.setUserId(user.getId());
		userRegisteredMessage.setStatus(TaskStatus.STATUS_COMPLETED);
		return userRegisteredMessage;
	}

	

	@Override
	@Transactional
	public List<StateDto> getAllStates() {
		List<State> allStates = stateDao.getAllStates();
		List<StateDto> returnList = new ArrayList<StateDto>();
		for (State oneState : allStates) {
			returnList.add(convertState(oneState));
		}
		return returnList;
	}

	private StateDto convertState(State oneState) {
		if(oneState == null){
			return null;
		}
		StateDto oneStateDto = new StateDto();
		oneStateDto.setId(oneState.getId());
		oneStateDto.setName(oneState.getName());
		oneStateDto.setDistrictDataAvailable(oneState
				.getDistrictDataAvailable());
		return oneStateDto;
	}

	@Override
	@Transactional
	public List<DistrictDto> getAllDistrictOfState(long stateId) {
		List<District> allDistricts = districtDao.getDistrictOfState(stateId);
		List<DistrictDto> returnList = new ArrayList<DistrictDto>();
		for (District oneDistrict : allDistricts) {
			returnList.add(convertDistrict(oneDistrict));
		}
		return returnList;
	}

	private DistrictDto convertDistrict(District oneDistrict) {
		if(oneDistrict == null){
			return null;
		}
		DistrictDto oneDistrictDto = new DistrictDto();
		oneDistrictDto.setId(oneDistrict.getId());
		oneDistrictDto.setName(oneDistrict.getName());
		oneDistrictDto.setAcDataAvailable(oneDistrict.getAcDataAvailable());
		return oneDistrictDto;
	}

	@Override
	@Transactional
	public List<AssemblyConstituencyDto> getAllAssemblyConstituenciesOfDistrict(
			long districtId) {
		List<AssemblyConstituency> allAssemblyConstituencies = assemblyConstituencyDao
				.getAssemblyConstituencyOfDistrict(districtId);
		return convertList(allAssemblyConstituencies);
	}
	
	
	private List<AssemblyConstituencyDto> convertList(List<AssemblyConstituency> allAssemblyConstituencies){
		List<AssemblyConstituencyDto> returnList = new ArrayList<AssemblyConstituencyDto>();
		if(allAssemblyConstituencies == null){
			return returnList;	
		}
		for (AssemblyConstituency oneAssemblyConstituency : allAssemblyConstituencies) {
			returnList
					.add(convertAssemblyConstituency(oneAssemblyConstituency));
		}
		return returnList;
	}

	private AssemblyConstituencyDto convertAssemblyConstituency(
			AssemblyConstituency oneAssemblyConstituency) {
		AssemblyConstituencyDto oneAssemblyConstituencyDto = new AssemblyConstituencyDto();
		oneAssemblyConstituencyDto.setId(oneAssemblyConstituency.getId());
		oneAssemblyConstituencyDto.setName(oneAssemblyConstituency.getName());
		return oneAssemblyConstituencyDto;

	}

	@Override
	@Transactional
	public StateDto saveState(StateDto stateDto) {
		State dbState = null;
		if (stateDto.getId() != null && stateDto.getId() > 0) {
			dbState = stateDao.getStateById(stateDto.getId());
			if (dbState == null) {
				throw new RuntimeException("No such states exist[id="
						+ stateDto.getId() + "]");
			}
		}
		System.out.println("Getting State By Name : "+ stateDto.getName());
		if (dbState == null) {
			dbState = stateDao.getStateByName(stateDto.getName());
		}

		if (dbState == null) {
			dbState = new State();
			dbState.setDateCreated(new Date());
		}
		if(dbState.getDateCreated() == null ){
			dbState.setDateCreated(new Date());
		}
		
		dbState.setDateModified(new Date());

		dbState.setName(stateDto.getName());
		dbState = stateDao.saveState(dbState);

		stateDto.setId(dbState.getId());
		return stateDto;
	}

	@Override
	@Transactional
	public DistrictWeb saveDistrict(DistrictWeb districtWeb) {
		District dbDistrict;
		if (districtWeb.getId() == null || districtWeb.getId() <= 0) {
			dbDistrict = districtDao.getDistrictByNameAndStateId(
					districtWeb.getStateId(), districtWeb.getName());
			if (dbDistrict == null) {
				dbDistrict = new District();
				dbDistrict.setDateCreated(new Date());
			}
		} else {
			dbDistrict = districtDao.getDistrictById(districtWeb.getId());
			if (dbDistrict == null) {
				throw new RuntimeException("No such District exist[id="
						+ districtWeb.getId() + "]");
			}
		}
		
		if(dbDistrict.getDateCreated() == null ){
			dbDistrict.setDateCreated(new Date());
		}
		dbDistrict.setName(districtWeb.getName());
		dbDistrict.setDateModified(new Date());
		
		
		State state = stateDao.getStateById(districtWeb.getStateId());
		dbDistrict.setState(state);

		dbDistrict = districtDao.saveDistrict(dbDistrict);

		districtWeb.setId(dbDistrict.getId());
		return districtWeb;
	}

	@Override
	@Transactional
	public List<DistrictWeb> saveDistricts(List<DistrictWeb> districtList) {
		List<DistrictWeb> returnList = new ArrayList<DistrictWeb>();
		for (DistrictWeb oneDistrictWeb : districtList) {
			returnList.add(saveDistrict(oneDistrictWeb));
		}
		return returnList;
	}

	@Override
	@Transactional
	public AssemblyConstituencyWeb saveAssemblyConstituency(
			AssemblyConstituencyWeb assemblyConstituencyWeb) {
		AssemblyConstituency dbAssemblyConstituency;
		if (assemblyConstituencyWeb.getId() == null
				|| assemblyConstituencyWeb.getId() <= 0) {
			dbAssemblyConstituency = assemblyConstituencyDao
					.getAssemblyConstituencyNameAndDistrictId(
							assemblyConstituencyWeb.getDistrictId(),
							assemblyConstituencyWeb.getName());
			if (dbAssemblyConstituency == null) {
				dbAssemblyConstituency = new AssemblyConstituency();
				dbAssemblyConstituency.setDateCreated(new Date());
			}
		} else {
			dbAssemblyConstituency = assemblyConstituencyDao
					.getAssemblyConstituencyById(assemblyConstituencyWeb
							.getId());
			if (dbAssemblyConstituency == null) {
				throw new RuntimeException(
						"No such Assembly Constituency exist[id="
								+ assemblyConstituencyWeb.getId() + "]");
			}
		}
		dbAssemblyConstituency.setDateModified(new Date());
		dbAssemblyConstituency.setName(assemblyConstituencyWeb.getName());
		District district = districtDao.getDistrictById(assemblyConstituencyWeb
				.getDistrictId());
		district.setAcDataAvailable(true);
		dbAssemblyConstituency.setDistrict(district);

		dbAssemblyConstituency = assemblyConstituencyDao
				.saveAssemblyConstituency(dbAssemblyConstituency);

		assemblyConstituencyWeb.setId(dbAssemblyConstituency.getId());
		return assemblyConstituencyWeb;
	}

	@Override
	@Transactional
	public List<AssemblyConstituencyWeb> saveAssemblyConstituencies(
			List<AssemblyConstituencyWeb> assemblyConstituencyList) {
		List<AssemblyConstituencyWeb> returnList = new ArrayList<AssemblyConstituencyWeb>();
		for (AssemblyConstituencyWeb oneAssemblyConstituencyWeb : assemblyConstituencyList) {
			returnList
					.add(saveAssemblyConstituency(oneAssemblyConstituencyWeb));
		}
		return returnList;
	}

	@Override
	@Transactional
	public CandidateDto saveCandidate(CandidateDto candidateDto) {
		Candidate dbCandidate = null;
		if (candidateDto.getId() != null && candidateDto.getId() > 0) {
			dbCandidate = candidateDao.getCandidateById(candidateDto.getId());
			if (dbCandidate == null) {
				throw new RuntimeException("No candidate found with id "
						+ candidateDto.getId());
			}
		} else {
			List<Candidate> existingCandidateOfAc = candidateDao
					.getCandidateOfAssemblyConstituency(candidateDto
							.getAssemblyConstituencyId());
			if (existingCandidateOfAc != null
					&& existingCandidateOfAc.size() > 0) {
				throw new RuntimeException(
						"Candidate already exists for this Constituency");
			}
			dbCandidate = new Candidate();
		}
		dbCandidate.setAddress(candidateDto.getAddress());
		dbCandidate.setContactNumber1(candidateDto.getContactNumber1());
		dbCandidate.setContactNumber2(candidateDto.getContactNumber2());
		dbCandidate.setEducation(candidateDto.getEducation());
		dbCandidate.setLegalCases(candidateDto.getLegalCases());
		dbCandidate.setName(candidateDto.getName());
		dbCandidate.setObjectives(candidateDto.getObjectives());
		dbCandidate.setProfile(candidateDto.getProfile());
		dbCandidate.setProfilePic(candidateDto.getProfilePic());
		dbCandidate.setSourceOfIncome(candidateDto.getSourceOfIncome());
		dbCandidate.setWealth(candidateDto.getWealth());
		dbCandidate.setAssemblyConstituencyId(candidateDto
				.getAssemblyConstituencyId());

		AssemblyConstituency dbAssemblyConstituency = assemblyConstituencyDao
				.getAssemblyConstituencyById(candidateDto
						.getAssemblyConstituencyId());
		dbCandidate.setAssemblyConstituency(dbAssemblyConstituency);

		dbCandidate = candidateDao.saveCandidate(dbCandidate);

		return convertCandidate(dbCandidate);
	}

	private CandidateDto convertCandidate(Candidate dbCandidate) {
		CandidateDto candidateDto = new CandidateDto();
		BeanUtils.copyProperties(dbCandidate, candidateDto);
		return candidateDto;
	}

	@Override
	@Transactional
	public List<CandidateDto> getAllCandidates() {
		List<Candidate> candidates = candidateDao.getAllCandidates();
		List<CandidateDto> allCandidate = new ArrayList<CandidateDto>();
		for (Candidate oneCandidate : candidates) {
			allCandidate.add(convertCandidate(oneCandidate));
		}
		return allCandidate;
	}

	@Override
	@Transactional
	public ManifestoDto saveManifesto(ManifestoDto manifestoDto) {
		Manifesto dbManifesto = null;
		if (manifestoDto.getId() != null && manifestoDto.getId() > 0) {
			dbManifesto = manifestoDao.getManifestoById(manifestoDto.getId());
			if (dbManifesto == null) {
				throw new RuntimeException("No Manifesto found with id "
						+ manifestoDto.getId());
			}
		} else {
			dbManifesto = manifestoDao
					.getManifestoOfAssemblyConstituency(manifestoDto
							.getAssemblyConstituencyId());
			if (dbManifesto == null) {
				dbManifesto = new Manifesto();
			}
		}
		dbManifesto.setContent(manifestoDto.getContent());
		AssemblyConstituency dbAssemblyConstituency = assemblyConstituencyDao
				.getAssemblyConstituencyById(manifestoDto
						.getAssemblyConstituencyId());
		dbManifesto.setAssemblyConstituency(dbAssemblyConstituency);

		dbManifesto = manifestoDao.saveManifesto(dbManifesto);
		manifestoDto.setId(dbManifesto.getId());
		return manifestoDto;
	}

	@Override
	@Transactional
	public List<ManifestoDto> getAllManifestos() {
		List<Manifesto> allManifestos = manifestoDao.getAllManifestos();
		List<ManifestoDto> returnManifestos = new ArrayList<ManifestoDto>();
		for (Manifesto oneManifesto : allManifestos) {
			returnManifestos.add(convertManifesto(oneManifesto));
		}
		return returnManifestos;
	}

	private ManifestoDto convertManifesto(Manifesto oneManifesto) {
		ManifestoDto oneManifestoDto = new ManifestoDto();
		oneManifestoDto.setId(oneManifesto.getId());
		oneManifestoDto.setContent(oneManifesto.getContent());
		oneManifestoDto.setAssemblyConstituencyId(oneManifesto
				.getAssemblyConstituencyId());
		return oneManifestoDto;

	}

	@Override
	@Transactional
	public List<AssemblyConstituencyWithCandidate> getAssemblyConstituencyWithCandidates(
			Long stateId) {
		List<AssemblyConstituency> allAssemblyConstituencies = assemblyConstituencyDao
				.getAssemblyConstituencyOfState(stateId);
		List<AssemblyConstituencyWithCandidate> returnList = new ArrayList<AssemblyConstituencyWithCandidate>(
				allAssemblyConstituencies.size());
		List<Candidate> allCandidatesOfAc;
		AssemblyConstituencyWithCandidate oneAssemblyConstituencyWithCandidate;
		for (AssemblyConstituency oneAssemblyConstituency : allAssemblyConstituencies) {

			allCandidatesOfAc = candidateDao
					.getCandidateOfAssemblyConstituency(oneAssemblyConstituency
							.getId());
			if (allCandidatesOfAc != null && allCandidatesOfAc.size() > 0) {
				oneAssemblyConstituencyWithCandidate = new AssemblyConstituencyWithCandidate(
						convertAssemblyConstituency(oneAssemblyConstituency),
						convertCandidate(allCandidatesOfAc.get(0)));
			} else {
				oneAssemblyConstituencyWithCandidate = new AssemblyConstituencyWithCandidate(
						convertAssemblyConstituency(oneAssemblyConstituency));
			}
			returnList.add(oneAssemblyConstituencyWithCandidate);
		}
		return returnList;
	}

	

	@Override
	@Transactional
	public NewsItem saveNews(NewsItem newsItem) {
		News dbNews;
		if (newsItem.getId() == null || newsItem.getId() <= 0) {
			dbNews = newsDao.getNewsByOriginalUrl(newsItem.getOriginalUrl());
			if (dbNews == null) {
				dbNews = new News();
				dbNews.setDateCreated(new Date());
			}
		} else {
			dbNews = newsDao.getNewsById(newsItem.getId());
			if (dbNews == null) {
				throw new RuntimeException("No news item exists with id ["
						+ newsItem.getId() + "]");
			}
		}
		dbNews.setAuthor(newsItem.getAuthor());
		dbNews.setContent(newsItem.getContent());
		dbNews.setDateModified(new Date());
		dbNews.setImageUrl(newsItem.getImageUrl());
		dbNews.setOriginalUrl(newsItem.getOriginalUrl());
		dbNews.setSource(newsItem.getSource());
		dbNews.setTitle(newsItem.getTitle());
		dbNews.setWebUrl(newsItem.getWebUrl());
		dbNews.setGlobal(newsItem.isGlobal());

		dbNews = newsDao.saveNews(dbNews);
		return convertNews(dbNews);
	}

	private NewsItem convertNews(News news) {
		if (news == null) {
			return null;
		}
		NewsItem newsItem = new NewsItem();
		BeanUtils.copyProperties(news, newsItem);

		List<Tweet> newsTweet = news.getTweets();
		if (newsTweet != null && newsTweet.size() > 0) {
			List<String> oneLiners = new ArrayList<String>();
			for (Tweet oneTweet : newsTweet) {
				oneLiners.add(oneTweet.getTweetContent());
			}
			newsItem.setOneLiners(oneLiners);
		}
		return newsItem;
	}

	private List<NewsItem> convertNewsList(List<News> news) {
		List<NewsItem> list = new ArrayList<NewsItem>();
		if(news == null){
			return list;
		}
		for (News oneNews : news) {
			list.add(convertNews(oneNews));
		}
		return list;
	}

	@Override
	@Transactional
	public NewsItem getNewsByOriginalUrl(String originalUrl) {
		News dbNews = newsDao.getNewsByOriginalUrl(originalUrl);
		return convertNews(dbNews);
	}

	@Override
	@Transactional
	public List<NewsItem> getNews(int pageSize, int pageNumber) {
		List<News> news = newsDao.getAllNewss(pageSize, pageNumber);
		return convertNewsList(news);
	}

	@Override
	@Transactional
	public List<NewsItem> getAllNews() {
		List<News> news = newsDao.getAllNewss();
		return convertNewsList(news);
	}

	@Override
	@Transactional
	public VideoItem saveVideo(VideoItem videoItem) {
		Video dbVideo;
		if (videoItem.getId() == null || videoItem.getId() <= 0) {
			dbVideo = videoDao.getVideoByVideoId(videoItem.getYoutubeVideoId());
			if (dbVideo == null) {
				dbVideo = new Video();
				dbVideo.setDateCreated(new Date());
			}
		} else {
			dbVideo = videoDao.getVideoById(videoItem.getId());
			if (dbVideo == null) {
				throw new RuntimeException("No video item exists with id ["
						+ videoItem.getId() + "]");
			}
		}
		dbVideo.setDateModified(new Date());
		dbVideo.setImageUrl(videoItem.getImageUrl());
		dbVideo.setTitle(videoItem.getTitle());
		dbVideo.setWebUrl(videoItem.getWebUrl());
		dbVideo.setDescription(videoItem.getDescription());
		dbVideo.setYoutubeVideoId(videoItem.getYoutubeVideoId());
		dbVideo.setPublishDate(videoItem.getPublishDate());
		dbVideo.setGlobal(videoItem.isGlobal());
		dbVideo = videoDao.saveVideo(dbVideo);
		return convertVideo(dbVideo);
	}

	@Override
	@Transactional
	public VideoItem getVideoByVideoId(String videoId) {
		Video dbVideo = videoDao.getVideoByVideoId(videoId);
		return convertVideo(dbVideo);
	}

	@Override
	@Transactional
	public List<VideoItem> getVideo(int pageSize, int pageNumber) {
		List<Video> video = videoDao.getAllVideos(pageSize, pageNumber);
		return convertVideoList(video);
	}

	@Override
	@Transactional
	public List<VideoItem> getAllVideo() {
		List<Video> video = videoDao.getAllVideos();
		return convertVideoList(video);
	}

	private VideoItem convertVideo(Video video) {
		if (video == null) {
			return null;
		}
		VideoItem videoItem = new VideoItem();
		BeanUtils.copyProperties(video, videoItem);

		List<Tweet> videoTweet = video.getTweets();
		if (videoTweet != null && videoTweet.size() > 0) {
			List<String> oneLiners = new ArrayList<>();
			for (Tweet oneTweet : videoTweet) {
				oneLiners.add(oneTweet.getTweetContent());
			}
			videoItem.setOneLiners(oneLiners);
		}
		return videoItem;
	}

	private List<VideoItem> convertVideoList(List<Video> video) {
		List<VideoItem> list = new ArrayList<>();
		if (video == null) {
			return list;
		}
		for (Video oneVideo : video) {
			list.add(convertVideo(oneVideo));
		}
		return list;
	}

	@Override
	@Transactional
	public BlogItem saveBlog(BlogItem blogItem) {
		Blog dbBlog;
		if (blogItem.getId() == null || blogItem.getId() <= 0) {
			dbBlog = blogDao.getBlogByOriginalUrl(blogItem.getOriginalUrl());
			if (dbBlog == null) {
				dbBlog = new Blog();
				dbBlog.setDateCreated(new Date());
			}
		} else {
			dbBlog = blogDao.getBlogById(blogItem.getId());
			if (dbBlog == null) {
				throw new RuntimeException("No blog item exists with id ["
						+ blogItem.getId() + "]");
			}
		}
		dbBlog.setAuthor(blogItem.getAuthor());
		dbBlog.setContent(blogItem.getContent());
		dbBlog.setDateModified(new Date());
		dbBlog.setImageUrl(blogItem.getImageUrl());
		dbBlog.setOriginalUrl(blogItem.getOriginalUrl());
		dbBlog.setSource(blogItem.getSource());
		dbBlog.setTitle(blogItem.getTitle());
		dbBlog.setWebUrl(blogItem.getWebUrl());
		dbBlog.setGlobal(dbBlog.isGlobal());
		dbBlog = blogDao.saveBlog(dbBlog);
		return convertBlog(dbBlog);
	}

	@Override
	@Transactional
	public BlogItem getBlogByOriginalUrl(String originalUrl) {
		Blog dbBlog = blogDao.getBlogByOriginalUrl(originalUrl);
		return convertBlog(dbBlog);
	}

	@Override
	@Transactional
	public List<BlogItem> getBlog(int pageSize, int pageNumber) {
		List<Blog> blog = blogDao.getAllBlogs(pageSize, pageNumber);
		return convertBlogList(blog);
	}

	@Override
	@Transactional
	public List<BlogItem> getAllBlog() {
		List<Blog> blogs = blogDao.getAllBlogs();
		return convertBlogList(blogs);
	}

	private BlogItem convertBlog(Blog blog) {
		if (blog == null) {
			return null;
		}
		BlogItem blogItem = new BlogItem();
		BeanUtils.copyProperties(blog, blogItem);

		List<Tweet> blogTweet = blog.getTweets();
		if (blogTweet != null && blogTweet.size() > 0) {
			List<String> oneLiners = new ArrayList<>();
			for (Tweet oneTweet : blogTweet) {
				oneLiners.add(oneTweet.getTweetContent());
			}
			blogItem.setOneLiners(oneLiners);
		}
		return blogItem;
	}

	private List<BlogItem> convertBlogList(List<Blog> blog) {
		List<BlogItem> list = new ArrayList<>();
		if (blog == null) {
			return list;
		}
		for (Blog oneBlog : blog) {
			list.add(convertBlog(oneBlog));
		}
		return list;
	}

	@Override
	@Transactional
	public List<UserDeviceDto> getAndroidUserDevices(long startId, int pageSize) {
		List<UserDeviceDto> returnList = new ArrayList<UserDeviceDto>();
		List<UserDevice> dbDeviceList = userDeviceDao
				.getAndroidUserDeviceAfterId(startId, pageSize);
		for (UserDevice oneUserDevice : dbDeviceList) {
			returnList.add(convertUserDevice(oneUserDevice));
		}
		return returnList;
	}

	private UserDeviceDto convertUserDevice(UserDevice userDevice) {
		UserDeviceDto oneUserDeviceDto = new UserDeviceDto();
		BeanUtils.copyProperties(userDevice, oneUserDeviceDto);
		return oneUserDeviceDto;
	}

	@Override
	@Transactional
	public List<String> getAndroidUserDevicesForMessageSending(long startId,
			int pageSize) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional
	public long getLastNewsId() {
		return newsDao.getLastNewsId();
	}

	@Override
	@Transactional
	public List<NewsItem> getNewsItemsAfterId(long newsId) {
		List<News> newsList = newsDao.getNewsItemsAfterId(newsId);
		return convertNewsList(newsList);
	}

	@Override
	@Transactional
	public List<VideoItem> getVideoItemsAfterId(long videoId) {
		List<Video> videoList = videoDao.getVideoItemsAfterId(videoId);
		return convertVideoList(videoList);
	}

	@Override
	@Transactional
	public long getLastVideoId() {
		return videoDao.getLastVideoId();
	}

	@Override
	@Transactional
	public List<BlogItem> getBlogItemsAfterId(long blogId) {
		List<Blog> blogList = blogDao.getBlogItemsAfterId(blogId);
		return convertBlogList(blogList);
	}

	@Override
	@Transactional
	public long getLastBlogId() {
		return blogDao.getLastBlogId();
	}

	@Override
	@Transactional
	public List<AssemblyConstituencyDto> getAllAssemblyConstituencies() {
		List<AssemblyConstituency> allAcs = assemblyConstituencyDao.getAllAssemblyConstituencys();
		return convertList(allAcs);
	}

	@Override
	@Transactional
	public List<Long> getNewsItemsOfAc(long acId) {
		AssemblyConstituency assemblyConstituency = assemblyConstituencyDao.getAssemblyConstituencyById(acId);
		if(assemblyConstituency == null){
			return new ArrayList<>(1);
		}
		List<Long> allNewsIdOfAc = newsDao.getNewsByLocation(acId, assemblyConstituency.getDistrict().getId(), assemblyConstituency.getDistrict().getStateId());
		return allNewsIdOfAc;
	}

	@Override
	@Transactional
	public List<Long> getNewsItemsOfParliamentConstituency(long pcId) {
		ParliamentConstituency parliamentConstituency = parliamentConstituencyDao.getParliamentConstituencyById(pcId);
		if(parliamentConstituency == null){
			return new ArrayList<>(1);
		}
		List<Long> allNewsIdOfPc = newsDao.getNewsByLocation(pcId, parliamentConstituency.getStateId());
		return allNewsIdOfPc;
	}

	@Override
	@Transactional
	public List<ParliamentConstituencyDto> getAllParliamentConstituencies() {
		List<ParliamentConstituency> parliamentConstituencies = parliamentConstituencyDao.getAllParliamentConstituencys();
		return convertParliamentConstituencyList(parliamentConstituencies);
	}
	
	private List<ParliamentConstituencyDto> convertParliamentConstituencyList(List<ParliamentConstituency> parliamentConstituencies){
		List<ParliamentConstituencyDto> returnList = new ArrayList<ParliamentConstituencyDto>();
		if(parliamentConstituencies == null){
			return returnList;	
		}
		for (ParliamentConstituency oneAssemblyConstituency : parliamentConstituencies) {
			returnList
					.add(convertParliamentConstituency(oneAssemblyConstituency));
		}
		return returnList;
	}
	
	private ParliamentConstituencyDto convertParliamentConstituency(
			ParliamentConstituency oneParliamentConstituency) {
		ParliamentConstituencyDto oneParliamentConstituencyDto = new ParliamentConstituencyDto();
		oneParliamentConstituencyDto.setId(oneParliamentConstituency.getId());
		oneParliamentConstituencyDto.setName(oneParliamentConstituency.getName());
		return oneParliamentConstituencyDto;

	}

	@Override
	@Transactional
	public List<ParliamentConstituencyDto> getAllParliamentConstituenciesOfState(
			long stateId) {
		List<ParliamentConstituency> parliamentConstituencies = parliamentConstituencyDao.getParliamentConstituencyOfState(stateId);
		return convertParliamentConstituencyList(parliamentConstituencies);
	}

	@Override
	@Transactional
	public List<Long> getBlogItemsOfAc(long acId) {
		AssemblyConstituency assemblyConstituency = assemblyConstituencyDao.getAssemblyConstituencyById(acId);
		if(assemblyConstituency == null){
			return new ArrayList<>(1);
		}
		List<Long> allBlogIdsOfAc = blogDao.getBlogByLocation(acId, assemblyConstituency.getDistrict().getId(), assemblyConstituency.getDistrict().getStateId());
		return allBlogIdsOfAc;
	}

	@Override
	@Transactional
	public List<Long> getBlogItemsOfParliamentConstituency(long pcId) {
		ParliamentConstituency parliamentConstituency = parliamentConstituencyDao.getParliamentConstituencyById(pcId);
		if(parliamentConstituency == null){
			return new ArrayList<>(1);
		}
		List<Long> allBlogIdsOfPc = blogDao.getBlogByLocation(pcId, parliamentConstituency.getStateId());
		return allBlogIdsOfPc;
	}

	@Override
	@Transactional
	public List<Long> getVideoItemsOfAc(long acId) {
		AssemblyConstituency assemblyConstituency = assemblyConstituencyDao.getAssemblyConstituencyById(acId);
		if(assemblyConstituency == null){
			return new ArrayList<>(1);
		}
		List<Long> allVideoIdsOfAc = videoDao.getVideoByLocation(acId, assemblyConstituency.getDistrict().getId(), assemblyConstituency.getDistrict().getStateId());
		return allVideoIdsOfAc;
	}

	@Override
	@Transactional
	public List<Long> getVideoItemsOfParliamentConstituency(long pcId) {
		ParliamentConstituency parliamentConstituency = parliamentConstituencyDao.getParliamentConstituencyById(pcId);
		if(parliamentConstituency == null){
			return new ArrayList<>(1);
		}
		List<Long> allVideoIdsOfPc = videoDao.getVideoByLocation(pcId, parliamentConstituency.getStateId());
		return allVideoIdsOfPc;
	}

	@Override
	@Transactional
	public StateDto getStateByName(String name) {
		State state = stateDao.getStateByName(name);
		return convertState(state);
	}

	@Override
	@Transactional
	public ParliamentConstituencyDto saveParliamentConstituency(
			ParliamentConstituencyDto parliamentConstituencyDto) {
		ParliamentConstituency dbParliamentConstituency;
		if (parliamentConstituencyDto.getId() == null || parliamentConstituencyDto.getId() <= 0) {
			dbParliamentConstituency = parliamentConstituencyDao.getParliamentConstituencyNameAndStateId(
					parliamentConstituencyDto.getStateId(), parliamentConstituencyDto.getName());
			if (dbParliamentConstituency == null) {
				dbParliamentConstituency = new ParliamentConstituency();
				dbParliamentConstituency.setDateCreated(new Date());
			}
		} else {
			dbParliamentConstituency = parliamentConstituencyDao.getParliamentConstituencyById(parliamentConstituencyDto.getId());
			if (dbParliamentConstituency == null) {
				throw new RuntimeException("No such District exist[id="
						+ parliamentConstituencyDto.getId() + "]");
			}
		}
		
		dbParliamentConstituency.setName(parliamentConstituencyDto.getName());
		dbParliamentConstituency.setDateModified(new Date());
		
		
		State state = stateDao.getStateById(parliamentConstituencyDto.getStateId());
		dbParliamentConstituency.setState(state);

		dbParliamentConstituency = parliamentConstituencyDao.saveParliamentConstituency(dbParliamentConstituency);

		parliamentConstituencyDto.setId(dbParliamentConstituency.getId());
		return parliamentConstituencyDto;
	}

	@Override
	@Transactional
	public DistrictDto getDistrictByNameAndStateId(String name, Long stateId) {
		District district = districtDao.getDistrictByNameAndStateId(stateId, name);
		return convertDistrict(district);
	}


}
