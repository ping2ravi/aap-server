package com.next.aap.server.persistance.dao.impl;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.stereotype.Component;

import com.next.aap.server.persistance.Candidate;
import com.next.aap.server.persistance.dao.CandidateDao;

@Component
public class CandidateDaoHibernateSpringImpl extends BaseDaoHibernateSpring<Candidate> implements CandidateDao {

	/* (non-Javadoc)
	 * @see com.next.aap.server.persistance.dao.impl.CandidateDao#saveCandidate(com.next.aap.server.persistance.Candidate)
	 */
	@Override
	public Candidate saveCandidate(Candidate candidate)
	{
		checkIfStringMissing("Name", candidate.getName());
		checkIfObjectMissing("AssemblyConstituency", candidate.getAssemblyConstituency());
		checkIfCandidateExistsWithSameName(candidate);
		candidate.setNameUp(candidate.getName().toUpperCase());
		candidate = super.saveObject(candidate);
		return candidate;
	}

	/* (non-Javadoc)
	 * @see com.next.aap.server.persistance.dao.impl.CandidateDao#deleteCandidate(com.next.aap.server.persistance.Candidate)
	 */
	@Override
	public void deleteCandidate(Candidate candidate) {
		super.deleteObject(candidate);
	}

	/* (non-Javadoc)
	 * @see com.next.aap.server.persistance.dao.impl.CandidateDao#getCandidateById(java.lang.Long)
	 */
	@Override
	public Candidate getCandidateById(Long id)
	{
		Candidate candidate = super.getObjectById(Candidate.class, id);
		return candidate;
	}

	/* (non-Javadoc)
	 * @see com.next.aap.server.persistance.dao.impl.CandidateDao#getAllCandidates()
	 */
	@Override
	public List<Candidate> getAllCandidates()
	{
		List<Candidate> list = executeQueryGetList("from Candidate order by name asc");
		return list;
	}
	/* (non-Javadoc)
	 * @see com.next.aap.server.persistance.dao.impl.CandidateDao#getCandidateOfAssemblyConstituency(long)
	 */
	@Override
	public List<Candidate> getCandidateOfAssemblyConstituency(long assemblyConstituencyId){
		Map<String, Object> params = new TreeMap<String, Object>();
		params.put("assemblyConstituencyId", assemblyConstituencyId);
		List<Candidate> list = executeQueryGetList("from Candidate where assemblyConstituencyId = :assemblyConstituencyId order by name asc", params);
		return list;
	}

	private void checkIfCandidateExistsWithSameName(Candidate candidate) {
		Map<String, Object> params = new TreeMap<String, Object>();
		params.put("assemblyConstituencyId", candidate.getAssemblyConstituency().getId());
		params.put("nameUp", candidate.getName().toUpperCase());
		Candidate existingCandidate = null;
		if(candidate.getId() != null && candidate.getId() > 0){
			params.put("id", candidate.getId());
			existingCandidate = executeQueryGetObject("from Candidate where assemblyConstituencyId = :assemblyConstituencyId and nameUp = :nameUp and id != :id order by name asc");
		}else{
			existingCandidate = executeQueryGetObject("from Candidate where assemblyConstituencyId = :assemblyConstituencyId and nameUp = :nameUp order by name asc");
		}
		
		if(existingCandidate != null){
			throw new RuntimeException("Candidate already exists with name = "+candidate.getName()+" in Assembly Constituency "+candidate.getAssemblyConstituency().getName());
		}
	}
}