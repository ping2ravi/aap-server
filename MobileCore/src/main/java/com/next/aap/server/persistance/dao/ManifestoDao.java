package com.next.aap.server.persistance.dao;

import java.util.List;

import com.next.aap.server.persistance.Manifesto;

public interface ManifestoDao {

	/**
	 * Creates/updates a manifesto in Database
	 * 
	 * @param manifesto
	 * @return saved manifesto
	 * @throws AppException
	 */
	public abstract Manifesto saveManifesto(Manifesto manifesto);

	/**
	 * deletes a manifesto in Database
	 * 
	 * @param manifesto
	 * @return updated manifesto
	 * @throws AppException
	 */
	public abstract void deleteManifesto(Manifesto manifesto);

	/**
	 * return a Manifesto with given primary key/id
	 * 
	 * @param id
	 * @return Manifesto with PK as id(parameter)
	 * @throws AppException
	 */
	public abstract Manifesto getManifestoById(Long id);

	public abstract List<Manifesto> getAllManifestos();

	public abstract Manifesto getManifestoOfAssemblyConstituency(
			long assemblyConstituencyId);

}