package com.next.aap.server.cache.db;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.next.aap.dto.AllHomeScreenItem;
import com.next.aap.dto.BlogItem;
import com.next.aap.dto.EventItem;
import com.next.aap.dto.NewsItem;
import com.next.aap.dto.VideoItem;
import com.next.aap.server.cache.HomeScreenDataCache;

public class HomeScreenDataCacheDbImpl extends BaseDbCache implements HomeScreenDataCache{
	
	private Map<String, AllHomeScreenItem> languageBasedHomeScreenItems = new HashMap<String, AllHomeScreenItem>();
	private Map<String, String> allHomeScreenItemJsonStringByLang = new HashMap<String, String>();
	private Map<String, String> allNewsItemJsonStringByLang = new HashMap<String, String>();
	private Map<String, String> allVideoItemJsonStringByLang = new HashMap<String, String>();
	private Map<String, String> allEventItemJsonStringByLang = new HashMap<String, String>();
	private Map<String, String> allBlogItemJsonStringByLang = new HashMap<String, String>();
	
	private Map<Integer, String> allNewsItemByPageNumbers = new HashMap<Integer, String>();
	private Map<Integer, String> allBlogItemByPageNumbers = new HashMap<Integer, String>();
	private Map<Integer, String> allVideoItemByPageNumbers = new HashMap<Integer, String>();
	
	private static final int MAX_NEWS_ITEM = 10;
	private static final int MAX_VIDEO_ITEM = 10;
	private static final int MAX_BLOG_ITEM = 10;
	private static final int MAX_EVENT_ITEM = 10;
	private static final String DEFAULT_LANGUAGE = "en";
	
	private Set<String> allValidLanguages;

	@PostConstruct
	public void init(){
		allValidLanguages = new TreeSet<String>();
		allValidLanguages.add(DEFAULT_LANGUAGE);

		refreshFullCache();
	}

	@Override
	public void refreshFullCache(){
		AllHomeScreenItem allHomeScreenItem = new AllHomeScreenItem();
		
		List<NewsItem> newsItems = aapService.getNews(MAX_NEWS_ITEM, 0);
		allHomeScreenItem.setNewsItems(newsItems);
		
		List<VideoItem> videoItems = aapService.getVideo(MAX_VIDEO_ITEM, 0);
		allHomeScreenItem.setVideoItems(videoItems);
		
		List<BlogItem> blogItems = aapService.getBlog(MAX_BLOG_ITEM, 0);
		allHomeScreenItem.setBlogItems(blogItems);

		List<NewsItem> allNewsItems = aapService.getAllNews();
		List<NewsItem> onePageNewsItems = new ArrayList<NewsItem>(MAX_NEWS_ITEM);
		int count = 0;
		int pageNumber = 1;
		Gson gson = new Gson();
		for(NewsItem oneNewsItem:allNewsItems){
			onePageNewsItems.add(oneNewsItem);
			count++;
			if(count == 10){
				count = 0;
				allNewsItemByPageNumbers.put(pageNumber, gson.toJson(onePageNewsItems));
				onePageNewsItems.clear();
				pageNumber++;
			}
		}
		
		//Blogs
		List<VideoItem> allVideoItems = aapService.getAllVideo();
		List<VideoItem> onePageVideoItems = new ArrayList<>(MAX_VIDEO_ITEM);
		count = 0;
		pageNumber = 1;
		for(VideoItem oneVideoItem:allVideoItems){
			onePageVideoItems.add(oneVideoItem);
			count++;
			if(count == 10){
				count = 0;
				allVideoItemByPageNumbers.put(pageNumber, gson.toJson(onePageVideoItems));
				onePageVideoItems.clear();
				pageNumber++;
			}
		}
		
		//Blogs
		List<BlogItem> allBlogItems = aapService.getAllBlog();
		List<BlogItem> onePageBlogItems = new ArrayList<>(MAX_BLOG_ITEM);
		count = 0;
		pageNumber = 1;
		for(BlogItem oneBlogItem:allBlogItems){
			onePageBlogItems.add(oneBlogItem);
			count++;
			if(count == 10){
				count = 0;
				allBlogItemByPageNumbers.put(pageNumber, gson.toJson(onePageBlogItems));
				onePageBlogItems.clear();
				pageNumber++;
			}
		}

		
		convertToJson(allHomeScreenItem, DEFAULT_LANGUAGE);
	}
	
	protected String convertToJson(AllHomeScreenItem allHomeScreenItem, String lang){
		Gson gson = new Gson();
		String returnJson = gson.toJson(allHomeScreenItem);
		allHomeScreenItemJsonStringByLang.put(lang, returnJson);
		allNewsItemJsonStringByLang.put(lang, gson.toJson(allHomeScreenItem.getNewsItems()));
		allVideoItemJsonStringByLang.put(lang, gson.toJson(allHomeScreenItem.getVideoItems()));
		allEventItemJsonStringByLang.put(lang, gson.toJson(allHomeScreenItem.getEventItems()));
		allBlogItemJsonStringByLang.put(lang, gson.toJson(allHomeScreenItem.getBlogItems()));
		return returnJson;
	}

	@Override
	public String getHomeScreenItems(String lang, long acId, long votingAcId, long livingPcId, long votingPcId) {
		return getJsonStringFromMapByLanguage(allHomeScreenItemJsonStringByLang, lang);
	}

	@Override
	public String getNewsItems(String lang, long acId, long votingAcId, long livingPcId, long votingPcId) {
		return getJsonStringFromMapByLanguage(allNewsItemJsonStringByLang, lang);
	}

	@Override
	public String getVideoItems(String lang, long acId, long votingAcId, long livingPcId, long votingPcId) {
		return getJsonStringFromMapByLanguage(allVideoItemJsonStringByLang, lang);
	}

	@Override
	public String getEventItems(String lang, long acId, long votingAcId, long livingPcId, long votingPcId) {
		return getJsonStringFromMapByLanguage(allEventItemJsonStringByLang, lang);
	}

	@Override
	public String getBlogItems(String lang, long acId, long votingAcId, long livingPcId, long votingPcId) {
		return getJsonStringFromMapByLanguage(allBlogItemJsonStringByLang, lang);
	}

	@Override
	public void addNewsItemToCache(NewsItem newsItem, String lang) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addVideoItemToCache(VideoItem videoItem, String lang) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addBlogItemToCache(BlogItem blogItem, String lang) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addEventItemToCache(EventItem eventItem, String lang) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clearAllNewsItems() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clearNewsItem(Long itemId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clearAllVideoItems() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clearVideoItem(Long itemId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clearAllEventItems() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clearEventItem(Long itemId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clearAllBlogItems() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clearBlogItem(Long itemId) {
		// TODO Auto-generated method stub
		
	}
	private String getJsonStringFromMapByLanguage(Map<String, String> langMap, String lang){
		lang = getValidLanugae(lang);
		String returnString = langMap.get(lang);
		if(returnString == null){
			returnString = langMap.get(DEFAULT_LANGUAGE);
		}
		return returnString;
	}
	private String getValidLanugae(String lang){
		lang = lang.toLowerCase();
		if(allValidLanguages.contains(lang)){
			return lang;
		}
		return DEFAULT_LANGUAGE;
	}

	@Override
	public String getNewsItems(String lang, long acId, long votingAcId, long livingPcId, long votingPcId, int pageNumber) {
		return allNewsItemByPageNumbers.get(pageNumber);
	}

	@Override
	public String getVideoItems(String lang, long acId, long votingAcId, long livingPcId, long votingPcId, int pageNumber) {
		return allVideoItemByPageNumbers.get(pageNumber);
	}

	@Override
	public String getBlogItems(String lang, long acId, long votingAcId, long livingPcId, long votingPcId, int pageNumber) {
		return allBlogItemByPageNumbers.get(pageNumber);
	}


}
