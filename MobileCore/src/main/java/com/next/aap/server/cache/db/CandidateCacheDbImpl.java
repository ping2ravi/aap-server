package com.next.aap.server.cache.db;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.next.aap.dto.AssemblyConstituencyWithCandidate;
import com.next.aap.server.cache.CandidateCache;
import com.next.aap.util.DataUtil;

@Component
public class CandidateCacheDbImpl extends BaseDbCache implements CandidateCache{

	private String assemblyConstituencyWithCandidates;
	
	@PostConstruct
	public void init(){
		if(loadCacheOnStartup){
			refreshCache();
		}
	}
	private boolean alreadyRunning = false;
	
	private boolean loadCacheOnStartup;
	
	private boolean cacheLoaded = false;;
	
	@Value("${load.cache.on.startup}")
	public void setDatabaseName(boolean loadCacheOnStartup) {
		this.loadCacheOnStartup = loadCacheOnStartup;
	}

	/* (non-Javadoc)
	 * @see com.next.aap.server.cache.db.CandidateCache#refreshCacheAsync()
	 */
	@Override
	public String refreshCacheAsync(){
		
		synchronized (CandidateCacheDbImpl.class) {
			if(alreadyRunning){
				logger.info("CandidateCache Refresh Task already running");
				return "Task Already Running";
			}
			Runnable task = new Runnable() {
				
				public void run() {
					refreshCache();
				}
			};
			new Thread(task).start();
		}
		logger.info("CandidateCache Refresh Task started");
		return "Task Started";
	}
	/* (non-Javadoc)
	 * @see com.next.aap.server.cache.db.CandidateCache#refreshCache()
	 */
	@Override
	public void refreshCache(){
		logger.info("Refreshing Cache");
		try{
			List<AssemblyConstituencyWithCandidate> acWithCandidates = aapService.getAssemblyConstituencyWithCandidates(DataUtil.delhiStateId);
			Gson gson = new Gson();
			assemblyConstituencyWithCandidates = gson.toJson(acWithCandidates);
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public String getAcWithCandidates() {
		return assemblyConstituencyWithCandidates;
	}

}
