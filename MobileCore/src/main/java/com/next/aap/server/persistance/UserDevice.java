package com.next.aap.server.persistance;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name="user_devices")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE,region="Users", include="all")
public class UserDevice {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO,  generator="USER_DEVICE_SEQ_GEN")
	@SequenceGenerator(
    name="USER_DEVICE_SEQ_GEN",
    sequenceName="USER_DEVICE_SEQ",
    allocationSize=20
	)
	private Long id;
	@Version
	@Column(name="ver")
	private int ver;
	
	@Column(name="date_created")
	private Date dateCreated;
	@Column(name="date_modified")
	private Date dateModified;
	@Column(name="creator_id")
	private Long creatorId;
	@Column(name="modifier_id")
	private Long modifierId;

	@ManyToOne( cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
    @JoinColumn(name="living_state_id")
    private State stateLiving;
	@Column(name="living_state_id", insertable=false,updatable=false)
	private Long stateLivingId;

	@ManyToOne( cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
    @JoinColumn(name="living_district_id")
    private District districtLiving;
	@Column(name="living_district_id", insertable=false,updatable=false)
	private Long districtLivingId;

	@ManyToOne( cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
    @JoinColumn(name="living_ac_id")
    private AssemblyConstituency assemblyConstituencyLiving;
	@Column(name="living_ac_id", insertable=false,updatable=false)
	private Long assemblyConstituencyLivingId;
	
	@ManyToOne( cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
    @JoinColumn(name="living_pc_id")
    private ParliamentConstituency parliamentConstituencyLiving;
	@Column(name="living_pc_id", insertable=false,updatable=false)
	private Long parliamentConstituencyLivingId;

	@ManyToOne( cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
    @JoinColumn(name="voting_state_id")
    private State stateVoting;
	@Column(name="voting_state_id", insertable=false,updatable=false)
	private Long stateVotingId;

	@ManyToOne( cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
    @JoinColumn(name="voting_district_id")
    private District districtVoting;
	@Column(name="voting_district_id", insertable=false,updatable=false)
	private Long districtVotingId;

	@ManyToOne( cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
    @JoinColumn(name="voting_ac_id")
    private AssemblyConstituency assemblyConstituencyVoting;
	@Column(name="voting_ac_id", insertable=false,updatable=false)
	private Long assemblyConstituencyVotingId;
	
	@ManyToOne( cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
    @JoinColumn(name="voting_pc_id")
    private ParliamentConstituency parliamentConstituencyVoting;
	@Column(name="voting_pc_id", insertable=false,updatable=false)
	private Long parliamentConstituencyVotingId;

	@Column(name = "reg_id", nullable = false, length=256)
	private String regId;
	
	@Column(name = "device_tye", nullable = false, length=256)
	private String deviceType;

	@ManyToMany(cascade=CascadeType.ALL)
	@JoinTable(name = "user_user_devices",
	joinColumns = {
	@JoinColumn(name="user_device_id") 
	},
	inverseJoinColumns = {
	@JoinColumn(name="user_id")
	})
	private Set<User> users;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public int getVer() {
		return ver;
	}
	public void setVer(int ver) {
		this.ver = ver;
	}
	public Date getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	public Date getDateModified() {
		return dateModified;
	}
	public void setDateModified(Date dateModified) {
		this.dateModified = dateModified;
	}
	public Long getCreatorId() {
		return creatorId;
	}
	public void setCreatorId(Long creatorId) {
		this.creatorId = creatorId;
	}
	public Long getModifierId() {
		return modifierId;
	}
	public void setModifierId(Long modifierId) {
		this.modifierId = modifierId;
	}
	public String getRegId() {
		return regId;
	}
	public void setRegId(String regId) {
		this.regId = regId;
	}
	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	public Set<User> getUsers() {
		return users;
	}
	public void setUsers(Set<User> users) {
		this.users = users;
	}
	public State getStateLiving() {
		return stateLiving;
	}
	public void setStateLiving(State stateLiving) {
		this.stateLiving = stateLiving;
	}
	public Long getStateLivingId() {
		return stateLivingId;
	}
	public void setStateLivingId(Long stateLivingId) {
		this.stateLivingId = stateLivingId;
	}
	public District getDistrictLiving() {
		return districtLiving;
	}
	public void setDistrictLiving(District districtLiving) {
		this.districtLiving = districtLiving;
	}
	public Long getDistrictLivingId() {
		return districtLivingId;
	}
	public void setDistrictLivingId(Long districtLivingId) {
		this.districtLivingId = districtLivingId;
	}
	public AssemblyConstituency getAssemblyConstituencyLiving() {
		return assemblyConstituencyLiving;
	}
	public void setAssemblyConstituencyLiving(
			AssemblyConstituency assemblyConstituencyLiving) {
		this.assemblyConstituencyLiving = assemblyConstituencyLiving;
	}
	public Long getAssemblyConstituencyLivingId() {
		return assemblyConstituencyLivingId;
	}
	public void setAssemblyConstituencyLivingId(Long assemblyConstituencyLivingId) {
		this.assemblyConstituencyLivingId = assemblyConstituencyLivingId;
	}
	public State getStateVoting() {
		return stateVoting;
	}
	public void setStateVoting(State stateVoting) {
		this.stateVoting = stateVoting;
	}
	public Long getStateVotingId() {
		return stateVotingId;
	}
	public void setStateVotingId(Long stateVotingId) {
		this.stateVotingId = stateVotingId;
	}
	public District getDistrictVoting() {
		return districtVoting;
	}
	public void setDistrictVoting(District districtVoting) {
		this.districtVoting = districtVoting;
	}
	public Long getDistrictVotingId() {
		return districtVotingId;
	}
	public void setDistrictVotingId(Long districtVotingId) {
		this.districtVotingId = districtVotingId;
	}
	public AssemblyConstituency getAssemblyConstituencyVoting() {
		return assemblyConstituencyVoting;
	}
	public void setAssemblyConstituencyVoting(
			AssemblyConstituency assemblyConstituencyVoting) {
		this.assemblyConstituencyVoting = assemblyConstituencyVoting;
	}
	public Long getAssemblyConstituencyVotingId() {
		return assemblyConstituencyVotingId;
	}
	public void setAssemblyConstituencyVotingId(Long assemblyConstituencyVotingId) {
		this.assemblyConstituencyVotingId = assemblyConstituencyVotingId;
	}
	public ParliamentConstituency getParliamentConstituencyLiving() {
		return parliamentConstituencyLiving;
	}
	public void setParliamentConstituencyLiving(
			ParliamentConstituency parliamentConstituencyLiving) {
		this.parliamentConstituencyLiving = parliamentConstituencyLiving;
	}
	public Long getParliamentConstituencyLivingId() {
		return parliamentConstituencyLivingId;
	}
	public void setParliamentConstituencyLivingId(
			Long parliamentConstituencyLivingId) {
		this.parliamentConstituencyLivingId = parliamentConstituencyLivingId;
	}
	public ParliamentConstituency getParliamentConstituencyVoting() {
		return parliamentConstituencyVoting;
	}
	public void setParliamentConstituencyVoting(
			ParliamentConstituency parliamentConstituencyVoting) {
		this.parliamentConstituencyVoting = parliamentConstituencyVoting;
	}
	public Long getParliamentConstituencyVotingId() {
		return parliamentConstituencyVotingId;
	}
	public void setParliamentConstituencyVotingId(
			Long parliamentConstituencyVotingId) {
		this.parliamentConstituencyVotingId = parliamentConstituencyVotingId;
	}

}
