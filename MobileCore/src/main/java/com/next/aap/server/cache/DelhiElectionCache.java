package com.next.aap.server.cache;

public interface DelhiElectionCache {

	public abstract void refreshCache();

	public abstract String getCandidateSelectionProcess(String language);

	public abstract void setCandidateSelectionProcess(String language,
			String candidateSelectionProcess);

}