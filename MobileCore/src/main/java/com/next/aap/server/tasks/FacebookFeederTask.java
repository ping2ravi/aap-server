package com.next.aap.server.tasks;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.next.aap.messages.NewFacebookNotificationMessage;
import com.next.aap.messages.NotificationMessage;
import com.next.aap.server.cache.db.FacebookCache;
import com.next.aap.server.service.AapService;

@Component
public class FacebookFeederTask extends BaseTask {

	@Autowired
	private AapService aapService;
	@Autowired
	private TaskManager taskManager;
	@Autowired
	private FacebookCache facebookCache;
	
	@Value("${work.in.task:true}")
	private boolean workInTask;
	
	@Value("${work.in.facebook.feeder.task:true}")
	private boolean workInFacebookFeederTask = false;;
	

	//private final String facebookPageUrl = "https://graph.facebook.com/290805814352519/posts?fields=link,actions,icon,source,type,id,name,status_type,picture,privacy,message,type,status_type,comments.limit(1).summary(true),likes.limit(10).summary(true)&limit=25&date_format=d-M-Y%20h:i%20e&access_token=CAACjs4PJeR8BAA0fU8CQsqja53juuqKv8HgTQvoKqlQeLp6iP9Xds32HyReJ85raVZBl2rJCZAoy4HZBgmLUGZCLCG7A2jjZAyz1GsAXX88ZAJXo8W2OL3M3OxdiEvF9WAuBlZATGZBkO8a0TaENjEgcZBBZCZAZCVi6LGF5dEdUsJDGuCqqZBx1uZCurPCaJTiWeNVC6CaFILZCjwRotkzTHcQ936Q7JW36kDnQGHKfdOZA7U1VTAZDZD";
	private final String facebookPageUrl = "https://graph.facebook.com/290805814352519/posts?fields=link,actions,icon,source,type,id,name,status_type,picture,privacy,message,type,status_type,comments.limit(1).summary(true),likes.limit(10).summary(true)&limit=25&date_format=U&access_token=CAACjs4PJeR8BAA0fU8CQsqja53juuqKv8HgTQvoKqlQeLp6iP9Xds32HyReJ85raVZBl2rJCZAoy4HZBgmLUGZCLCG7A2jjZAyz1GsAXX88ZAJXo8W2OL3M3OxdiEvF9WAuBlZATGZBkO8a0TaENjEgcZBBZCZAZCVi6LGF5dEdUsJDGuCqqZBx1uZCurPCaJTiWeNVC6CaFILZCjwRotkzTHcQ936Q7JW36kDnQGHKfdOZA7U1VTAZDZD";
	Logger logger = LoggerFactory.getLogger(this.getClass());
	List<String> previousList = new ArrayList<String>();
	@Scheduled(fixedDelay=1000 * 60 * 15)//Every 10 minutes
	public void facebookTask(){
		if(!(workInTask && workInFacebookFeederTask)){
			logger.warn("Facebook Feeder task is disabled");
			return;
		}
		try{
			HttpClient httpClient = new DefaultHttpClient();
			HttpGet httpGet = new HttpGet(facebookPageUrl);
			logger.info("Hitting Url = {}", facebookPageUrl);
			System.out.println("Hitting Url = "+ facebookPageUrl);
			HttpResponse httpResponse = httpClient.execute(httpGet);
			System.out.println("Got Response= "+ httpResponse);
			HttpEntity httpEntity = httpResponse.getEntity();
			System.out.println("Converting to String= "+ httpEntity);
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			System.out.println("IOUtils.copy(httpEntity.getContent(), byteArrayOutputStream);");
			IOUtils.copy(httpEntity.getContent(), byteArrayOutputStream);
			String facebookFeed = byteArrayOutputStream.toString();
			//System.out.println("data="+data);
			JSONObject jsonObject = new JSONObject(facebookFeed);
			JSONArray dataArray = jsonObject.getJSONArray("data");
			System.out.println("dataArray="+dataArray.length());
			List<String> currentList = new ArrayList<String>(dataArray.length());
			JSONObject onePost;
			for(int i=0;i<dataArray.length();i++){
				try{
					onePost = dataArray.getJSONObject(i);
					currentList.add(onePost.getString("id"));
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}
			
			int totalItems = 0;
			//Now compare and see if there is new post
			String title = "Facebook Updates";
			if(previousList.isEmpty()){
				//For first time after server restart update the facebook feed
				facebookCache.setFacebookFeed(facebookFeed);
			}else{
				for(int i=0;i<previousList.size();i++){
					if(currentList.size() > i){
						if(previousList.get(0).equals(currentList.get(i))){
							//as soon as same id found break the loop
							break;
						}
						try{
							title = dataArray.getJSONObject(i).getString("message").substring(0, 45);	
						}catch(Exception ex){
							title = "New Photo Added";
							ex.printStackTrace();
						}
						totalItems++;
					}
				}
			}
			previousList = currentList;
			
			logger.info("Total New items are :  " + totalItems);
			if(totalItems > 0 ){
				//If something changed then only update the facebook feed
				facebookCache.setFacebookFeed(facebookFeed);
				NewFacebookNotificationMessage notificationMessage = new NewFacebookNotificationMessage();
				notificationMessage.setNotificationTitle(title);
				notificationMessage.setTotalItem(totalItems);
				
				sendMessage(notificationMessage, NotificationMessage.NEW_FACEBOOK_MESSAGE);
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
}
