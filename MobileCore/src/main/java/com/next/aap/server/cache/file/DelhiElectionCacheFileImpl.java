package com.next.aap.server.cache.file;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.next.aap.dto.CandidateSelectionProcess;
import com.next.aap.server.cache.DelhiElectionCache;

@Component
public class DelhiElectionCacheFileImpl extends BaseFileCache implements DelhiElectionCache{

	private final String ENG_CANDIDATE_SELECTION_PROCESS_FILE_NAME=baseCacheFileDirectory +"/csp_eng.json";
	private final String HINDI_CANDIDATE_SELECTION_PROCESS_FILE_NAME=baseCacheFileDirectory +"/csp_hin.json";
	
	private Map<String, String> candidateSelectionProcessByLanguage = new HashMap<String, String>();
	
	private boolean loadCacheOnStartup;
	
	
	@Value("${load.cache.on.startup}")
	public void setDatabaseName(boolean loadCacheOnStartup) {
		this.loadCacheOnStartup = loadCacheOnStartup;
	}

	@PostConstruct
	public void init(){
		if(loadCacheOnStartup){
			refreshCache();	
		}
		
	}
	/* (non-Javadoc)
	 * @see com.next.aap.server.cache.file.DelhiElectionCache#refreshCache()
	 */
	@Override
	public void refreshCache(){
		candidateSelectionProcessByLanguage.clear();
		String candidateSelectionProceEnglish = readFromFile(ENG_CANDIDATE_SELECTION_PROCESS_FILE_NAME);
		String candidateSelectionProceHindi = readFromFile(HINDI_CANDIDATE_SELECTION_PROCESS_FILE_NAME);
		if(candidateSelectionProceEnglish == null){
			CandidateSelectionProcess candidateSelectionProcess = new CandidateSelectionProcess();
			candidateSelectionProcess.setMessage("Not available at the moment");
			candidateSelectionProceEnglish = new Gson().toJson(candidateSelectionProcess);	
		}
		if(candidateSelectionProceHindi == null){
			CandidateSelectionProcess candidateSelectionProcess = new CandidateSelectionProcess();
			candidateSelectionProcess.setMessage("इस समय उपलब्ध नहीं है");
			candidateSelectionProceHindi = new Gson().toJson(candidateSelectionProcess);	
		}
		candidateSelectionProcessByLanguage.put("en", candidateSelectionProceEnglish);
		candidateSelectionProcessByLanguage.put("hin", candidateSelectionProceHindi);
	}
	/* (non-Javadoc)
	 * @see com.next.aap.server.cache.file.DelhiElectionCache#getCandidateSelectionProcess(java.lang.String)
	 */
	@Override
	public String getCandidateSelectionProcess(String language){
		String csp = candidateSelectionProcessByLanguage.get(language);
		if(csp == null){
			csp = "Not available at the moment / इस समय उपलब्ध नहीं है";
		}
		return csp;
	}
	/* (non-Javadoc)
	 * @see com.next.aap.server.cache.file.DelhiElectionCache#setCandidateSelectionProcess(java.lang.String, java.lang.String)
	 */
	@Override
	public void setCandidateSelectionProcess(String language, String candidateSelectionProcess) {
		candidateSelectionProcessByLanguage.put(language, candidateSelectionProcess);
		if("en".equalsIgnoreCase(language)){
			writeToFile(ENG_CANDIDATE_SELECTION_PROCESS_FILE_NAME, candidateSelectionProcess);	
		}else if("hin".equalsIgnoreCase(language)){
			writeToFile(HINDI_CANDIDATE_SELECTION_PROCESS_FILE_NAME, candidateSelectionProcess);
		}else {
			writeToFile("csp_"+language+".json", candidateSelectionProcess);
		}
		
	}

	public String getCandidateSelectionProceEnglish() {
		return candidateSelectionProcessByLanguage.get("en");
	}
	public void setCandidateSelectionProceEnglish(
			String candidateSelectionProceEnglish) {
		candidateSelectionProcessByLanguage.put("en", candidateSelectionProceEnglish);
		writeToFile(ENG_CANDIDATE_SELECTION_PROCESS_FILE_NAME, candidateSelectionProceEnglish);
	}
	public String getCandidateSelectionProceHindi() {
		return candidateSelectionProcessByLanguage.get("hin");
	}
	public void setCandidateSelectionProceHindi(String candidateSelectionProceHindi) {
		candidateSelectionProcessByLanguage.put("hin", candidateSelectionProceHindi);
		writeToFile(HINDI_CANDIDATE_SELECTION_PROCESS_FILE_NAME, candidateSelectionProceHindi);
	}
}
