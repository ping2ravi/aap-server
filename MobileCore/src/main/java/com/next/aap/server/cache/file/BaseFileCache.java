package com.next.aap.server.cache.file;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BaseFileCache {

	protected String baseCacheFileDirectory = "/usr/local/aap/cache";

	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	
	protected void writeToFile(String fileName,String jsonData){
		logger.info("Writing to file = {}",fileName);
		BufferedWriter writer = null;
		try
		{
			logger.info("Writing Data = {}",jsonData);
			writer = new BufferedWriter( new FileWriter( fileName));
			writer.write( jsonData);
			logger.info("Writing Data finished ");
		}
		catch ( Exception e)
		{
			logger.error("Error in writing File",e);
		}
		finally
		{
			try
			{
				if ( writer != null)
					writer.close( );
			}
			catch ( IOException e)
			{
				logger.error("Error in closing File",e);
			}
	     }
	}
	protected String readFromFile(String fileName){
		logger.info("Reading from file = {}",fileName);
		BufferedReader reader = null;
		String jsonData = null;
		try
		{
			reader = new BufferedReader( new FileReader( fileName));
			jsonData = reader.readLine();

		}
		catch ( Exception e)
		{
			logger.error("Error in reading File",e);
		}
		finally
		{
			try
			{
				if ( reader != null)
					reader.close( );
			}
			catch ( IOException e)
			{
				logger.error("Error in closing(Reading) File",e);
			}
	     }
		return jsonData;
	}
}
