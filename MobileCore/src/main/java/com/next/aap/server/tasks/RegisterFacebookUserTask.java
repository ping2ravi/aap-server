package com.next.aap.server.tasks;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.next.aap.dto.RegisterFacebookUserProfile;
import com.next.aap.messages.FacebookUserRegisteredMessage;
import com.next.aap.messages.NotificationMessage;
import com.next.aap.messages.TaskStatus;
import com.next.aap.server.messages.MessageSender;
import com.next.aap.server.service.AapService;

@Component
@Scope(value = "prototype")
public class RegisterFacebookUserTask implements Runnable {

	private RegisterFacebookUserProfile registerFacebookUserProfile;
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private AapService aapService;
	@Autowired
	private MessageSender messageSender;

	public void run() {
		if (registerFacebookUserProfile == null ) {
			logger.warn("No RegisterFacebookUserProfile provided for RegisterFacebookUserTask");
			return;
		}
		if (registerFacebookUserProfile.getDeviceRegId() == null || registerFacebookUserProfile.getDeviceRegId().trim().isEmpty()) {
			logger.warn("No GCM DeviceRegId provided for RegisterFacebookUserTask");
			return;
		}

		try {
			FacebookUserRegisteredMessage facebookUserRegisteredMessage = aapService.registerFacebookUser(registerFacebookUserProfile);

			if (TaskStatus.STATUS_COMPLETED
					.equals(facebookUserRegisteredMessage.getStatus()) || TaskStatus.STATUS_ALREADY_REGISTERED
					.equals(facebookUserRegisteredMessage.getStatus())) {
				// Now send the message to Client that registration is succesfull
				List<String> deviceList = new ArrayList<String>();
				deviceList.add(facebookUserRegisteredMessage.getDeviceRegId());
				messageSender.sendMessage(facebookUserRegisteredMessage,NotificationMessage.FACEBOOK_USER_REGISTERED_MESSAGE, deviceList);
				logger.info("Message Sent Succesfully");
			}

		} catch (Exception e) {
			logger.error("Unable to register facebook user {}", registerFacebookUserProfile.getUserName() + " , "+registerFacebookUserProfile.getEmail(), e);
		}

	}

	public RegisterFacebookUserProfile getRegisterFacebookUserProfile() {
		return registerFacebookUserProfile;
	}

	public void setRegisterFacebookUserProfile(
			RegisterFacebookUserProfile registerFacebookUserProfile) {
		this.registerFacebookUserProfile = registerFacebookUserProfile;
	}

}
