package com.next.aap.server.persistance.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.next.aap.server.persistance.UserDevice;
import com.next.aap.server.persistance.dao.UserDeviceDao;

@Component
public class UserDeviceDaoHibernateSpringImpl extends BaseDaoHibernateSpring<UserDevice> implements UserDeviceDao {

	/* (non-Javadoc)
	 * @see com.next.aap.server.persistance.dao.impl.UserDeviceDao#saveUserDevice(com.next.aap.server.persistance.UserDevice)
	 */
	@Override
	public UserDevice saveUserDevice(UserDevice userDevice) 
	{
		//checkIfStringMissing("Device Type", userDevice.getDeviceType());
		//checkIfStringMissing("Registration Id", userDevice.getRegId());
		userDevice = super.saveObject(userDevice);
		return userDevice;
	}

	/* (non-Javadoc)
	 * @see com.next.aap.server.persistance.dao.impl.UserDeviceDao#deleteUserDevice(com.next.aap.server.persistance.UserDevice)
	 */
	@Override
	public void deleteUserDevice(UserDevice userDevice)  {
		super.deleteObject(userDevice);
	}

	/* (non-Javadoc)
	 * @see com.next.aap.server.persistance.dao.impl.UserDeviceDao#getUserDeviceById(java.lang.Long)
	 */
	@Override
	public UserDevice getUserDeviceById(Long id) 
	{
		UserDevice userDevice = super.getObjectById(UserDevice.class, id);
		return userDevice;
	}
	
	public UserDevice getUserDeviceByRegId(String deviceRegId, String deviceType) {
		logger.info("getUserDeviceByRegId  {} and device type",deviceRegId,deviceType);
		UserDevice userDevice = executeQueryGetObject("from UserDevice where regId='"+deviceRegId+"' and deviceType='"+deviceType+"'");
		return userDevice;
	}

	public List<UserDevice> getAndroidUserDeviceAfterId(Long id, int pageSize) {
		logger.info("getUserDeviceAfterId  {}",id);
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", id);
		params.put("deviceType", "Android");
		String query = "from UserDevice where deviceType=:deviceType and id > :id order by id";
		return executeQueryGetList(query, params, pageSize);
	}
	/**
	 * @param pageInfo
	 * @return search result
	 * @
	 */
	/*
	public PageResult<UserDevice> searchUserDevices(PageInfo pageInfo) 
	{
		return super.findObject(UserDevice.class, pageInfo);
	}

	public List<UserDevice> getAllUserDevices() 
	{
		PageInfo pageInfo = null;
		return super.findObject(UserDevice.class, pageInfo).getResultList();
	}
	
	public List<UserDevice> getAndroidUserDeviceAfterId(Long id, int pageSize) {
		logger.info("getUserDeviceAfterId  {}",id);
		HibernateQueryParamPageInfo pageInfo = new HibernateQueryParamPageInfo();
		QueryParam emailQueryParam = new QueryParam();
		emailQueryParam.setCaseSenstive(false);
		emailQueryParam.setField("id");
		emailQueryParam.setFieldType(QueryParam.FIELD_TYPE_NUMBER);
		emailQueryParam.setOperator(QueryParam.OPERATOR_MORE_THEN);
		emailQueryParam.setValue(id);
		pageInfo.addCriteria(emailQueryParam);
		
		QueryParam deviceTypeQueryParam = new QueryParam();
		deviceTypeQueryParam.setCaseSenstive(false);
		deviceTypeQueryParam.setField("deviceType");
		deviceTypeQueryParam.setFieldType(QueryParam.FIELD_TYPE_STRING);
		deviceTypeQueryParam.setOperator(QueryParam.OPERATOR_EQUAL);
		deviceTypeQueryParam.setValue("Android");
		pageInfo.addCriteria(deviceTypeQueryParam);
		
		pageInfo.setPageSize(pageSize);
		
		pageInfo.addOrderBy("id", ORDER.ASC);
		
		PageResult<UserDevice> pr = this.searchUserDevices(pageInfo);
		return pr.getResultList();
	}
	*/
}