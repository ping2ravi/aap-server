package com.next.aap.server.tasks;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.next.aap.dto.BlogItem;
import com.next.aap.dto.NewsItem;
import com.next.aap.messages.NewBlogMessage;
import com.next.aap.messages.NewNewsMessage;
import com.next.aap.messages.NotificationMessage;
import com.next.aap.server.cache.HomeScreenDataCache;
import com.next.aap.server.service.AapService;
import com.next.aap.server.util.HttpUtil;

@Component
public class AapBlogDownloader extends BaseTask{

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	private static final String urlShortnerUrl = "http://myaap.in/yourls-api.php?format=json&username=arvind&password=4delhi&action=shorturl&url=";
	
	@Value("${work.in.task:true}")
	private boolean workInTask;
	
	@Value("${work.in.blog.download.task:true}")
	private boolean workInBlogDowloadTask = false;;

	private long lastBlogId = 0;
	
	@Autowired
	private HomeScreenDataCache homeScreenDataCache;
	
	@PostConstruct
	public void init(){
		lastBlogId = aapService.getLastBlogId();
	}

	@Scheduled(cron = "0 08,18,28,38,48,58 * * * *")
	public void downloadAapBlogs(){
		if(!(workInTask && workInBlogDowloadTask)){
			logger.warn("Aap Blog task is disabled");
			return;
		}
		try {
			downloadData(aapService);
			if(lastBlogId > 0){
				List<BlogItem> newBlogItems = aapService.getBlogItemsAfterId(lastBlogId);
				if(newBlogItems != null && !newBlogItems.isEmpty()){
					homeScreenDataCache.refreshFullCache();
					BlogItem blogItem;
					if(newBlogItems.size() == 1){
						blogItem = newBlogItems.get(0);
						NewBlogMessage newNewsMessage = new NewBlogMessage();
						newNewsMessage.setTotalItem(1);
						newNewsMessage.setNotificationTitle(blogItem.getTitle());
						newNewsMessage.setNotificationDescription(blogItem.getTitle());
						sendMessage(newNewsMessage, NotificationMessage.NEW_BLOG_MESSAGE);
					}else{
						NewBlogMessage newNewsMessage = new NewBlogMessage();
						blogItem = newBlogItems.get(newBlogItems.size() - 1);
						newNewsMessage.setTotalItem(newBlogItems.size());
						newNewsMessage.setNotificationTitle(blogItem.getTitle());
						newNewsMessage.setNotificationDescription(blogItem.getTitle());
						sendMessage(newNewsMessage, NotificationMessage.NEW_BLOG_MESSAGE);
					}
					lastBlogId = blogItem.getId();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void downloadData(AapService aapService) throws Exception {
		logger.info("Downloading AAP Blogs");
		WebDriver webDriver = new HtmlUnitDriver(true);
		List<WebElement> readMoreLinks;
		List<String> allLinks = new ArrayList<String>();
		String url;
		WebElement pageDiv;
		BlogItem oneBlogItem;
		String title;
		String content;
		HttpUtil httpUtil = new HttpUtil();

		WebDriver webDriverForPage = new HtmlUnitDriver(true);
		BlogItem existingItem;
		String listPageUrl;
		for (int i = 0; i >= 0; i--) {
			listPageUrl = "http://www.aamaadmiparty.org/blog?page=" + i;
			logger.info("Hitting Url : "+listPageUrl);
			webDriver.get(listPageUrl);
			readMoreLinks = webDriver.findElements(By.className("read-more"));
			for (int j = readMoreLinks.size() - 1; j >= 0; j--) {
				WebElement oneWebElement = readMoreLinks.get(j);
				url = oneWebElement.findElement(By.tagName("a")).getAttribute(
						"href");
				allLinks.add(url);
				existingItem = aapService.getBlogByOriginalUrl(url);
				if(existingItem != null){
					continue;
				}
				logger.info("Hitting News Url : "+url);
				webDriverForPage.get(url);
				Thread.sleep(1000);
				pageDiv = webDriverForPage.findElement(By
						.className("aap-page-content-class"));

				title = pageDiv.findElement(By.className("pane-title"))
						.getText();
				WebElement nodeContentElement = pageDiv.findElement(By.className("node__content"));
				List<WebElement> fieldItems = nodeContentElement.findElements(By.className("field__item"));
				String imgSrc = null;
				try{
					WebElement imgElement = fieldItems.get(0).findElement(By.tagName("img"));
					if(imgElement != null){
						imgSrc = imgElement.getAttribute("src");
					}
				}catch(Exception ex){
					
				}
				WebElement contentElement = pageDiv.findElement(By.className("field--type-text-with-summary"));
				
				
				content = (String)((JavascriptExecutor)webDriverForPage).executeScript("return arguments[0].innerHTML;", contentElement); 
				
				//content = contentElement.getText();
				

				oneBlogItem = new BlogItem();
				oneBlogItem.setSource("www.aamaadmiparty.org");
				oneBlogItem.setTitle(title);
				oneBlogItem.setImageUrl(imgSrc);
				oneBlogItem.setOriginalUrl(url);
				oneBlogItem.setGlobal(true);
				oneBlogItem.setWebUrl(getShortUrl(httpUtil, url));
				oneBlogItem.setContent(content);
				// oneBlogItem.setId(id++);
				oneBlogItem.setAuthor("");
				aapService.saveBlog(oneBlogItem);

			}
		}
		System.out.println("Total Urls = " + allLinks.size());
		webDriverForPage.close();
		webDriver.close();
	}

	private static String getShortUrl(HttpUtil httpUtil, String longUrl) throws Exception{
		HttpClient httpClient = new DefaultHttpClient();
		System.out.println("Long url = " + longUrl);
		String encodedUrl = URLEncoder.encode(longUrl, "UTF-8");
		System.out.println("encodedUrl url = " + encodedUrl);
		System.out.println("final url = " + urlShortnerUrl + encodedUrl);
		String dayDonationString = httpUtil.getResponse(httpClient,
				urlShortnerUrl + encodedUrl);
		System.out.println("dayDonationString = " + dayDonationString);
		JSONObject jsonObject = new JSONObject(dayDonationString);
		String status = jsonObject.getString("status");
		if ("fail".equals(status)) {
			String errorCode = jsonObject.getString("code");
			if ("error:keyword".equals(errorCode)) {
				throw new RuntimeException(jsonObject.getString("message"));
			} else {
				throw new RuntimeException(jsonObject.getString("message"));
			}
		} else {
			String shortUrl = jsonObject.getString("shorturl");
			return shortUrl;
		}

	}

}
