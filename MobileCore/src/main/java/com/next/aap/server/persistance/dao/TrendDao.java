package com.next.aap.server.persistance.dao;

import java.util.List;

import com.next.aap.server.persistance.Trend;

public interface TrendDao {

	/**
	 * Creates/updates a trend in Database
	 * 
	 * @param trend
	 * @return saved trend
	 * @
	 */
	public abstract Trend saveTrend(Trend trend);

	/**
	 * deletes a trend in Database
	 * 
	 * @param trend
	 * @return updated trend
	 * @
	 */
	public abstract void deleteTrend(Trend trend);

	/**
	 * return a Trend with given primary key/id
	 * 
	 * @param id
	 * @return Trend with PK as id(parameter)
	 * @
	 */
	public abstract Trend getTrendById(Long id);
	
	public abstract Trend getTrendByName(String name);

	public abstract List<Trend> getAllTrends();

}