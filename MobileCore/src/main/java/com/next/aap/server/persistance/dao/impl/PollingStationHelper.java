package com.next.aap.server.persistance.dao.impl;

import org.springframework.stereotype.Component;

import com.next.aap.server.persistance.PollingStation;

@Component
public class PollingStationHelper extends BaseDaoHibernateSpring<PollingStation>  {

	/**
	 * Creates/updates a pollingStation in Database
	 * 
	 * @param pollingStation
	 * @return saved pollingStation
	 * @throws AppException
	 */
	public PollingStation savePollingStation(PollingStation pollingStation)
	{
		checkIfPollingStationExistsWithSameName(pollingStation);
		pollingStation.setNameUp(pollingStation.getName().toUpperCase());
		pollingStation = super.saveObject(pollingStation);
		return pollingStation;
	}

	/**
	 * deletes a pollingStation in Database
	 * 
	 * @param pollingStation
	 * @return updated pollingStation
	 * @throws AppException
	 */
	public void deletePollingStation(PollingStation pollingStation) {
		super.deleteObject(pollingStation);
	}

	/**
	 * return a PollingStation with given primary key/id
	 * 
	 * @param id
	 * @return PollingStation with PK as id(parameter)
	 * @throws AppException
	 */
	public PollingStation getPollingStationById(Long id)
	{
		PollingStation pollingStation = super.getObjectById(PollingStation.class, id);
		return pollingStation;
	}

	private void checkIfPollingStationExistsWithSameName(PollingStation pollingStation) {
		
	}
	/**
	 * @param pageInfo
	 * @return search result
	 * @throws AppException
	 */
	/*
	public PageResult<PollingStation> searchPollingStations(PageInfo pageInfo) throws AppException
	{
		return super.findObject(PollingStation.class, pageInfo);
	}

	public List<PollingStation> getAllPollingStations() throws AppException
	{
		PageInfo pageInfo = null;
		return super.findObject(PollingStation.class, pageInfo).getResultList();
	}
	public List<PollingStation> getPollingStationOfState(long stateId) throws AppException{
		HibernateQueryParamPageInfo pageInfo = new HibernateQueryParamPageInfo();
		QueryParam stateQueryParam = new QueryParam();
		stateQueryParam.setCaseSenstive(false);
		stateQueryParam.setField("stateId");
		stateQueryParam.setFieldType(QueryParam.FIELD_TYPE_NUMBER);
		stateQueryParam.setOperator(QueryParam.OPERATOR_EQUAL);
		stateQueryParam.setValue(stateId);
		pageInfo.addCriteria(stateQueryParam);
		
		PageResult<PollingStation> pr = this.searchPollingStations(pageInfo);
		List<PollingStation> pollingStationList = pr.getResultList();
		return pollingStationList;
	}

	public void validateObjectForCreate(PollingStation pollingStation) throws AppException {
		checkIfStringMissing("Name", pollingStation.getName());
		checkIfObjectMissing("AssemblyConstituency", pollingStation.getAssemblyConstituency());
	}

	public void validateObjectForUpdate(PollingStation pollingStation) throws AppException {
		checkIfStringMissing("Name", pollingStation.getName());
		checkIfObjectMissing("AssemblyConstituency", pollingStation.getAssemblyConstituency());
	}
	private void checkIfPollingStationExistsWithSameName(PollingStation pollingStation) throws AppException{
		HibernateQueryParamPageInfo pageInfo = new HibernateQueryParamPageInfo();
		QueryParam emailQueryParam = new QueryParam();
		emailQueryParam.setCaseSenstive(false);
		emailQueryParam.setField("nameUp");
		emailQueryParam.setFieldType(QueryParam.FIELD_TYPE_STRING);
		emailQueryParam.setOperator(QueryParam.OPERATOR_EQUAL);
		emailQueryParam.setValue(pollingStation.getName().toUpperCase());
		pageInfo.addCriteria(emailQueryParam);
		
		QueryParam stateQueryParam = new QueryParam();
		stateQueryParam.setField("assemblyConstituencyId");
		stateQueryParam.setFieldType(QueryParam.FIELD_TYPE_NUMBER);
		stateQueryParam.setOperator(QueryParam.OPERATOR_EQUAL);
		stateQueryParam.setValue(pollingStation.getAssemblyConstituency().getId());
		pageInfo.addCriteria(stateQueryParam);
		
		if(pollingStation.getId() != null && pollingStation.getId() > 0){
			QueryParam pollingStationIdQueryParam = new QueryParam();
			pollingStationIdQueryParam.setCaseSenstive(false);
			pollingStationIdQueryParam.setField("id");
			pollingStationIdQueryParam.setFieldType(QueryParam.FIELD_TYPE_NUMBER);
			pollingStationIdQueryParam.setOperator(QueryParam.OPERATOR_NOT_EQUAL);
			pollingStationIdQueryParam.setValue(pollingStation.getId());
			pageInfo.addCriteria(pollingStationIdQueryParam);
		}
		
		PageResult<PollingStation> pr = this.searchPollingStations(pageInfo);
		if(pr.getResultList().size() > 0){
			throw new AppException("PollingStation already exists with name = "+pollingStation.getName()+" in state "+pollingStation.getAssemblyConstituency().getName());
		}
	}
	*/
}