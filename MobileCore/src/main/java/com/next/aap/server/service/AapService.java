package com.next.aap.server.service;

import java.util.List;

import com.next.aap.dto.AssemblyConstituencyDto;
import com.next.aap.dto.AssemblyConstituencyWeb;
import com.next.aap.dto.AssemblyConstituencyWithCandidate;
import com.next.aap.dto.BlogItem;
import com.next.aap.dto.CandidateDto;
import com.next.aap.dto.DistrictDto;
import com.next.aap.dto.DistrictWeb;
import com.next.aap.dto.ManifestoDto;
import com.next.aap.dto.NewsItem;
import com.next.aap.dto.ParliamentConstituencyDto;
import com.next.aap.dto.RegisterFacebookUserProfile;
import com.next.aap.dto.RegisterGoogleUserProfile;
import com.next.aap.dto.RegisterTwitterUserProfile;
import com.next.aap.dto.RegisterUserDevice;
import com.next.aap.dto.RegisterUserDeviceResponse;
import com.next.aap.dto.RegisterUserProfile;
import com.next.aap.dto.StateDto;
import com.next.aap.dto.UserDeviceDto;
import com.next.aap.dto.VideoItem;
import com.next.aap.messages.FacebookUserRegisteredMessage;
import com.next.aap.messages.GoogleUserRegisteredMessage;
import com.next.aap.messages.TwitterUserRegisteredMessage;
import com.next.aap.messages.UserRegisteredMessage;
import com.next.aap.server.persistance.User;

public interface AapService {

	User saveUser(User user);
	User getUserById(Long id);
	
	RegisterUserDeviceResponse registerDevice(RegisterUserDevice registerUserDevice);
	
	FacebookUserRegisteredMessage registerFacebookUser(RegisterFacebookUserProfile registerFacebookUserProfile) ;
	
	TwitterUserRegisteredMessage registerTwitterUser(RegisterTwitterUserProfile registerTwitterUserProfile) ;
	
	GoogleUserRegisteredMessage registerGoogleUser(RegisterGoogleUserProfile registerGoogleUserProfile) ;
	
	UserRegisteredMessage registerUser(RegisterUserProfile registerUserProfile);
	
	List<StateDto> getAllStates() ;
	
	StateDto getStateByName(String name) ;
	
	List<DistrictDto> getAllDistrictOfState(long stateId) ;
	
	List<AssemblyConstituencyDto> getAllAssemblyConstituenciesOfDistrict(long districtId) ;
	
	List<AssemblyConstituencyDto> getAllAssemblyConstituencies() ;
	
	List<ParliamentConstituencyDto> getAllParliamentConstituencies() ;
	
	List<ParliamentConstituencyDto> getAllParliamentConstituenciesOfState(long stateId) ;
	
	StateDto saveState(StateDto stateDto) ;
	
	DistrictWeb saveDistrict(DistrictWeb districtWeb) ;
	
	DistrictDto getDistrictByNameAndStateId(String name, Long stateId) ;
	
	List<DistrictWeb> saveDistricts(List<DistrictWeb> districtWeb) ;
	
	AssemblyConstituencyWeb saveAssemblyConstituency(AssemblyConstituencyWeb assemblyConstituencyWeb) ;
	
	List<AssemblyConstituencyWeb> saveAssemblyConstituencies(List<AssemblyConstituencyWeb> assemblyConstituencyList) ;
	
	CandidateDto saveCandidate(CandidateDto candidateDto) ;
	
	List<CandidateDto> getAllCandidates() ;

	ManifestoDto saveManifesto(ManifestoDto manifestoDto) ;
	
	List<ManifestoDto> getAllManifestos() ;
	
	List<AssemblyConstituencyWithCandidate> getAssemblyConstituencyWithCandidates(Long stateId);

	NewsItem saveNews(NewsItem newsItem);
	
	NewsItem getNewsByOriginalUrl(String originalUrl);
	
	List<NewsItem> getNews(int pageSize, int pageNumber);
	
	List<NewsItem> getAllNews();
	
	List<NewsItem> getNewsItemsAfterId(long newsId);
	
	List<Long> getNewsItemsOfAc(long acId);
	
	List<Long> getBlogItemsOfAc(long acId);
	
	List<Long> getVideoItemsOfAc(long acId);
	
	List<Long> getNewsItemsOfParliamentConstituency(long pcId);
	
	List<Long> getBlogItemsOfParliamentConstituency(long pcId);
	
	List<Long> getVideoItemsOfParliamentConstituency(long pcId);

	VideoItem saveVideo(VideoItem videoItem);
	
	VideoItem getVideoByVideoId(String videoId);
	
	List<VideoItem> getVideo(int pageSize, int pageNumber);
	
	List<VideoItem> getAllVideo();
	
	List<VideoItem> getVideoItemsAfterId(long videoId);

	BlogItem saveBlog(BlogItem blogItem);
	
	BlogItem getBlogByOriginalUrl(String originalUrl);
	
	List<BlogItem> getBlog(int pageSize, int pageNumber);
	
	List<BlogItem> getAllBlog();
	
	List<BlogItem> getBlogItemsAfterId(long blogId);
	
	List<UserDeviceDto> getAndroidUserDevices(long startId, int pageSize);
	
	List<String> getAndroidUserDevicesForMessageSending(long startId, int pageSize);
	
	long getLastNewsId();
	
	long getLastVideoId();
	
	long getLastBlogId();
	
	ParliamentConstituencyDto saveParliamentConstituency(ParliamentConstituencyDto parliamentConstituencyDto);

}
