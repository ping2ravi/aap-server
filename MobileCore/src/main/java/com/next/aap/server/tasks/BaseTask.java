package com.next.aap.server.tasks;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.next.aap.dto.UserDeviceDto;
import com.next.aap.server.messages.MessageSender;
import com.next.aap.server.service.AapService;

public class BaseTask {

	@Autowired
	protected AapService aapService;
	
	@Autowired
	protected MessageSender messageSender;
	

	
	protected void sendMessage(Object message,String messageType) throws IOException{
		long startId = 0;
		int pageSize = 100;
		while(true){
			List<UserDeviceDto> userDevices = aapService.getAndroidUserDevices(startId, pageSize);
			if(userDevices == null || userDevices.size() == 0){
				break;
			}
			List<String> deviceList = new ArrayList<>(userDevices.size());
			for(UserDeviceDto oneUserDeviceDto:userDevices){
				deviceList.add(oneUserDeviceDto.getRegId());
				startId = oneUserDeviceDto.getId();
			}
			messageSender.sendMessage(message, messageType, deviceList);
		}
	}

}
