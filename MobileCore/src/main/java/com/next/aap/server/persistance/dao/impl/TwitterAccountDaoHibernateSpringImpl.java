package com.next.aap.server.persistance.dao.impl;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.stereotype.Component;

import com.next.aap.server.persistance.TwitterAccount;
import com.next.aap.server.persistance.dao.TwitterAccountDao;

@Component
public class TwitterAccountDaoHibernateSpringImpl extends BaseDaoHibernateSpring<TwitterAccount> implements TwitterAccountDao{


	/* (non-Javadoc)
	 * @see com.next.aap.server.persistance.dao.impl.TwitterAccountDao#saveTwitterAccount(com.next.aap.server.persistance.TwitterAccount)
	 */
	@Override
	public TwitterAccount saveTwitterAccount(TwitterAccount twitterAccount){
		checkIfStringMissing("Twitter account Handle", twitterAccount.getScreenName());
		twitterAccount.setScreenNameCap(twitterAccount.getScreenName().toUpperCase());
		twitterAccount = super.saveObject(twitterAccount);
		return twitterAccount;
	}

	/* (non-Javadoc)
	 * @see com.next.aap.server.persistance.dao.impl.TwitterAccountDao#getTwitterAccountById(java.lang.Long)
	 */
	@Override
	public TwitterAccount getTwitterAccountById(Long id) {
		return super.getObjectById(TwitterAccount.class, id);
	}
	/* (non-Javadoc)
	 * @see com.next.aap.server.persistance.dao.impl.TwitterAccountDao#getTwitterAccountByUserId(java.lang.Long)
	 */
	@Override
	public TwitterAccount getTwitterAccountByUserId(Long userId) {
		Map<String, Object> params = new TreeMap<String, Object>();
		params.put("userId", userId);
		TwitterAccount twitterAccount = executeQueryGetObject("from TwitterAccount where userId = :userId", params);
		return twitterAccount;
	}
	/* (non-Javadoc)
	 * @see com.next.aap.server.persistance.dao.impl.TwitterAccountDao#getTwitterAccountByUserName(java.lang.String)
	 */
	@Override
	public TwitterAccount getTwitterAccountByHandle(String userName){
		Map<String, Object> params = new TreeMap<String, Object>();
		params.put("screenNameCap", userName.toUpperCase());
		TwitterAccount twitterAccount = executeQueryGetObject("from TwitterAccount where screenNameCap = :screenNameCap", params);
		return twitterAccount;
	}
	
	/* (non-Javadoc)
	 * @see com.next.aap.server.persistance.dao.impl.TwitterAccountDao#getTwitterAccountsAfterId(java.lang.Long, int)
	 */
	@Override
	public List<TwitterAccount> getTwitterAccountsAfterId(Long lastId,int pageSize){
		Map<String, Object> params = new TreeMap<String, Object>();
		params.put("lastId", lastId);
		params.put("pageSize", pageSize);
		List<TwitterAccount> list = executeQueryGetList("from TwitterAccount where id > :lastId order by id asc limit :pageSize");
		return list;
	}

	@Override
	public TwitterAccount getTwitterAccountByTwitterUserId(String twitterId) {
		Map<String, Object> params = new TreeMap<String, Object>();
		params.put("twitterId", twitterId);
		TwitterAccount twitterAccount = executeQueryGetObject("from TwitterAccount where twitterId = :twitterId", params);
		return twitterAccount;
	}
}
