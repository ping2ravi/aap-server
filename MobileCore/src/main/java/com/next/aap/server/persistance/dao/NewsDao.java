package com.next.aap.server.persistance.dao;

import java.util.List;

import com.next.aap.server.persistance.News;

public interface NewsDao {

	public abstract News saveNews(News news);
	
	public abstract void deleteNews(News news);
	
	public abstract News getNewsById(Long id);
	
	public abstract List<News> getAllNewss(int totalItems, int pageNumber);
	
	public abstract List<News> getAllNewss();
	
	public abstract News getNewsByWebUrl(String webUrl);
	
	public abstract News getNewsByOriginalUrl(String originalUrl);
	
	public abstract long getLastNewsId();
	
	public abstract List<News> getNewsItemsAfterId(long newsId);
	
	public abstract List<Long> getNewsByLocation(long acId, long districtId, long stateId);
	
	public abstract List<Long> getNewsByLocation(long pcId, long stateId);
	
}
