package com.next.aap.server.cache.db;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.next.aap.dto.AssemblyConstituencyDto;
import com.next.aap.dto.DistrictDto;
import com.next.aap.dto.ParliamentConstituencyDto;
import com.next.aap.dto.StateDto;
import com.next.aap.server.cache.LocationCache;
import com.next.aap.server.service.AapService;

@Component
public class LocationCacheDbImpl implements LocationCache {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private AapService aapService;
	
	private boolean loadCacheOnStartup;
	
	private boolean cacheLoaded = false;;
	
	@Value("${load.cache.on.startup}")
	public void setDatabaseName(boolean loadCacheOnStartup) {
		this.loadCacheOnStartup = loadCacheOnStartup;
	}

	private String allStates;
	private Map<Long, String> districtMap;
	private Map<Long, String> acMap;
	Map<Long, String> stateToAcMap = new HashMap<Long, String>();
	
	Map<Long, String> stateToPcMap = new HashMap<Long, String>();
	
	@PostConstruct
	public void init(){
		if(loadCacheOnStartup){
			refreshCache();
		}
	}
	private boolean alreadyRunning = false;
	/* (non-Javadoc)
	 * @see com.next.aap.server.cache.db.LocaltionCache#refreshCacheAsync()
	 */
	@Override
	public String refreshCacheAsync(){
		
		synchronized (LocationCacheDbImpl.class) {
			if(alreadyRunning){
				return "Task Already Running";
			}
			Runnable task = new Runnable() {
				
				public void run() {
					refreshCache();
				}
			};
			new Thread(task).start();
		}
		return "Task Started";
	}
	/* (non-Javadoc)
	 * @see com.next.aap.server.cache.db.LocaltionCache#refreshCache()
	 */
	@Override
	public void refreshCache(){
		logger.info("refreshing Location cache");
		List<StateDto> allDbStates;
		Map<Long, String> localAcMap = new HashMap<Long, String>();
		Map<Long, String> localDistrictMap = new HashMap<Long, String>();
		Map<Long, String> localStateToAcMap = new HashMap<Long, String>();
		try {
			allDbStates = aapService.getAllStates();
			Gson gson = new Gson();
			//Create State Cache
			String localAllStates = gson.toJson(allDbStates);
			
			//Create District Cache
			List<DistrictDto> allDistricts;
			List<AssemblyConstituencyDto> allAssemblyConstituencies;
			List<AssemblyConstituencyDto> allStateAssemblyConstituencies = new ArrayList<AssemblyConstituencyDto>();
			List<ParliamentConstituencyDto> parliamentConstituencyDtos;
			for(StateDto oneStateDto:allDbStates){
				allStateAssemblyConstituencies.clear();
				allDistricts = aapService.getAllDistrictOfState(oneStateDto.getId());
				localDistrictMap.put(oneStateDto.getId(), gson.toJson(allDistricts));
				//Create Assembly COnstituency Cache
				for(DistrictDto oneDistrictDto:allDistricts){
					allAssemblyConstituencies = aapService.getAllAssemblyConstituenciesOfDistrict(oneDistrictDto.getId());
					allStateAssemblyConstituencies.addAll(allAssemblyConstituencies);
					localAcMap.put(oneDistrictDto.getId(), gson.toJson(allAssemblyConstituencies));
					
				}
				localStateToAcMap.put(oneStateDto.getId(), gson.toJson(allStateAssemblyConstituencies));
				
				parliamentConstituencyDtos = aapService.getAllParliamentConstituenciesOfState(oneStateDto.getId());
				stateToPcMap.put(oneStateDto.getId(), gson.toJson(parliamentConstituencyDtos));
			}
			this.allStates = localAllStates;
			this.districtMap = localDistrictMap;
			this.acMap = localAcMap;
			this.stateToAcMap = localStateToAcMap;
		}catch (Exception e) {
			logger.error("Error occured while refreshing Location cache",e);
		}finally{
			logger.info("refreshing Location cache Ended");
		}
	}

	/* (non-Javadoc)
	 * @see com.next.aap.server.cache.db.LocaltionCache#getAllStates()
	 */
	@Override
	public String getAllStates() {
		return allStates;
	}

	/* (non-Javadoc)
	 * @see com.next.aap.server.cache.db.LocaltionCache#getAllDistrictOfState(java.lang.Long)
	 */
	@Override
	public String getAllDistrictOfState(Long stateId) {
		return districtMap.get(stateId);
	}

	/* (non-Javadoc)
	 * @see com.next.aap.server.cache.db.LocaltionCache#getAllAssemblyConstituenciesOfDistrict(java.lang.Long)
	 */
	@Override
	public String getAllAssemblyConstituenciesOfDistrict(Long districtId) {
		return acMap.get(districtId);
	}
	/* (non-Javadoc)
	 * @see com.next.aap.server.cache.db.LocaltionCache#getAllAssemblyConstituenciesOfState(java.lang.Long)
	 */
	@Override
	public String getAllAssemblyConstituenciesOfState(Long stateId) {
		return stateToAcMap.get(stateId);
	}
	@Override
	public String getAllParliamentConstituenciesOfState(Long stateId) {
		return stateToPcMap.get(stateId);
	}

}
