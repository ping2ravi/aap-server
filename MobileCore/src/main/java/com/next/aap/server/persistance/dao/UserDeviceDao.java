package com.next.aap.server.persistance.dao;

import java.util.List;

import com.next.aap.server.persistance.UserDevice;

public interface UserDeviceDao {

	/**
	 * Creates/updates a userDevice in Database
	 * 
	 * @param userDevice
	 * @return saved userDevice
	 * @
	 */
	public abstract UserDevice saveUserDevice(UserDevice userDevice);

	/**
	 * deletes a userDevice in Database
	 * 
	 * @param userDevice
	 * @return updated userDevice
	 * @
	 */
	public abstract void deleteUserDevice(UserDevice userDevice);

	/**
	 * return a UserDevice with given primary key/id
	 * 
	 * @param id
	 * @return UserDevice with PK as id(parameter)
	 * @
	 */
	public abstract UserDevice getUserDeviceById(Long id);
	
	/**
	 * 
	 * @param deviceRegId
	 * @param deviceType
	 * @return
	 */
	public abstract UserDevice getUserDeviceByRegId(String deviceRegId, String deviceType);
	
	/**
	 * 
	 * @param id
	 * @param pageSize
	 * @return
	 */
	public List<UserDevice> getAndroidUserDeviceAfterId(Long id, int pageSize); 

}