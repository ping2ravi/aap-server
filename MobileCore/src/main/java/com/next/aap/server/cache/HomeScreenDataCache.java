package com.next.aap.server.cache;

import com.next.aap.dto.BlogItem;
import com.next.aap.dto.EventItem;
import com.next.aap.dto.NewsItem;
import com.next.aap.dto.VideoItem;


public interface HomeScreenDataCache {

	String getHomeScreenItems(String lang, long livingAcId, long votingAcId, long livingPcId, long votingPcId);
	
	String getNewsItems(String lang, long livingAcId, long votingAcId, long livingPcId, long votingPcId);
	
	String getNewsItems(String lang, long livingAcId, long votingAcId, long livingPcId, long votingPcId, int pageNumber);
	
	String getVideoItems(String lang, long livingAcId, long votingAcId, long livingPcId, long votingPcId);
	
	String getVideoItems(String lang, long livingAcId, long votingAcId, long livingPcId, long votingPcId, int pageNumber);

	String getEventItems(String lang, long livingAcId, long votingAcId, long livingPcId, long votingPcId);
	
	String getBlogItems(String lang, long livingAcId, long votingAcId, long livingPcId, long votingPcId);
	
	String getBlogItems(String lang, long livingAcId, long votingAcId, long livingPcId, long votingPcId, int pageNumber);
	
	void addNewsItemToCache(NewsItem newsItem, String lang);
	
	void addVideoItemToCache(VideoItem videoItem, String lang);
	
	void addBlogItemToCache(BlogItem blogItem, String lang);
	
	void addEventItemToCache(EventItem eventItem, String lang);
	
	void clearAllNewsItems();
	
	void clearNewsItem(Long itemId);
	
	void clearAllVideoItems();
	
	void clearVideoItem(Long itemId);
	
	void clearAllEventItems();
	
	void clearEventItem(Long itemId);
	
	void clearAllBlogItems();
	
	void clearBlogItem(Long itemId);
	
	void refreshFullCache();
	
}
