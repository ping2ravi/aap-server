package com.next.aap.server.cache.db;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.next.aap.dto.AllHomeScreenItem;
import com.next.aap.dto.AssemblyConstituencyDto;
import com.next.aap.dto.BlogItem;
import com.next.aap.dto.BlogItemList;
import com.next.aap.dto.EventItem;
import com.next.aap.dto.NewsItem;
import com.next.aap.dto.NewsItemList;
import com.next.aap.dto.ParliamentConstituencyDto;
import com.next.aap.dto.VideoItem;
import com.next.aap.dto.VideoItemList;
import com.next.aap.server.cache.HomeScreenDataCache;


@Service
public class AapDataCacheDbImpl extends BaseDbCache implements HomeScreenDataCache{
	

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private AapDataHolder aapDataHolder;
	
	private boolean loadCacheOnStartup;
	
	private boolean cacheLoaded = false;;
	
	@Value("${load.cache.on.startup}")
	public void setDatabaseName(boolean loadCacheOnStartup) {
		this.loadCacheOnStartup = loadCacheOnStartup;
	}
	
	/*
	public AapDataCacheDbImpl(){
		try {
			JCS cache = JCS.getInstance("CACHE");
		} catch (CacheException e) {
			logger.error("Unable to create cache object", e);
			e.printStackTrace();
		}	
	}
	*/
	
	
	private static final int MAX_NEWS_ITEM = 10;
	private static final int MAX_VIDEO_ITEM = 10;
	private static final int MAX_BLOG_ITEM = 10;
	private static final int MAX_EVENT_ITEM = 10;
	private static final String DEFAULT_LANGUAGE = "en";
	
	private Set<String> allValidLanguages;

	@PostConstruct
	public void init(){
		System.out.println("loadCacheOnStartup="+loadCacheOnStartup);
		if(loadCacheOnStartup){
			refreshFullCache();
		}
	}

	@Override
	public void refreshFullCache(){
		
		
		AapDataHolder localAapDataHolder = new AapDataHolder();

		
		loadAllNews(localAapDataHolder);
		loadAllBlogs(localAapDataHolder);
		loadAllVideos(localAapDataHolder);
		
		loadAllNewsItemsOfAssemblyConstituencies(localAapDataHolder);
		loadAllBlogItemsOfAssemblyConstituencies(localAapDataHolder);
		loadAllVideoItemsOfAssemblyConstituencies(localAapDataHolder);
		
		loadAllNewsItemsOfParliamentConstituencies(localAapDataHolder);
		loadAllBlogItemsOfParliamentConstituencies(localAapDataHolder);
		loadAllVideoItemsOfParliamentConstituencies(localAapDataHolder);
		
		
		this.aapDataHolder = localAapDataHolder;
		cacheLoaded = true;
		System.out.println("Loading Done");
	}
	
	private void loadAllNews(AapDataHolder localAapDataHolder){
		List<NewsItem> allNewsItems = aapService.getAllNews();
		for(NewsItem oneNewsItem:allNewsItems){
			localAapDataHolder.addNews(DEFAULT_LANGUAGE, oneNewsItem);
		}
	}
	
	private void loadAllBlogs(AapDataHolder localAapDataHolder){
		List<BlogItem> allBlogItems = aapService.getAllBlog();
		for(BlogItem oneBlogItem:allBlogItems){
			localAapDataHolder.addBlog(DEFAULT_LANGUAGE, oneBlogItem);
		}
	}

	private void loadAllVideos(AapDataHolder localAapDataHolder){
		List<VideoItem> allVideoItems = aapService.getAllVideo();
		for(VideoItem oneVideoItem:allVideoItems){
			localAapDataHolder.addVideo(DEFAULT_LANGUAGE, oneVideoItem);
		}
	}
	
	
	private void loadAllNewsItemsOfAssemblyConstituencies(AapDataHolder localAapDataHolder){
		List<AssemblyConstituencyDto> allAcs = aapService.getAllAssemblyConstituencies();
		for(AssemblyConstituencyDto oneAc:allAcs){
			List<Long> newsIds = aapService.getNewsItemsOfAc(oneAc.getId());
			localAapDataHolder.addAcNews(oneAc.getId(), newsIds);
		}
	}
	
	private void loadAllBlogItemsOfAssemblyConstituencies(AapDataHolder localAapDataHolder){
		List<AssemblyConstituencyDto> allAcs = aapService.getAllAssemblyConstituencies();
		for(AssemblyConstituencyDto oneAc:allAcs){
			List<Long> newsIds = aapService.getBlogItemsOfAc(oneAc.getId());
			localAapDataHolder.addAcBlogs(oneAc.getId(), newsIds);
		}
	}
	
	private void loadAllVideoItemsOfAssemblyConstituencies(AapDataHolder localAapDataHolder){
		List<AssemblyConstituencyDto> allAcs = aapService.getAllAssemblyConstituencies();
		for(AssemblyConstituencyDto oneAc:allAcs){
			List<Long> newsIds = aapService.getVideoItemsOfAc(oneAc.getId());
			localAapDataHolder.addAcVideos(oneAc.getId(), newsIds);
		}
	}
	
	private void loadAllNewsItemsOfParliamentConstituencies(AapDataHolder localAapDataHolder){
		List<ParliamentConstituencyDto> allAcs = aapService.getAllParliamentConstituencies();
		for(ParliamentConstituencyDto onePc:allAcs){
			List<Long> newsIds = aapService.getNewsItemsOfParliamentConstituency(onePc.getId());
			localAapDataHolder.addPcNews(onePc.getId(), newsIds);
		}
	}
	
	private void loadAllBlogItemsOfParliamentConstituencies(AapDataHolder localAapDataHolder){
		List<ParliamentConstituencyDto> allAcs = aapService.getAllParliamentConstituencies();
		for(ParliamentConstituencyDto onePc:allAcs){
			List<Long> newsIds = aapService.getBlogItemsOfParliamentConstituency(onePc.getId());
			localAapDataHolder.addPcBlogs(onePc.getId(), newsIds);
		}
	}
	
	private void loadAllVideoItemsOfParliamentConstituencies(AapDataHolder localAapDataHolder){
		List<ParliamentConstituencyDto> allAcs = aapService.getAllParliamentConstituencies();
		for(ParliamentConstituencyDto onePc:allAcs){
			List<Long> newsIds = aapService.getVideoItemsOfParliamentConstituency(onePc.getId());
			localAapDataHolder.addPcVideos(onePc.getId(), newsIds);
		}
	}
	
	

	protected String convertToJson(Object object){
		Gson gson = new Gson();
		String returnJson = gson.toJson(object);
		return returnJson;
	}

	@Override
	public String getHomeScreenItems(String lang, long livingAcId, long votingAcId, long livingPcId, long votingPcId) {
		AllHomeScreenItem allHomeScreenItem = new AllHomeScreenItem();
		List<NewsItem> newsItems = aapDataHolder.getNewsItems(lang, livingAcId, votingAcId, livingPcId, votingPcId,  1, MAX_NEWS_ITEM);
		allHomeScreenItem.setNewsItems(newsItems);
		List<VideoItem> videoItems = aapDataHolder.getVideoItems(lang, livingAcId, votingAcId, livingPcId, votingPcId, 1, MAX_VIDEO_ITEM);
		allHomeScreenItem.setVideoItems(videoItems);
		List<BlogItem> blogItems = aapDataHolder.getBlogItems(lang, livingAcId, votingAcId, livingPcId, votingPcId, 1, MAX_BLOG_ITEM);
		allHomeScreenItem.setBlogItems(blogItems);
		return convertToJson(allHomeScreenItem);
	}

	@Override
	public String getNewsItems(String lang, long livingAcId, long votingAcId, long livingPcId, long votingPcId) {
		return getNewsItems(lang, livingAcId, votingAcId, livingPcId, votingPcId, 1);
	}
	@Override
	public String getNewsItems(String lang, long livingAcId, long votingAcId, long livingPcId, long votingPcId, int pageNumber) {
		return getNewsItems(lang, livingAcId, votingAcId, livingPcId, votingPcId, pageNumber, MAX_NEWS_ITEM);
	}
	
	private String getNewsItems(String lang, long livingAcId, long votingAcId, long livingPcId, long votingPcId, int pageNumber, int pageSize) {
		NewsItemList newsItemList = new NewsItemList();
		List<NewsItem> newsItems = aapDataHolder.getNewsItems(lang, livingAcId, votingAcId, livingPcId, votingPcId, pageNumber, pageSize);
		newsItemList.setNewsItems(newsItems);
		newsItemList.setPageNumber(1);
		newsItemList.setPageSize(pageSize);
		newsItemList.setTotalPages(aapDataHolder.getTotalNewsItemPages(lang, livingAcId, votingAcId, livingPcId, votingPcId, pageSize));
		return convertToJson(newsItemList);
	}

	@Override
	public String getVideoItems(String lang, long livingAcId, long votingAcId, long livingPcId, long votingPcId) {
		return getVideoItems(lang, livingAcId, votingAcId, livingPcId, votingPcId, 1);
	}
	@Override
	public String getVideoItems(String lang, long livingAcId, long votingAcId, long livingPcId, long votingPcId, int pageNumber) {
		return getVideoItems(lang, livingAcId, votingAcId, livingPcId, votingPcId, pageNumber, MAX_VIDEO_ITEM);
	}

	private String getVideoItems(String lang, long livingAcId, long votingAcId, long livingPcId, long votingPcId, int pageNumber, int pageSize) {
		VideoItemList newsItemList = new VideoItemList();
		List<VideoItem> videoItems = aapDataHolder.getVideoItems(lang, livingAcId, votingAcId, livingPcId, votingPcId, pageNumber, pageSize);
		newsItemList.setVideoItems(videoItems);
		newsItemList.setPageNumber(pageNumber);
		newsItemList.setPageSize(pageSize);
		newsItemList.setTotalPages(aapDataHolder.getTotalNewsItemPages(lang, livingAcId, votingAcId, livingPcId, votingPcId, pageSize));
		return convertToJson(newsItemList);
	}

	@Override
	public String getEventItems(String lang, long livingAcId, long votingAcId, long livingPcId, long votingPcId) {
		return null;//getJsonStringFromMapByLanguage(allEventItemJsonStringByLang, lang);
	}

	@Override
	public String getBlogItems(String lang, long livingAcId, long votingAcId, long livingPcId, long votingPcId) {
		return getBlogItems(lang, livingAcId, votingAcId, livingPcId, votingPcId, 1);
	}
	@Override
	public String getBlogItems(String lang, long livingAcId, long votingAcId, long livingPcId, long votingPcId, int pageNumber) {
		return getBlogItems(lang, livingAcId, votingAcId, livingPcId, votingPcId, pageNumber, MAX_BLOG_ITEM);
	}


	private String getBlogItems(String lang, long livingAcId, long votingAcId, long livingPcId, long votingPcId, int pageNumber, int pageSize) {
		BlogItemList blogItemList = new BlogItemList();
		List<BlogItem> blogItems = aapDataHolder.getBlogItems(lang, livingAcId, votingAcId, livingPcId, votingPcId, pageNumber, pageSize);
		blogItemList.setBlogItems(blogItems);
		blogItemList.setPageNumber(pageNumber);
		blogItemList.setPageSize(pageSize);
		blogItemList.setTotalPages(aapDataHolder.getTotalNewsItemPages(lang, livingAcId, votingAcId, livingPcId, votingPcId, pageSize));
		return convertToJson(blogItemList);
	}

	@Override
	public void addNewsItemToCache(NewsItem newsItem, String lang) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addVideoItemToCache(VideoItem videoItem, String lang) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addBlogItemToCache(BlogItem blogItem, String lang) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addEventItemToCache(EventItem eventItem, String lang) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clearAllNewsItems() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clearNewsItem(Long itemId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clearAllVideoItems() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clearVideoItem(Long itemId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clearAllEventItems() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clearEventItem(Long itemId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clearAllBlogItems() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clearBlogItem(Long itemId) {
		// TODO Auto-generated method stub
		
	}
	private String getJsonStringFromMapByLanguage(Map<String, String> langMap, String lang){
		lang = getValidLanugae(lang);
		String returnString = langMap.get(lang);
		if(returnString == null){
			returnString = langMap.get(DEFAULT_LANGUAGE);
		}
		return returnString;
	}
	private String getValidLanugae(String lang){
		lang = lang.toLowerCase();
		if(allValidLanguages.contains(lang)){
			return lang;
		}
		return DEFAULT_LANGUAGE;
	}

}
