package com.next.aap.server.cache.file;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

import com.google.gson.Gson;
import com.next.aap.dto.AllHomeScreenItem;
import com.next.aap.dto.BlogItem;
import com.next.aap.dto.EventItem;
import com.next.aap.dto.NewsItem;
import com.next.aap.dto.VideoItem;
import com.next.aap.server.cache.HomeScreenDataCache;

public class HomeScreenDataCacheFileImpl extends BaseFileCache implements HomeScreenDataCache {
	private Map<String, AllHomeScreenItem> languageBasedHomeScreenItems = new HashMap<String, AllHomeScreenItem>();
	private Map<String, String> allHomeScreenItemJsonStringByLang = new HashMap<String, String>();
	private Map<String, String> allNewsItemJsonStringByLang = new HashMap<String, String>();
	private Map<String, String> allVideoItemJsonStringByLang = new HashMap<String, String>();
	private Map<String, String> allEventItemJsonStringByLang = new HashMap<String, String>();
	private Map<String, String> allBlogItemJsonStringByLang = new HashMap<String, String>();
	
	private static final int MAX_NEWS_ITEM = 10;
	private static final int MAX_VIDEO_ITEM = 10;
	private static final int MAX_BLOG_ITEM = 10;
	private static final int MAX_EVENT_ITEM = 10;
	private static final String DEFAULT_LANGUAGE = "en";
	
	private String cacheFileDirectory = baseCacheFileDirectory + "/allhomeitem.json";
	
	private Set<String> allValidLanguages;

	public AllHomeScreenItem createAllHomeScreenItem(){
		AllHomeScreenItem allHomeScreenItem = new AllHomeScreenItem();
		allHomeScreenItem.setNewsItems(new ArrayList<NewsItem>(MAX_NEWS_ITEM + 1));
		allHomeScreenItem.setVideoItems(new ArrayList<VideoItem>(MAX_VIDEO_ITEM + 1));
		allHomeScreenItem.setEventItems(new ArrayList<EventItem>(MAX_EVENT_ITEM + 1));
		allHomeScreenItem.setBlogItems(new ArrayList<BlogItem>(MAX_BLOG_ITEM + 1));

		return  allHomeScreenItem;
	}
	public HomeScreenDataCacheFileImpl(){
		AllHomeScreenItem allHomeScreenItemForEnglish = createAllHomeScreenItem();
		languageBasedHomeScreenItems.put(DEFAULT_LANGUAGE, allHomeScreenItemForEnglish);
		
		allValidLanguages = new TreeSet<String>();
		allValidLanguages.add(DEFAULT_LANGUAGE);
		allValidLanguages.add("hin");
	}
	private String getValidLanugae(String lang){
		lang = lang.toLowerCase();
		if(allValidLanguages.contains(lang)){
			return lang;
		}
		return DEFAULT_LANGUAGE;
	}
	
	private String getJsonStringFromMapByLanguage(Map<String, String> langMap, String lang){
		lang = getValidLanugae(lang);
		String returnString = langMap.get(lang);
		if(returnString == null){
			returnString = langMap.get(DEFAULT_LANGUAGE);
		}
		return returnString;
	}
	public String getHomeScreenItems(String lang) {
		return getJsonStringFromMapByLanguage(allHomeScreenItemJsonStringByLang, lang);
	}

	public String getNewsItems(String lang) {
		return getJsonStringFromMapByLanguage(allNewsItemJsonStringByLang, lang);
	}

	public String getVideoItems(String lang) {
		return getJsonStringFromMapByLanguage(allVideoItemJsonStringByLang, lang);
	}

	public String getEventItems(String lang) {
		return getJsonStringFromMapByLanguage(allEventItemJsonStringByLang, lang);
	}

	public String getBlogItems(String lang) {
		return getJsonStringFromMapByLanguage(allBlogItemJsonStringByLang, lang);
	}

	protected synchronized <T> void addItemToList(List<T> list, T item, int maxSize){
		list.add(0, item);
		while(list.size() > maxSize){
			list.remove(list.size() - 1);
		}
	}
	private AllHomeScreenItem getAllHomeScreenItemForLanguage(String lang){
		lang = getValidLanugae(lang);
		AllHomeScreenItem allHomeScreenItem = languageBasedHomeScreenItems.get(lang);
		if(allHomeScreenItem == null){
			allHomeScreenItem = createAllHomeScreenItem();
			languageBasedHomeScreenItems.put(lang, allHomeScreenItem);
		}
		return allHomeScreenItem;
	}
	public synchronized void addNewsItemToCache(NewsItem newsItem, String lang) {
		AllHomeScreenItem allHomeScreenItem =  getAllHomeScreenItemForLanguage(lang);
		addItemToList(allHomeScreenItem.getNewsItems(), newsItem, MAX_NEWS_ITEM);
		String allHomeScreenItemJsonString = convertToJson(allHomeScreenItem, lang);
		writeToFile(cacheFileDirectory, allHomeScreenItemJsonString);
	}

	public void addVideoItemToCache(VideoItem videoItem, String lang) {
		AllHomeScreenItem allHomeScreenItem =  getAllHomeScreenItemForLanguage(lang);
		addItemToList(allHomeScreenItem.getVideoItems(), videoItem, MAX_VIDEO_ITEM);
		String allHomeScreenItemJsonString = convertToJson(allHomeScreenItem, lang);
		writeToFile(cacheFileDirectory, allHomeScreenItemJsonString);
	}

	public void addBlogItemToCache(BlogItem blogItem, String lang) {
		AllHomeScreenItem allHomeScreenItem =  getAllHomeScreenItemForLanguage(lang);
		addItemToList(allHomeScreenItem.getBlogItems(), blogItem, MAX_BLOG_ITEM);
		String allHomeScreenItemJsonString = convertToJson(allHomeScreenItem, lang);
		writeToFile(cacheFileDirectory, allHomeScreenItemJsonString);
	}

	public void addEventItemToCache(EventItem eventItem, String lang) {
		AllHomeScreenItem allHomeScreenItem =  getAllHomeScreenItemForLanguage(lang);
		addItemToList(allHomeScreenItem.getEventItems(), eventItem, MAX_EVENT_ITEM);
		String allHomeScreenItemJsonString = convertToJson(allHomeScreenItem, lang);
		writeToFile(cacheFileDirectory, allHomeScreenItemJsonString);
	}
	protected String convertToJson(AllHomeScreenItem allHomeScreenItem, String lang){
		Gson gson = new Gson();
		String returnJson = gson.toJson(allHomeScreenItem);
		allHomeScreenItemJsonStringByLang.put(lang, returnJson);
		allNewsItemJsonStringByLang.put(lang, gson.toJson(allHomeScreenItem.getNewsItems()));
		allVideoItemJsonStringByLang.put(lang, gson.toJson(allHomeScreenItem.getVideoItems()));
		allEventItemJsonStringByLang.put(lang, gson.toJson(allHomeScreenItem.getEventItems()));
		allBlogItemJsonStringByLang.put(lang, gson.toJson(allHomeScreenItem.getBlogItems()));
		return returnJson;
	}
	@Override
	public void clearAllNewsItems() {
		allNewsItemJsonStringByLang.clear();
	}
	@Override
	public void clearAllVideoItems() {
		allVideoItemJsonStringByLang.clear();
	}
	@Override
	public void clearAllEventItems() {
		allEventItemJsonStringByLang.clear();
	}
	@Override
	public void clearAllBlogItems() {
		allBlogItemJsonStringByLang.clear();
	}
	@Override
	public void clearNewsItem(Long itemId) {
		int index = -1;
		int i=0;
		for(Entry<String, AllHomeScreenItem> oneLanguageEntry:languageBasedHomeScreenItems.entrySet()){
			index = -1;
			for(NewsItem oneNewsItem:oneLanguageEntry.getValue().getNewsItems()){
				if(oneNewsItem.getId().equals(itemId)){
					index = i;
					break;
				}
				i++;
			}
			if(index >= 0 ){
				oneLanguageEntry.getValue().getNewsItems().remove(index);
				String allHomeScreenItemJsonString = convertToJson(oneLanguageEntry.getValue(), oneLanguageEntry.getKey());
				writeToFile(cacheFileDirectory, allHomeScreenItemJsonString);
			}
		}
	}
	@Override
	public void clearVideoItem(Long itemId) {
		int index = -1;
		int i=0;
		for(Entry<String, AllHomeScreenItem> oneLanguageEntry:languageBasedHomeScreenItems.entrySet()){
			for(VideoItem oneVideoItem:oneLanguageEntry.getValue().getVideoItems()){
				if(oneVideoItem.getId().equals(itemId)){
					index = i;
					break;
				}
				i++;
			}
			if(index >= 0 ){
				oneLanguageEntry.getValue().getVideoItems().remove(index);
				String allHomeScreenItemJsonString = convertToJson(oneLanguageEntry.getValue(), oneLanguageEntry.getKey());
				writeToFile(cacheFileDirectory, allHomeScreenItemJsonString);
			}
		}
	}
	@Override
	public void clearEventItem(Long itemId) {
		int index = -1;
		int i=0;
		for(Entry<String, AllHomeScreenItem> oneLanguageEntry:languageBasedHomeScreenItems.entrySet()){
			for(EventItem oneEventItem:oneLanguageEntry.getValue().getEventItems()){
				if(oneEventItem.getId().equals(itemId)){
					index = i;
					break;
				}
				i++;
			}
			if(index >= 0 ){
				oneLanguageEntry.getValue().getEventItems().remove(index);
				String allHomeScreenItemJsonString = convertToJson(oneLanguageEntry.getValue(), oneLanguageEntry.getKey());
				writeToFile(cacheFileDirectory, allHomeScreenItemJsonString);
			}
		}
	}
	@Override
	public void clearBlogItem(Long itemId) {
		int index = -1;
		int i=0;
		for(Entry<String, AllHomeScreenItem> oneLanguageEntry:languageBasedHomeScreenItems.entrySet()){
			for(BlogItem oneBlogItem:oneLanguageEntry.getValue().getBlogItems()){
				if(oneBlogItem.getId().equals(itemId)){
					index = i;
					break;
				}
				i++;
			}
			if(index >= 0 ){
				oneLanguageEntry.getValue().getBlogItems().remove(index);
				String allHomeScreenItemJsonString = convertToJson(oneLanguageEntry.getValue(), oneLanguageEntry.getKey());
				writeToFile(cacheFileDirectory, allHomeScreenItemJsonString);
			}
		}
	}
	@Override
	public String getNewsItems(String lang,long acId, long votingAcId, long livingPcId, long votingPcId , int pageNumber) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String getVideoItems(String lang,long acId, long votingAcId, long livingPcId, long votingPcId, int pageNumber) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String getBlogItems(String lang,long acId, long votingAcId, long livingPcId, long votingPcId, int pageNumber) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void refreshFullCache() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public String getHomeScreenItems(String lang, long ac, long votingAcId, long livingPcId, long votingPcId) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String getNewsItems(String lang, long ac, long votingAcId, long livingPcId, long votingPcId) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String getVideoItems(String lang, long ac, long votingAcId, long livingPcId, long votingPcId) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String getEventItems(String lang, long ac, long votingAcId, long livingPcId, long votingPcId) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String getBlogItems(String lang, long ac, long votingAcId, long livingPcId, long votingPcId) {
		// TODO Auto-generated method stub
		return null;
	}

}
