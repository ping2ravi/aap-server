package com.next.aap.server.cache;

public interface LocationCache {

	public abstract String refreshCacheAsync();

	public abstract void refreshCache();

	public abstract String getAllStates();

	public abstract String getAllDistrictOfState(Long stateId);

	public abstract String getAllAssemblyConstituenciesOfDistrict(
			Long districtId);

	public abstract String getAllAssemblyConstituenciesOfState(Long stateId);
	
	public abstract String getAllParliamentConstituenciesOfState(Long stateId);

}