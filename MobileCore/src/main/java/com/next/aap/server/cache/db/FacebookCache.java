package com.next.aap.server.cache.db;

import org.springframework.stereotype.Component;

@Component
public class FacebookCache {

	private String facebookFeed;

	public String getFacebookFeed() {
		return facebookFeed;
	}

	public void setFacebookFeed(String facebookFeed) {
		this.facebookFeed = facebookFeed;
	}
}
